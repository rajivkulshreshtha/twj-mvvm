package io.realm;


public interface twj_lastlocal_in_twj_models_PullDataRealmProxyInterface {
    public long realmGet$_id();
    public void realmSet$_id(long value);
    public int realmGet$statusCode();
    public void realmSet$statusCode(int value);
    public String realmGet$message();
    public void realmSet$message(String value);
    public twj.lastlocal.in.twj.models.DataBean realmGet$data();
    public void realmSet$data(twj.lastlocal.in.twj.models.DataBean value);
}

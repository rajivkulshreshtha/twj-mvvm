package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.ProxyUtils;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class twj_lastlocal_in_twj_models_DataBeanRealmProxy extends twj.lastlocal.in.twj.models.DataBean
    implements RealmObjectProxy, twj_lastlocal_in_twj_models_DataBeanRealmProxyInterface {

    static final class DataBeanColumnInfo extends ColumnInfo {
        long counterUserRealmListIndex;
        long venueRealmListIndex;
        long visitorRealmListIndex;

        DataBeanColumnInfo(OsSchemaInfo schemaInfo) {
            super(3);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("DataBean");
            this.counterUserRealmListIndex = addColumnDetails("counterUserRealmList", "counterUserRealmList", objectSchemaInfo);
            this.venueRealmListIndex = addColumnDetails("venueRealmList", "venueRealmList", objectSchemaInfo);
            this.visitorRealmListIndex = addColumnDetails("visitorRealmList", "visitorRealmList", objectSchemaInfo);
        }

        DataBeanColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new DataBeanColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final DataBeanColumnInfo src = (DataBeanColumnInfo) rawSrc;
            final DataBeanColumnInfo dst = (DataBeanColumnInfo) rawDst;
            dst.counterUserRealmListIndex = src.counterUserRealmListIndex;
            dst.venueRealmListIndex = src.venueRealmListIndex;
            dst.visitorRealmListIndex = src.visitorRealmListIndex;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();

    private DataBeanColumnInfo columnInfo;
    private ProxyState<twj.lastlocal.in.twj.models.DataBean> proxyState;
    private RealmList<twj.lastlocal.in.twj.models.CounterUser> counterUserRealmListRealmList;
    private RealmList<twj.lastlocal.in.twj.models.Venue> venueRealmListRealmList;
    private RealmList<twj.lastlocal.in.twj.models.Visitor> visitorRealmListRealmList;

    twj_lastlocal_in_twj_models_DataBeanRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (DataBeanColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<twj.lastlocal.in.twj.models.DataBean>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    public RealmList<twj.lastlocal.in.twj.models.CounterUser> realmGet$counterUserRealmList() {
        proxyState.getRealm$realm().checkIfValid();
        // use the cached value if available
        if (counterUserRealmListRealmList != null) {
            return counterUserRealmListRealmList;
        } else {
            OsList osList = proxyState.getRow$realm().getModelList(columnInfo.counterUserRealmListIndex);
            counterUserRealmListRealmList = new RealmList<twj.lastlocal.in.twj.models.CounterUser>(twj.lastlocal.in.twj.models.CounterUser.class, osList, proxyState.getRealm$realm());
            return counterUserRealmListRealmList;
        }
    }

    @Override
    public void realmSet$counterUserRealmList(RealmList<twj.lastlocal.in.twj.models.CounterUser> value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("counterUserRealmList")) {
                return;
            }
            // if the list contains unmanaged RealmObjects, convert them to managed.
            if (value != null && !value.isManaged()) {
                final Realm realm = (Realm) proxyState.getRealm$realm();
                final RealmList<twj.lastlocal.in.twj.models.CounterUser> original = value;
                value = new RealmList<twj.lastlocal.in.twj.models.CounterUser>();
                for (twj.lastlocal.in.twj.models.CounterUser item : original) {
                    if (item == null || RealmObject.isManaged(item)) {
                        value.add(item);
                    } else {
                        value.add(realm.copyToRealm(item));
                    }
                }
            }
        }

        proxyState.getRealm$realm().checkIfValid();
        OsList osList = proxyState.getRow$realm().getModelList(columnInfo.counterUserRealmListIndex);
        // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
        if (value != null && value.size() == osList.size()) {
            int objects = value.size();
            for (int i = 0; i < objects; i++) {
                twj.lastlocal.in.twj.models.CounterUser linkedObject = value.get(i);
                proxyState.checkValidObject(linkedObject);
                osList.setRow(i, ((RealmObjectProxy) linkedObject).realmGet$proxyState().getRow$realm().getIndex());
            }
        } else {
            osList.removeAll();
            if (value == null) {
                return;
            }
            int objects = value.size();
            for (int i = 0; i < objects; i++) {
                twj.lastlocal.in.twj.models.CounterUser linkedObject = value.get(i);
                proxyState.checkValidObject(linkedObject);
                osList.addRow(((RealmObjectProxy) linkedObject).realmGet$proxyState().getRow$realm().getIndex());
            }
        }
    }

    @Override
    public RealmList<twj.lastlocal.in.twj.models.Venue> realmGet$venueRealmList() {
        proxyState.getRealm$realm().checkIfValid();
        // use the cached value if available
        if (venueRealmListRealmList != null) {
            return venueRealmListRealmList;
        } else {
            OsList osList = proxyState.getRow$realm().getModelList(columnInfo.venueRealmListIndex);
            venueRealmListRealmList = new RealmList<twj.lastlocal.in.twj.models.Venue>(twj.lastlocal.in.twj.models.Venue.class, osList, proxyState.getRealm$realm());
            return venueRealmListRealmList;
        }
    }

    @Override
    public void realmSet$venueRealmList(RealmList<twj.lastlocal.in.twj.models.Venue> value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("venueRealmList")) {
                return;
            }
            // if the list contains unmanaged RealmObjects, convert them to managed.
            if (value != null && !value.isManaged()) {
                final Realm realm = (Realm) proxyState.getRealm$realm();
                final RealmList<twj.lastlocal.in.twj.models.Venue> original = value;
                value = new RealmList<twj.lastlocal.in.twj.models.Venue>();
                for (twj.lastlocal.in.twj.models.Venue item : original) {
                    if (item == null || RealmObject.isManaged(item)) {
                        value.add(item);
                    } else {
                        value.add(realm.copyToRealm(item));
                    }
                }
            }
        }

        proxyState.getRealm$realm().checkIfValid();
        OsList osList = proxyState.getRow$realm().getModelList(columnInfo.venueRealmListIndex);
        // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
        if (value != null && value.size() == osList.size()) {
            int objects = value.size();
            for (int i = 0; i < objects; i++) {
                twj.lastlocal.in.twj.models.Venue linkedObject = value.get(i);
                proxyState.checkValidObject(linkedObject);
                osList.setRow(i, ((RealmObjectProxy) linkedObject).realmGet$proxyState().getRow$realm().getIndex());
            }
        } else {
            osList.removeAll();
            if (value == null) {
                return;
            }
            int objects = value.size();
            for (int i = 0; i < objects; i++) {
                twj.lastlocal.in.twj.models.Venue linkedObject = value.get(i);
                proxyState.checkValidObject(linkedObject);
                osList.addRow(((RealmObjectProxy) linkedObject).realmGet$proxyState().getRow$realm().getIndex());
            }
        }
    }

    @Override
    public RealmList<twj.lastlocal.in.twj.models.Visitor> realmGet$visitorRealmList() {
        proxyState.getRealm$realm().checkIfValid();
        // use the cached value if available
        if (visitorRealmListRealmList != null) {
            return visitorRealmListRealmList;
        } else {
            OsList osList = proxyState.getRow$realm().getModelList(columnInfo.visitorRealmListIndex);
            visitorRealmListRealmList = new RealmList<twj.lastlocal.in.twj.models.Visitor>(twj.lastlocal.in.twj.models.Visitor.class, osList, proxyState.getRealm$realm());
            return visitorRealmListRealmList;
        }
    }

    @Override
    public void realmSet$visitorRealmList(RealmList<twj.lastlocal.in.twj.models.Visitor> value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("visitorRealmList")) {
                return;
            }
            // if the list contains unmanaged RealmObjects, convert them to managed.
            if (value != null && !value.isManaged()) {
                final Realm realm = (Realm) proxyState.getRealm$realm();
                final RealmList<twj.lastlocal.in.twj.models.Visitor> original = value;
                value = new RealmList<twj.lastlocal.in.twj.models.Visitor>();
                for (twj.lastlocal.in.twj.models.Visitor item : original) {
                    if (item == null || RealmObject.isManaged(item)) {
                        value.add(item);
                    } else {
                        value.add(realm.copyToRealm(item));
                    }
                }
            }
        }

        proxyState.getRealm$realm().checkIfValid();
        OsList osList = proxyState.getRow$realm().getModelList(columnInfo.visitorRealmListIndex);
        // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
        if (value != null && value.size() == osList.size()) {
            int objects = value.size();
            for (int i = 0; i < objects; i++) {
                twj.lastlocal.in.twj.models.Visitor linkedObject = value.get(i);
                proxyState.checkValidObject(linkedObject);
                osList.setRow(i, ((RealmObjectProxy) linkedObject).realmGet$proxyState().getRow$realm().getIndex());
            }
        } else {
            osList.removeAll();
            if (value == null) {
                return;
            }
            int objects = value.size();
            for (int i = 0; i < objects; i++) {
                twj.lastlocal.in.twj.models.Visitor linkedObject = value.get(i);
                proxyState.checkValidObject(linkedObject);
                osList.addRow(((RealmObjectProxy) linkedObject).realmGet$proxyState().getRow$realm().getIndex());
            }
        }
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("DataBean", 3, 0);
        builder.addPersistedLinkProperty("counterUserRealmList", RealmFieldType.LIST, "CounterUser");
        builder.addPersistedLinkProperty("venueRealmList", RealmFieldType.LIST, "Venue");
        builder.addPersistedLinkProperty("visitorRealmList", RealmFieldType.LIST, "Visitor");
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static DataBeanColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new DataBeanColumnInfo(schemaInfo);
    }

    public static String getSimpleClassName() {
        return "DataBean";
    }

    public static final class ClassNameHelper {
        public static final String INTERNAL_CLASS_NAME = "DataBean";
    }

    @SuppressWarnings("cast")
    public static twj.lastlocal.in.twj.models.DataBean createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = new ArrayList<String>(3);
        if (json.has("counterUserRealmList")) {
            excludeFields.add("counterUserRealmList");
        }
        if (json.has("venueRealmList")) {
            excludeFields.add("venueRealmList");
        }
        if (json.has("visitorRealmList")) {
            excludeFields.add("visitorRealmList");
        }
        twj.lastlocal.in.twj.models.DataBean obj = realm.createObjectInternal(twj.lastlocal.in.twj.models.DataBean.class, true, excludeFields);

        final twj_lastlocal_in_twj_models_DataBeanRealmProxyInterface objProxy = (twj_lastlocal_in_twj_models_DataBeanRealmProxyInterface) obj;
        if (json.has("counterUserRealmList")) {
            if (json.isNull("counterUserRealmList")) {
                objProxy.realmSet$counterUserRealmList(null);
            } else {
                objProxy.realmGet$counterUserRealmList().clear();
                JSONArray array = json.getJSONArray("counterUserRealmList");
                for (int i = 0; i < array.length(); i++) {
                    twj.lastlocal.in.twj.models.CounterUser item = twj_lastlocal_in_twj_models_CounterUserRealmProxy.createOrUpdateUsingJsonObject(realm, array.getJSONObject(i), update);
                    objProxy.realmGet$counterUserRealmList().add(item);
                }
            }
        }
        if (json.has("venueRealmList")) {
            if (json.isNull("venueRealmList")) {
                objProxy.realmSet$venueRealmList(null);
            } else {
                objProxy.realmGet$venueRealmList().clear();
                JSONArray array = json.getJSONArray("venueRealmList");
                for (int i = 0; i < array.length(); i++) {
                    twj.lastlocal.in.twj.models.Venue item = twj_lastlocal_in_twj_models_VenueRealmProxy.createOrUpdateUsingJsonObject(realm, array.getJSONObject(i), update);
                    objProxy.realmGet$venueRealmList().add(item);
                }
            }
        }
        if (json.has("visitorRealmList")) {
            if (json.isNull("visitorRealmList")) {
                objProxy.realmSet$visitorRealmList(null);
            } else {
                objProxy.realmGet$visitorRealmList().clear();
                JSONArray array = json.getJSONArray("visitorRealmList");
                for (int i = 0; i < array.length(); i++) {
                    twj.lastlocal.in.twj.models.Visitor item = twj_lastlocal_in_twj_models_VisitorRealmProxy.createOrUpdateUsingJsonObject(realm, array.getJSONObject(i), update);
                    objProxy.realmGet$visitorRealmList().add(item);
                }
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static twj.lastlocal.in.twj.models.DataBean createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        final twj.lastlocal.in.twj.models.DataBean obj = new twj.lastlocal.in.twj.models.DataBean();
        final twj_lastlocal_in_twj_models_DataBeanRealmProxyInterface objProxy = (twj_lastlocal_in_twj_models_DataBeanRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("counterUserRealmList")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    objProxy.realmSet$counterUserRealmList(null);
                } else {
                    objProxy.realmSet$counterUserRealmList(new RealmList<twj.lastlocal.in.twj.models.CounterUser>());
                    reader.beginArray();
                    while (reader.hasNext()) {
                        twj.lastlocal.in.twj.models.CounterUser item = twj_lastlocal_in_twj_models_CounterUserRealmProxy.createUsingJsonStream(realm, reader);
                        objProxy.realmGet$counterUserRealmList().add(item);
                    }
                    reader.endArray();
                }
            } else if (name.equals("venueRealmList")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    objProxy.realmSet$venueRealmList(null);
                } else {
                    objProxy.realmSet$venueRealmList(new RealmList<twj.lastlocal.in.twj.models.Venue>());
                    reader.beginArray();
                    while (reader.hasNext()) {
                        twj.lastlocal.in.twj.models.Venue item = twj_lastlocal_in_twj_models_VenueRealmProxy.createUsingJsonStream(realm, reader);
                        objProxy.realmGet$venueRealmList().add(item);
                    }
                    reader.endArray();
                }
            } else if (name.equals("visitorRealmList")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    objProxy.realmSet$visitorRealmList(null);
                } else {
                    objProxy.realmSet$visitorRealmList(new RealmList<twj.lastlocal.in.twj.models.Visitor>());
                    reader.beginArray();
                    while (reader.hasNext()) {
                        twj.lastlocal.in.twj.models.Visitor item = twj_lastlocal_in_twj_models_VisitorRealmProxy.createUsingJsonStream(realm, reader);
                        objProxy.realmGet$visitorRealmList().add(item);
                    }
                    reader.endArray();
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return realm.copyToRealm(obj);
    }

    public static twj.lastlocal.in.twj.models.DataBean copyOrUpdate(Realm realm, twj.lastlocal.in.twj.models.DataBean object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (twj.lastlocal.in.twj.models.DataBean) cachedRealmObject;
        }

        return copy(realm, object, update, cache);
    }

    public static twj.lastlocal.in.twj.models.DataBean copy(Realm realm, twj.lastlocal.in.twj.models.DataBean newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (twj.lastlocal.in.twj.models.DataBean) cachedRealmObject;
        }

        // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
        twj.lastlocal.in.twj.models.DataBean realmObject = realm.createObjectInternal(twj.lastlocal.in.twj.models.DataBean.class, false, Collections.<String>emptyList());
        cache.put(newObject, (RealmObjectProxy) realmObject);

        twj_lastlocal_in_twj_models_DataBeanRealmProxyInterface realmObjectSource = (twj_lastlocal_in_twj_models_DataBeanRealmProxyInterface) newObject;
        twj_lastlocal_in_twj_models_DataBeanRealmProxyInterface realmObjectCopy = (twj_lastlocal_in_twj_models_DataBeanRealmProxyInterface) realmObject;


        RealmList<twj.lastlocal.in.twj.models.CounterUser> counterUserRealmListList = realmObjectSource.realmGet$counterUserRealmList();
        if (counterUserRealmListList != null) {
            RealmList<twj.lastlocal.in.twj.models.CounterUser> counterUserRealmListRealmList = realmObjectCopy.realmGet$counterUserRealmList();
            counterUserRealmListRealmList.clear();
            for (int i = 0; i < counterUserRealmListList.size(); i++) {
                twj.lastlocal.in.twj.models.CounterUser counterUserRealmListItem = counterUserRealmListList.get(i);
                twj.lastlocal.in.twj.models.CounterUser cachecounterUserRealmList = (twj.lastlocal.in.twj.models.CounterUser) cache.get(counterUserRealmListItem);
                if (cachecounterUserRealmList != null) {
                    counterUserRealmListRealmList.add(cachecounterUserRealmList);
                } else {
                    counterUserRealmListRealmList.add(twj_lastlocal_in_twj_models_CounterUserRealmProxy.copyOrUpdate(realm, counterUserRealmListItem, update, cache));
                }
            }
        }


        RealmList<twj.lastlocal.in.twj.models.Venue> venueRealmListList = realmObjectSource.realmGet$venueRealmList();
        if (venueRealmListList != null) {
            RealmList<twj.lastlocal.in.twj.models.Venue> venueRealmListRealmList = realmObjectCopy.realmGet$venueRealmList();
            venueRealmListRealmList.clear();
            for (int i = 0; i < venueRealmListList.size(); i++) {
                twj.lastlocal.in.twj.models.Venue venueRealmListItem = venueRealmListList.get(i);
                twj.lastlocal.in.twj.models.Venue cachevenueRealmList = (twj.lastlocal.in.twj.models.Venue) cache.get(venueRealmListItem);
                if (cachevenueRealmList != null) {
                    venueRealmListRealmList.add(cachevenueRealmList);
                } else {
                    venueRealmListRealmList.add(twj_lastlocal_in_twj_models_VenueRealmProxy.copyOrUpdate(realm, venueRealmListItem, update, cache));
                }
            }
        }


        RealmList<twj.lastlocal.in.twj.models.Visitor> visitorRealmListList = realmObjectSource.realmGet$visitorRealmList();
        if (visitorRealmListList != null) {
            RealmList<twj.lastlocal.in.twj.models.Visitor> visitorRealmListRealmList = realmObjectCopy.realmGet$visitorRealmList();
            visitorRealmListRealmList.clear();
            for (int i = 0; i < visitorRealmListList.size(); i++) {
                twj.lastlocal.in.twj.models.Visitor visitorRealmListItem = visitorRealmListList.get(i);
                twj.lastlocal.in.twj.models.Visitor cachevisitorRealmList = (twj.lastlocal.in.twj.models.Visitor) cache.get(visitorRealmListItem);
                if (cachevisitorRealmList != null) {
                    visitorRealmListRealmList.add(cachevisitorRealmList);
                } else {
                    visitorRealmListRealmList.add(twj_lastlocal_in_twj_models_VisitorRealmProxy.copyOrUpdate(realm, visitorRealmListItem, update, cache));
                }
            }
        }

        return realmObject;
    }

    public static long insert(Realm realm, twj.lastlocal.in.twj.models.DataBean object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(twj.lastlocal.in.twj.models.DataBean.class);
        long tableNativePtr = table.getNativePtr();
        DataBeanColumnInfo columnInfo = (DataBeanColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.DataBean.class);
        long rowIndex = OsObject.createRow(table);
        cache.put(object, rowIndex);

        RealmList<twj.lastlocal.in.twj.models.CounterUser> counterUserRealmListList = ((twj_lastlocal_in_twj_models_DataBeanRealmProxyInterface) object).realmGet$counterUserRealmList();
        if (counterUserRealmListList != null) {
            OsList counterUserRealmListOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.counterUserRealmListIndex);
            for (twj.lastlocal.in.twj.models.CounterUser counterUserRealmListItem : counterUserRealmListList) {
                Long cacheItemIndexcounterUserRealmList = cache.get(counterUserRealmListItem);
                if (cacheItemIndexcounterUserRealmList == null) {
                    cacheItemIndexcounterUserRealmList = twj_lastlocal_in_twj_models_CounterUserRealmProxy.insert(realm, counterUserRealmListItem, cache);
                }
                counterUserRealmListOsList.addRow(cacheItemIndexcounterUserRealmList);
            }
        }

        RealmList<twj.lastlocal.in.twj.models.Venue> venueRealmListList = ((twj_lastlocal_in_twj_models_DataBeanRealmProxyInterface) object).realmGet$venueRealmList();
        if (venueRealmListList != null) {
            OsList venueRealmListOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.venueRealmListIndex);
            for (twj.lastlocal.in.twj.models.Venue venueRealmListItem : venueRealmListList) {
                Long cacheItemIndexvenueRealmList = cache.get(venueRealmListItem);
                if (cacheItemIndexvenueRealmList == null) {
                    cacheItemIndexvenueRealmList = twj_lastlocal_in_twj_models_VenueRealmProxy.insert(realm, venueRealmListItem, cache);
                }
                venueRealmListOsList.addRow(cacheItemIndexvenueRealmList);
            }
        }

        RealmList<twj.lastlocal.in.twj.models.Visitor> visitorRealmListList = ((twj_lastlocal_in_twj_models_DataBeanRealmProxyInterface) object).realmGet$visitorRealmList();
        if (visitorRealmListList != null) {
            OsList visitorRealmListOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.visitorRealmListIndex);
            for (twj.lastlocal.in.twj.models.Visitor visitorRealmListItem : visitorRealmListList) {
                Long cacheItemIndexvisitorRealmList = cache.get(visitorRealmListItem);
                if (cacheItemIndexvisitorRealmList == null) {
                    cacheItemIndexvisitorRealmList = twj_lastlocal_in_twj_models_VisitorRealmProxy.insert(realm, visitorRealmListItem, cache);
                }
                visitorRealmListOsList.addRow(cacheItemIndexvisitorRealmList);
            }
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(twj.lastlocal.in.twj.models.DataBean.class);
        long tableNativePtr = table.getNativePtr();
        DataBeanColumnInfo columnInfo = (DataBeanColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.DataBean.class);
        twj.lastlocal.in.twj.models.DataBean object = null;
        while (objects.hasNext()) {
            object = (twj.lastlocal.in.twj.models.DataBean) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = OsObject.createRow(table);
            cache.put(object, rowIndex);

            RealmList<twj.lastlocal.in.twj.models.CounterUser> counterUserRealmListList = ((twj_lastlocal_in_twj_models_DataBeanRealmProxyInterface) object).realmGet$counterUserRealmList();
            if (counterUserRealmListList != null) {
                OsList counterUserRealmListOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.counterUserRealmListIndex);
                for (twj.lastlocal.in.twj.models.CounterUser counterUserRealmListItem : counterUserRealmListList) {
                    Long cacheItemIndexcounterUserRealmList = cache.get(counterUserRealmListItem);
                    if (cacheItemIndexcounterUserRealmList == null) {
                        cacheItemIndexcounterUserRealmList = twj_lastlocal_in_twj_models_CounterUserRealmProxy.insert(realm, counterUserRealmListItem, cache);
                    }
                    counterUserRealmListOsList.addRow(cacheItemIndexcounterUserRealmList);
                }
            }

            RealmList<twj.lastlocal.in.twj.models.Venue> venueRealmListList = ((twj_lastlocal_in_twj_models_DataBeanRealmProxyInterface) object).realmGet$venueRealmList();
            if (venueRealmListList != null) {
                OsList venueRealmListOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.venueRealmListIndex);
                for (twj.lastlocal.in.twj.models.Venue venueRealmListItem : venueRealmListList) {
                    Long cacheItemIndexvenueRealmList = cache.get(venueRealmListItem);
                    if (cacheItemIndexvenueRealmList == null) {
                        cacheItemIndexvenueRealmList = twj_lastlocal_in_twj_models_VenueRealmProxy.insert(realm, venueRealmListItem, cache);
                    }
                    venueRealmListOsList.addRow(cacheItemIndexvenueRealmList);
                }
            }

            RealmList<twj.lastlocal.in.twj.models.Visitor> visitorRealmListList = ((twj_lastlocal_in_twj_models_DataBeanRealmProxyInterface) object).realmGet$visitorRealmList();
            if (visitorRealmListList != null) {
                OsList visitorRealmListOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.visitorRealmListIndex);
                for (twj.lastlocal.in.twj.models.Visitor visitorRealmListItem : visitorRealmListList) {
                    Long cacheItemIndexvisitorRealmList = cache.get(visitorRealmListItem);
                    if (cacheItemIndexvisitorRealmList == null) {
                        cacheItemIndexvisitorRealmList = twj_lastlocal_in_twj_models_VisitorRealmProxy.insert(realm, visitorRealmListItem, cache);
                    }
                    visitorRealmListOsList.addRow(cacheItemIndexvisitorRealmList);
                }
            }
        }
    }

    public static long insertOrUpdate(Realm realm, twj.lastlocal.in.twj.models.DataBean object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(twj.lastlocal.in.twj.models.DataBean.class);
        long tableNativePtr = table.getNativePtr();
        DataBeanColumnInfo columnInfo = (DataBeanColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.DataBean.class);
        long rowIndex = OsObject.createRow(table);
        cache.put(object, rowIndex);

        OsList counterUserRealmListOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.counterUserRealmListIndex);
        RealmList<twj.lastlocal.in.twj.models.CounterUser> counterUserRealmListList = ((twj_lastlocal_in_twj_models_DataBeanRealmProxyInterface) object).realmGet$counterUserRealmList();
        if (counterUserRealmListList != null && counterUserRealmListList.size() == counterUserRealmListOsList.size()) {
            // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
            int objects = counterUserRealmListList.size();
            for (int i = 0; i < objects; i++) {
                twj.lastlocal.in.twj.models.CounterUser counterUserRealmListItem = counterUserRealmListList.get(i);
                Long cacheItemIndexcounterUserRealmList = cache.get(counterUserRealmListItem);
                if (cacheItemIndexcounterUserRealmList == null) {
                    cacheItemIndexcounterUserRealmList = twj_lastlocal_in_twj_models_CounterUserRealmProxy.insertOrUpdate(realm, counterUserRealmListItem, cache);
                }
                counterUserRealmListOsList.setRow(i, cacheItemIndexcounterUserRealmList);
            }
        } else {
            counterUserRealmListOsList.removeAll();
            if (counterUserRealmListList != null) {
                for (twj.lastlocal.in.twj.models.CounterUser counterUserRealmListItem : counterUserRealmListList) {
                    Long cacheItemIndexcounterUserRealmList = cache.get(counterUserRealmListItem);
                    if (cacheItemIndexcounterUserRealmList == null) {
                        cacheItemIndexcounterUserRealmList = twj_lastlocal_in_twj_models_CounterUserRealmProxy.insertOrUpdate(realm, counterUserRealmListItem, cache);
                    }
                    counterUserRealmListOsList.addRow(cacheItemIndexcounterUserRealmList);
                }
            }
        }


        OsList venueRealmListOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.venueRealmListIndex);
        RealmList<twj.lastlocal.in.twj.models.Venue> venueRealmListList = ((twj_lastlocal_in_twj_models_DataBeanRealmProxyInterface) object).realmGet$venueRealmList();
        if (venueRealmListList != null && venueRealmListList.size() == venueRealmListOsList.size()) {
            // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
            int objects = venueRealmListList.size();
            for (int i = 0; i < objects; i++) {
                twj.lastlocal.in.twj.models.Venue venueRealmListItem = venueRealmListList.get(i);
                Long cacheItemIndexvenueRealmList = cache.get(venueRealmListItem);
                if (cacheItemIndexvenueRealmList == null) {
                    cacheItemIndexvenueRealmList = twj_lastlocal_in_twj_models_VenueRealmProxy.insertOrUpdate(realm, venueRealmListItem, cache);
                }
                venueRealmListOsList.setRow(i, cacheItemIndexvenueRealmList);
            }
        } else {
            venueRealmListOsList.removeAll();
            if (venueRealmListList != null) {
                for (twj.lastlocal.in.twj.models.Venue venueRealmListItem : venueRealmListList) {
                    Long cacheItemIndexvenueRealmList = cache.get(venueRealmListItem);
                    if (cacheItemIndexvenueRealmList == null) {
                        cacheItemIndexvenueRealmList = twj_lastlocal_in_twj_models_VenueRealmProxy.insertOrUpdate(realm, venueRealmListItem, cache);
                    }
                    venueRealmListOsList.addRow(cacheItemIndexvenueRealmList);
                }
            }
        }


        OsList visitorRealmListOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.visitorRealmListIndex);
        RealmList<twj.lastlocal.in.twj.models.Visitor> visitorRealmListList = ((twj_lastlocal_in_twj_models_DataBeanRealmProxyInterface) object).realmGet$visitorRealmList();
        if (visitorRealmListList != null && visitorRealmListList.size() == visitorRealmListOsList.size()) {
            // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
            int objects = visitorRealmListList.size();
            for (int i = 0; i < objects; i++) {
                twj.lastlocal.in.twj.models.Visitor visitorRealmListItem = visitorRealmListList.get(i);
                Long cacheItemIndexvisitorRealmList = cache.get(visitorRealmListItem);
                if (cacheItemIndexvisitorRealmList == null) {
                    cacheItemIndexvisitorRealmList = twj_lastlocal_in_twj_models_VisitorRealmProxy.insertOrUpdate(realm, visitorRealmListItem, cache);
                }
                visitorRealmListOsList.setRow(i, cacheItemIndexvisitorRealmList);
            }
        } else {
            visitorRealmListOsList.removeAll();
            if (visitorRealmListList != null) {
                for (twj.lastlocal.in.twj.models.Visitor visitorRealmListItem : visitorRealmListList) {
                    Long cacheItemIndexvisitorRealmList = cache.get(visitorRealmListItem);
                    if (cacheItemIndexvisitorRealmList == null) {
                        cacheItemIndexvisitorRealmList = twj_lastlocal_in_twj_models_VisitorRealmProxy.insertOrUpdate(realm, visitorRealmListItem, cache);
                    }
                    visitorRealmListOsList.addRow(cacheItemIndexvisitorRealmList);
                }
            }
        }

        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(twj.lastlocal.in.twj.models.DataBean.class);
        long tableNativePtr = table.getNativePtr();
        DataBeanColumnInfo columnInfo = (DataBeanColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.DataBean.class);
        twj.lastlocal.in.twj.models.DataBean object = null;
        while (objects.hasNext()) {
            object = (twj.lastlocal.in.twj.models.DataBean) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = OsObject.createRow(table);
            cache.put(object, rowIndex);

            OsList counterUserRealmListOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.counterUserRealmListIndex);
            RealmList<twj.lastlocal.in.twj.models.CounterUser> counterUserRealmListList = ((twj_lastlocal_in_twj_models_DataBeanRealmProxyInterface) object).realmGet$counterUserRealmList();
            if (counterUserRealmListList != null && counterUserRealmListList.size() == counterUserRealmListOsList.size()) {
                // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
                int objectCount = counterUserRealmListList.size();
                for (int i = 0; i < objectCount; i++) {
                    twj.lastlocal.in.twj.models.CounterUser counterUserRealmListItem = counterUserRealmListList.get(i);
                    Long cacheItemIndexcounterUserRealmList = cache.get(counterUserRealmListItem);
                    if (cacheItemIndexcounterUserRealmList == null) {
                        cacheItemIndexcounterUserRealmList = twj_lastlocal_in_twj_models_CounterUserRealmProxy.insertOrUpdate(realm, counterUserRealmListItem, cache);
                    }
                    counterUserRealmListOsList.setRow(i, cacheItemIndexcounterUserRealmList);
                }
            } else {
                counterUserRealmListOsList.removeAll();
                if (counterUserRealmListList != null) {
                    for (twj.lastlocal.in.twj.models.CounterUser counterUserRealmListItem : counterUserRealmListList) {
                        Long cacheItemIndexcounterUserRealmList = cache.get(counterUserRealmListItem);
                        if (cacheItemIndexcounterUserRealmList == null) {
                            cacheItemIndexcounterUserRealmList = twj_lastlocal_in_twj_models_CounterUserRealmProxy.insertOrUpdate(realm, counterUserRealmListItem, cache);
                        }
                        counterUserRealmListOsList.addRow(cacheItemIndexcounterUserRealmList);
                    }
                }
            }


            OsList venueRealmListOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.venueRealmListIndex);
            RealmList<twj.lastlocal.in.twj.models.Venue> venueRealmListList = ((twj_lastlocal_in_twj_models_DataBeanRealmProxyInterface) object).realmGet$venueRealmList();
            if (venueRealmListList != null && venueRealmListList.size() == venueRealmListOsList.size()) {
                // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
                int objectCount = venueRealmListList.size();
                for (int i = 0; i < objectCount; i++) {
                    twj.lastlocal.in.twj.models.Venue venueRealmListItem = venueRealmListList.get(i);
                    Long cacheItemIndexvenueRealmList = cache.get(venueRealmListItem);
                    if (cacheItemIndexvenueRealmList == null) {
                        cacheItemIndexvenueRealmList = twj_lastlocal_in_twj_models_VenueRealmProxy.insertOrUpdate(realm, venueRealmListItem, cache);
                    }
                    venueRealmListOsList.setRow(i, cacheItemIndexvenueRealmList);
                }
            } else {
                venueRealmListOsList.removeAll();
                if (venueRealmListList != null) {
                    for (twj.lastlocal.in.twj.models.Venue venueRealmListItem : venueRealmListList) {
                        Long cacheItemIndexvenueRealmList = cache.get(venueRealmListItem);
                        if (cacheItemIndexvenueRealmList == null) {
                            cacheItemIndexvenueRealmList = twj_lastlocal_in_twj_models_VenueRealmProxy.insertOrUpdate(realm, venueRealmListItem, cache);
                        }
                        venueRealmListOsList.addRow(cacheItemIndexvenueRealmList);
                    }
                }
            }


            OsList visitorRealmListOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.visitorRealmListIndex);
            RealmList<twj.lastlocal.in.twj.models.Visitor> visitorRealmListList = ((twj_lastlocal_in_twj_models_DataBeanRealmProxyInterface) object).realmGet$visitorRealmList();
            if (visitorRealmListList != null && visitorRealmListList.size() == visitorRealmListOsList.size()) {
                // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
                int objectCount = visitorRealmListList.size();
                for (int i = 0; i < objectCount; i++) {
                    twj.lastlocal.in.twj.models.Visitor visitorRealmListItem = visitorRealmListList.get(i);
                    Long cacheItemIndexvisitorRealmList = cache.get(visitorRealmListItem);
                    if (cacheItemIndexvisitorRealmList == null) {
                        cacheItemIndexvisitorRealmList = twj_lastlocal_in_twj_models_VisitorRealmProxy.insertOrUpdate(realm, visitorRealmListItem, cache);
                    }
                    visitorRealmListOsList.setRow(i, cacheItemIndexvisitorRealmList);
                }
            } else {
                visitorRealmListOsList.removeAll();
                if (visitorRealmListList != null) {
                    for (twj.lastlocal.in.twj.models.Visitor visitorRealmListItem : visitorRealmListList) {
                        Long cacheItemIndexvisitorRealmList = cache.get(visitorRealmListItem);
                        if (cacheItemIndexvisitorRealmList == null) {
                            cacheItemIndexvisitorRealmList = twj_lastlocal_in_twj_models_VisitorRealmProxy.insertOrUpdate(realm, visitorRealmListItem, cache);
                        }
                        visitorRealmListOsList.addRow(cacheItemIndexvisitorRealmList);
                    }
                }
            }

        }
    }

    public static twj.lastlocal.in.twj.models.DataBean createDetachedCopy(twj.lastlocal.in.twj.models.DataBean realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        twj.lastlocal.in.twj.models.DataBean unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new twj.lastlocal.in.twj.models.DataBean();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (twj.lastlocal.in.twj.models.DataBean) cachedObject.object;
            }
            unmanagedObject = (twj.lastlocal.in.twj.models.DataBean) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        twj_lastlocal_in_twj_models_DataBeanRealmProxyInterface unmanagedCopy = (twj_lastlocal_in_twj_models_DataBeanRealmProxyInterface) unmanagedObject;
        twj_lastlocal_in_twj_models_DataBeanRealmProxyInterface realmSource = (twj_lastlocal_in_twj_models_DataBeanRealmProxyInterface) realmObject;

        // Deep copy of counterUserRealmList
        if (currentDepth == maxDepth) {
            unmanagedCopy.realmSet$counterUserRealmList(null);
        } else {
            RealmList<twj.lastlocal.in.twj.models.CounterUser> managedcounterUserRealmListList = realmSource.realmGet$counterUserRealmList();
            RealmList<twj.lastlocal.in.twj.models.CounterUser> unmanagedcounterUserRealmListList = new RealmList<twj.lastlocal.in.twj.models.CounterUser>();
            unmanagedCopy.realmSet$counterUserRealmList(unmanagedcounterUserRealmListList);
            int nextDepth = currentDepth + 1;
            int size = managedcounterUserRealmListList.size();
            for (int i = 0; i < size; i++) {
                twj.lastlocal.in.twj.models.CounterUser item = twj_lastlocal_in_twj_models_CounterUserRealmProxy.createDetachedCopy(managedcounterUserRealmListList.get(i), nextDepth, maxDepth, cache);
                unmanagedcounterUserRealmListList.add(item);
            }
        }

        // Deep copy of venueRealmList
        if (currentDepth == maxDepth) {
            unmanagedCopy.realmSet$venueRealmList(null);
        } else {
            RealmList<twj.lastlocal.in.twj.models.Venue> managedvenueRealmListList = realmSource.realmGet$venueRealmList();
            RealmList<twj.lastlocal.in.twj.models.Venue> unmanagedvenueRealmListList = new RealmList<twj.lastlocal.in.twj.models.Venue>();
            unmanagedCopy.realmSet$venueRealmList(unmanagedvenueRealmListList);
            int nextDepth = currentDepth + 1;
            int size = managedvenueRealmListList.size();
            for (int i = 0; i < size; i++) {
                twj.lastlocal.in.twj.models.Venue item = twj_lastlocal_in_twj_models_VenueRealmProxy.createDetachedCopy(managedvenueRealmListList.get(i), nextDepth, maxDepth, cache);
                unmanagedvenueRealmListList.add(item);
            }
        }

        // Deep copy of visitorRealmList
        if (currentDepth == maxDepth) {
            unmanagedCopy.realmSet$visitorRealmList(null);
        } else {
            RealmList<twj.lastlocal.in.twj.models.Visitor> managedvisitorRealmListList = realmSource.realmGet$visitorRealmList();
            RealmList<twj.lastlocal.in.twj.models.Visitor> unmanagedvisitorRealmListList = new RealmList<twj.lastlocal.in.twj.models.Visitor>();
            unmanagedCopy.realmSet$visitorRealmList(unmanagedvisitorRealmListList);
            int nextDepth = currentDepth + 1;
            int size = managedvisitorRealmListList.size();
            for (int i = 0; i < size; i++) {
                twj.lastlocal.in.twj.models.Visitor item = twj_lastlocal_in_twj_models_VisitorRealmProxy.createDetachedCopy(managedvisitorRealmListList.get(i), nextDepth, maxDepth, cache);
                unmanagedvisitorRealmListList.add(item);
            }
        }

        return unmanagedObject;
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        twj_lastlocal_in_twj_models_DataBeanRealmProxy aDataBean = (twj_lastlocal_in_twj_models_DataBeanRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aDataBean.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aDataBean.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aDataBean.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }
}

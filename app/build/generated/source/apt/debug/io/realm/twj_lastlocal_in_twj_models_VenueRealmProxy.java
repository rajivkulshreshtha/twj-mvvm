package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.ProxyUtils;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class twj_lastlocal_in_twj_models_VenueRealmProxy extends twj.lastlocal.in.twj.models.Venue
    implements RealmObjectProxy, twj_lastlocal_in_twj_models_VenueRealmProxyInterface {

    static final class VenueColumnInfo extends ColumnInfo {
        long _idIndex;
        long venueIdIndex;
        long locationIndex;
        long fromDateIndex;
        long toDateIndex;

        VenueColumnInfo(OsSchemaInfo schemaInfo) {
            super(5);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("Venue");
            this._idIndex = addColumnDetails("_id", "_id", objectSchemaInfo);
            this.venueIdIndex = addColumnDetails("venueId", "venueId", objectSchemaInfo);
            this.locationIndex = addColumnDetails("location", "location", objectSchemaInfo);
            this.fromDateIndex = addColumnDetails("fromDate", "fromDate", objectSchemaInfo);
            this.toDateIndex = addColumnDetails("toDate", "toDate", objectSchemaInfo);
        }

        VenueColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new VenueColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final VenueColumnInfo src = (VenueColumnInfo) rawSrc;
            final VenueColumnInfo dst = (VenueColumnInfo) rawDst;
            dst._idIndex = src._idIndex;
            dst.venueIdIndex = src.venueIdIndex;
            dst.locationIndex = src.locationIndex;
            dst.fromDateIndex = src.fromDateIndex;
            dst.toDateIndex = src.toDateIndex;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();

    private VenueColumnInfo columnInfo;
    private ProxyState<twj.lastlocal.in.twj.models.Venue> proxyState;

    twj_lastlocal_in_twj_models_VenueRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (VenueColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<twj.lastlocal.in.twj.models.Venue>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public long realmGet$_id() {
        proxyState.getRealm$realm().checkIfValid();
        return (long) proxyState.getRow$realm().getLong(columnInfo._idIndex);
    }

    @Override
    public void realmSet$_id(long value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field '_id' cannot be changed after object was created.");
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$venueId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.venueIdIndex);
    }

    @Override
    public void realmSet$venueId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.venueIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.venueIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.venueIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.venueIdIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$location() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.locationIndex);
    }

    @Override
    public void realmSet$location(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.locationIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.locationIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.locationIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.locationIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$fromDate() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.fromDateIndex);
    }

    @Override
    public void realmSet$fromDate(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.fromDateIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.fromDateIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.fromDateIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.fromDateIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$toDate() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.toDateIndex);
    }

    @Override
    public void realmSet$toDate(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.toDateIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.toDateIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.toDateIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.toDateIndex, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("Venue", 5, 0);
        builder.addPersistedProperty("_id", RealmFieldType.INTEGER, Property.PRIMARY_KEY, Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("venueId", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("location", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("fromDate", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("toDate", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static VenueColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new VenueColumnInfo(schemaInfo);
    }

    public static String getSimpleClassName() {
        return "Venue";
    }

    public static final class ClassNameHelper {
        public static final String INTERNAL_CLASS_NAME = "Venue";
    }

    @SuppressWarnings("cast")
    public static twj.lastlocal.in.twj.models.Venue createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        twj.lastlocal.in.twj.models.Venue obj = null;
        if (update) {
            Table table = realm.getTable(twj.lastlocal.in.twj.models.Venue.class);
            VenueColumnInfo columnInfo = (VenueColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.Venue.class);
            long pkColumnIndex = columnInfo._idIndex;
            long rowIndex = Table.NO_MATCH;
            if (!json.isNull("_id")) {
                rowIndex = table.findFirstLong(pkColumnIndex, json.getLong("_id"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.Venue.class), false, Collections.<String> emptyList());
                    obj = new io.realm.twj_lastlocal_in_twj_models_VenueRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("_id")) {
                if (json.isNull("_id")) {
                    obj = (io.realm.twj_lastlocal_in_twj_models_VenueRealmProxy) realm.createObjectInternal(twj.lastlocal.in.twj.models.Venue.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.twj_lastlocal_in_twj_models_VenueRealmProxy) realm.createObjectInternal(twj.lastlocal.in.twj.models.Venue.class, json.getLong("_id"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field '_id'.");
            }
        }

        final twj_lastlocal_in_twj_models_VenueRealmProxyInterface objProxy = (twj_lastlocal_in_twj_models_VenueRealmProxyInterface) obj;
        if (json.has("venueId")) {
            if (json.isNull("venueId")) {
                objProxy.realmSet$venueId(null);
            } else {
                objProxy.realmSet$venueId((String) json.getString("venueId"));
            }
        }
        if (json.has("location")) {
            if (json.isNull("location")) {
                objProxy.realmSet$location(null);
            } else {
                objProxy.realmSet$location((String) json.getString("location"));
            }
        }
        if (json.has("fromDate")) {
            if (json.isNull("fromDate")) {
                objProxy.realmSet$fromDate(null);
            } else {
                objProxy.realmSet$fromDate((String) json.getString("fromDate"));
            }
        }
        if (json.has("toDate")) {
            if (json.isNull("toDate")) {
                objProxy.realmSet$toDate(null);
            } else {
                objProxy.realmSet$toDate((String) json.getString("toDate"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static twj.lastlocal.in.twj.models.Venue createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        final twj.lastlocal.in.twj.models.Venue obj = new twj.lastlocal.in.twj.models.Venue();
        final twj_lastlocal_in_twj_models_VenueRealmProxyInterface objProxy = (twj_lastlocal_in_twj_models_VenueRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("_id")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$_id((long) reader.nextLong());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field '_id' to null.");
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("venueId")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$venueId((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$venueId(null);
                }
            } else if (name.equals("location")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$location((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$location(null);
                }
            } else if (name.equals("fromDate")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$fromDate((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$fromDate(null);
                }
            } else if (name.equals("toDate")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$toDate((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$toDate(null);
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field '_id'.");
        }
        return realm.copyToRealm(obj);
    }

    public static twj.lastlocal.in.twj.models.Venue copyOrUpdate(Realm realm, twj.lastlocal.in.twj.models.Venue object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (twj.lastlocal.in.twj.models.Venue) cachedRealmObject;
        }

        twj.lastlocal.in.twj.models.Venue realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(twj.lastlocal.in.twj.models.Venue.class);
            VenueColumnInfo columnInfo = (VenueColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.Venue.class);
            long pkColumnIndex = columnInfo._idIndex;
            long rowIndex = table.findFirstLong(pkColumnIndex, ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$_id());
            if (rowIndex == Table.NO_MATCH) {
                canUpdate = false;
            } else {
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.Venue.class), false, Collections.<String> emptyList());
                    realmObject = new io.realm.twj_lastlocal_in_twj_models_VenueRealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            }
        }

        return (canUpdate) ? update(realm, realmObject, object, cache) : copy(realm, object, update, cache);
    }

    public static twj.lastlocal.in.twj.models.Venue copy(Realm realm, twj.lastlocal.in.twj.models.Venue newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (twj.lastlocal.in.twj.models.Venue) cachedRealmObject;
        }

        // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
        twj.lastlocal.in.twj.models.Venue realmObject = realm.createObjectInternal(twj.lastlocal.in.twj.models.Venue.class, ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) newObject).realmGet$_id(), false, Collections.<String>emptyList());
        cache.put(newObject, (RealmObjectProxy) realmObject);

        twj_lastlocal_in_twj_models_VenueRealmProxyInterface realmObjectSource = (twj_lastlocal_in_twj_models_VenueRealmProxyInterface) newObject;
        twj_lastlocal_in_twj_models_VenueRealmProxyInterface realmObjectCopy = (twj_lastlocal_in_twj_models_VenueRealmProxyInterface) realmObject;

        realmObjectCopy.realmSet$venueId(realmObjectSource.realmGet$venueId());
        realmObjectCopy.realmSet$location(realmObjectSource.realmGet$location());
        realmObjectCopy.realmSet$fromDate(realmObjectSource.realmGet$fromDate());
        realmObjectCopy.realmSet$toDate(realmObjectSource.realmGet$toDate());
        return realmObject;
    }

    public static long insert(Realm realm, twj.lastlocal.in.twj.models.Venue object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(twj.lastlocal.in.twj.models.Venue.class);
        long tableNativePtr = table.getNativePtr();
        VenueColumnInfo columnInfo = (VenueColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.Venue.class);
        long pkColumnIndex = columnInfo._idIndex;
        long rowIndex = Table.NO_MATCH;
        Object primaryKeyValue = ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$_id();
        if (primaryKeyValue != null) {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$_id());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$_id());
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$venueId = ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$venueId();
        if (realmGet$venueId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.venueIdIndex, rowIndex, realmGet$venueId, false);
        }
        String realmGet$location = ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$location();
        if (realmGet$location != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.locationIndex, rowIndex, realmGet$location, false);
        }
        String realmGet$fromDate = ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$fromDate();
        if (realmGet$fromDate != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.fromDateIndex, rowIndex, realmGet$fromDate, false);
        }
        String realmGet$toDate = ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$toDate();
        if (realmGet$toDate != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.toDateIndex, rowIndex, realmGet$toDate, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(twj.lastlocal.in.twj.models.Venue.class);
        long tableNativePtr = table.getNativePtr();
        VenueColumnInfo columnInfo = (VenueColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.Venue.class);
        long pkColumnIndex = columnInfo._idIndex;
        twj.lastlocal.in.twj.models.Venue object = null;
        while (objects.hasNext()) {
            object = (twj.lastlocal.in.twj.models.Venue) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = Table.NO_MATCH;
            Object primaryKeyValue = ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$_id();
            if (primaryKeyValue != null) {
                rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$_id());
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$_id());
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$venueId = ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$venueId();
            if (realmGet$venueId != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.venueIdIndex, rowIndex, realmGet$venueId, false);
            }
            String realmGet$location = ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$location();
            if (realmGet$location != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.locationIndex, rowIndex, realmGet$location, false);
            }
            String realmGet$fromDate = ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$fromDate();
            if (realmGet$fromDate != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.fromDateIndex, rowIndex, realmGet$fromDate, false);
            }
            String realmGet$toDate = ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$toDate();
            if (realmGet$toDate != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.toDateIndex, rowIndex, realmGet$toDate, false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, twj.lastlocal.in.twj.models.Venue object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(twj.lastlocal.in.twj.models.Venue.class);
        long tableNativePtr = table.getNativePtr();
        VenueColumnInfo columnInfo = (VenueColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.Venue.class);
        long pkColumnIndex = columnInfo._idIndex;
        long rowIndex = Table.NO_MATCH;
        Object primaryKeyValue = ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$_id();
        if (primaryKeyValue != null) {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$_id());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$_id());
        }
        cache.put(object, rowIndex);
        String realmGet$venueId = ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$venueId();
        if (realmGet$venueId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.venueIdIndex, rowIndex, realmGet$venueId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.venueIdIndex, rowIndex, false);
        }
        String realmGet$location = ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$location();
        if (realmGet$location != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.locationIndex, rowIndex, realmGet$location, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.locationIndex, rowIndex, false);
        }
        String realmGet$fromDate = ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$fromDate();
        if (realmGet$fromDate != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.fromDateIndex, rowIndex, realmGet$fromDate, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.fromDateIndex, rowIndex, false);
        }
        String realmGet$toDate = ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$toDate();
        if (realmGet$toDate != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.toDateIndex, rowIndex, realmGet$toDate, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.toDateIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(twj.lastlocal.in.twj.models.Venue.class);
        long tableNativePtr = table.getNativePtr();
        VenueColumnInfo columnInfo = (VenueColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.Venue.class);
        long pkColumnIndex = columnInfo._idIndex;
        twj.lastlocal.in.twj.models.Venue object = null;
        while (objects.hasNext()) {
            object = (twj.lastlocal.in.twj.models.Venue) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = Table.NO_MATCH;
            Object primaryKeyValue = ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$_id();
            if (primaryKeyValue != null) {
                rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$_id());
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$_id());
            }
            cache.put(object, rowIndex);
            String realmGet$venueId = ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$venueId();
            if (realmGet$venueId != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.venueIdIndex, rowIndex, realmGet$venueId, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.venueIdIndex, rowIndex, false);
            }
            String realmGet$location = ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$location();
            if (realmGet$location != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.locationIndex, rowIndex, realmGet$location, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.locationIndex, rowIndex, false);
            }
            String realmGet$fromDate = ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$fromDate();
            if (realmGet$fromDate != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.fromDateIndex, rowIndex, realmGet$fromDate, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.fromDateIndex, rowIndex, false);
            }
            String realmGet$toDate = ((twj_lastlocal_in_twj_models_VenueRealmProxyInterface) object).realmGet$toDate();
            if (realmGet$toDate != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.toDateIndex, rowIndex, realmGet$toDate, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.toDateIndex, rowIndex, false);
            }
        }
    }

    public static twj.lastlocal.in.twj.models.Venue createDetachedCopy(twj.lastlocal.in.twj.models.Venue realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        twj.lastlocal.in.twj.models.Venue unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new twj.lastlocal.in.twj.models.Venue();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (twj.lastlocal.in.twj.models.Venue) cachedObject.object;
            }
            unmanagedObject = (twj.lastlocal.in.twj.models.Venue) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        twj_lastlocal_in_twj_models_VenueRealmProxyInterface unmanagedCopy = (twj_lastlocal_in_twj_models_VenueRealmProxyInterface) unmanagedObject;
        twj_lastlocal_in_twj_models_VenueRealmProxyInterface realmSource = (twj_lastlocal_in_twj_models_VenueRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$_id(realmSource.realmGet$_id());
        unmanagedCopy.realmSet$venueId(realmSource.realmGet$venueId());
        unmanagedCopy.realmSet$location(realmSource.realmGet$location());
        unmanagedCopy.realmSet$fromDate(realmSource.realmGet$fromDate());
        unmanagedCopy.realmSet$toDate(realmSource.realmGet$toDate());

        return unmanagedObject;
    }

    static twj.lastlocal.in.twj.models.Venue update(Realm realm, twj.lastlocal.in.twj.models.Venue realmObject, twj.lastlocal.in.twj.models.Venue newObject, Map<RealmModel, RealmObjectProxy> cache) {
        twj_lastlocal_in_twj_models_VenueRealmProxyInterface realmObjectTarget = (twj_lastlocal_in_twj_models_VenueRealmProxyInterface) realmObject;
        twj_lastlocal_in_twj_models_VenueRealmProxyInterface realmObjectSource = (twj_lastlocal_in_twj_models_VenueRealmProxyInterface) newObject;
        realmObjectTarget.realmSet$venueId(realmObjectSource.realmGet$venueId());
        realmObjectTarget.realmSet$location(realmObjectSource.realmGet$location());
        realmObjectTarget.realmSet$fromDate(realmObjectSource.realmGet$fromDate());
        realmObjectTarget.realmSet$toDate(realmObjectSource.realmGet$toDate());
        return realmObject;
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        twj_lastlocal_in_twj_models_VenueRealmProxy aVenue = (twj_lastlocal_in_twj_models_VenueRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aVenue.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aVenue.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aVenue.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }
}

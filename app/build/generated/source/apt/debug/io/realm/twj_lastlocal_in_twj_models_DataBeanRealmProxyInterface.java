package io.realm;


public interface twj_lastlocal_in_twj_models_DataBeanRealmProxyInterface {
    public RealmList<twj.lastlocal.in.twj.models.CounterUser> realmGet$counterUserRealmList();
    public void realmSet$counterUserRealmList(RealmList<twj.lastlocal.in.twj.models.CounterUser> value);
    public RealmList<twj.lastlocal.in.twj.models.Venue> realmGet$venueRealmList();
    public void realmSet$venueRealmList(RealmList<twj.lastlocal.in.twj.models.Venue> value);
    public RealmList<twj.lastlocal.in.twj.models.Visitor> realmGet$visitorRealmList();
    public void realmSet$visitorRealmList(RealmList<twj.lastlocal.in.twj.models.Visitor> value);
}

package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.ProxyUtils;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxy extends twj.lastlocal.in.twj.models.ExibitionDateBean
    implements RealmObjectProxy, twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface {

    static final class ExibitionDateBeanColumnInfo extends ColumnInfo {
        long dateIndex;
        long timeIndex;
        long attendIndex;
        long venueIdIndex;
        long approvedByIndex;
        long isExibitionIndex;
        long isFashionIndex;

        ExibitionDateBeanColumnInfo(OsSchemaInfo schemaInfo) {
            super(7);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("ExibitionDateBean");
            this.dateIndex = addColumnDetails("date", "date", objectSchemaInfo);
            this.timeIndex = addColumnDetails("time", "time", objectSchemaInfo);
            this.attendIndex = addColumnDetails("attend", "attend", objectSchemaInfo);
            this.venueIdIndex = addColumnDetails("venueId", "venueId", objectSchemaInfo);
            this.approvedByIndex = addColumnDetails("approvedBy", "approvedBy", objectSchemaInfo);
            this.isExibitionIndex = addColumnDetails("isExibition", "isExibition", objectSchemaInfo);
            this.isFashionIndex = addColumnDetails("isFashion", "isFashion", objectSchemaInfo);
        }

        ExibitionDateBeanColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new ExibitionDateBeanColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final ExibitionDateBeanColumnInfo src = (ExibitionDateBeanColumnInfo) rawSrc;
            final ExibitionDateBeanColumnInfo dst = (ExibitionDateBeanColumnInfo) rawDst;
            dst.dateIndex = src.dateIndex;
            dst.timeIndex = src.timeIndex;
            dst.attendIndex = src.attendIndex;
            dst.venueIdIndex = src.venueIdIndex;
            dst.approvedByIndex = src.approvedByIndex;
            dst.isExibitionIndex = src.isExibitionIndex;
            dst.isFashionIndex = src.isFashionIndex;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();

    private ExibitionDateBeanColumnInfo columnInfo;
    private ProxyState<twj.lastlocal.in.twj.models.ExibitionDateBean> proxyState;

    twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (ExibitionDateBeanColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<twj.lastlocal.in.twj.models.ExibitionDateBean>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$date() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.dateIndex);
    }

    @Override
    public void realmSet$date(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.dateIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.dateIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.dateIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.dateIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$time() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.timeIndex);
    }

    @Override
    public void realmSet$time(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.timeIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.timeIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.timeIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.timeIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$attend() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.attendIndex);
    }

    @Override
    public void realmSet$attend(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.attendIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.attendIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$venueId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.venueIdIndex);
    }

    @Override
    public void realmSet$venueId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.venueIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.venueIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.venueIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.venueIdIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$approvedBy() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.approvedByIndex);
    }

    @Override
    public void realmSet$approvedBy(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.approvedByIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.approvedByIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.approvedByIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.approvedByIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$isExibition() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.isExibitionIndex);
    }

    @Override
    public void realmSet$isExibition(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.isExibitionIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.isExibitionIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$isFashion() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.isFashionIndex);
    }

    @Override
    public void realmSet$isFashion(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.isFashionIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.isFashionIndex, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("ExibitionDateBean", 7, 0);
        builder.addPersistedProperty("date", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("time", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("attend", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("venueId", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("approvedBy", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("isExibition", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("isFashion", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static ExibitionDateBeanColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new ExibitionDateBeanColumnInfo(schemaInfo);
    }

    public static String getSimpleClassName() {
        return "ExibitionDateBean";
    }

    public static final class ClassNameHelper {
        public static final String INTERNAL_CLASS_NAME = "ExibitionDateBean";
    }

    @SuppressWarnings("cast")
    public static twj.lastlocal.in.twj.models.ExibitionDateBean createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        twj.lastlocal.in.twj.models.ExibitionDateBean obj = realm.createObjectInternal(twj.lastlocal.in.twj.models.ExibitionDateBean.class, true, excludeFields);

        final twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface objProxy = (twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) obj;
        if (json.has("date")) {
            if (json.isNull("date")) {
                objProxy.realmSet$date(null);
            } else {
                objProxy.realmSet$date((String) json.getString("date"));
            }
        }
        if (json.has("time")) {
            if (json.isNull("time")) {
                objProxy.realmSet$time(null);
            } else {
                objProxy.realmSet$time((String) json.getString("time"));
            }
        }
        if (json.has("attend")) {
            if (json.isNull("attend")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'attend' to null.");
            } else {
                objProxy.realmSet$attend((int) json.getInt("attend"));
            }
        }
        if (json.has("venueId")) {
            if (json.isNull("venueId")) {
                objProxy.realmSet$venueId(null);
            } else {
                objProxy.realmSet$venueId((String) json.getString("venueId"));
            }
        }
        if (json.has("approvedBy")) {
            if (json.isNull("approvedBy")) {
                objProxy.realmSet$approvedBy(null);
            } else {
                objProxy.realmSet$approvedBy((String) json.getString("approvedBy"));
            }
        }
        if (json.has("isExibition")) {
            if (json.isNull("isExibition")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'isExibition' to null.");
            } else {
                objProxy.realmSet$isExibition((int) json.getInt("isExibition"));
            }
        }
        if (json.has("isFashion")) {
            if (json.isNull("isFashion")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'isFashion' to null.");
            } else {
                objProxy.realmSet$isFashion((int) json.getInt("isFashion"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static twj.lastlocal.in.twj.models.ExibitionDateBean createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        final twj.lastlocal.in.twj.models.ExibitionDateBean obj = new twj.lastlocal.in.twj.models.ExibitionDateBean();
        final twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface objProxy = (twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("date")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$date((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$date(null);
                }
            } else if (name.equals("time")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$time((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$time(null);
                }
            } else if (name.equals("attend")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$attend((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'attend' to null.");
                }
            } else if (name.equals("venueId")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$venueId((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$venueId(null);
                }
            } else if (name.equals("approvedBy")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$approvedBy((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$approvedBy(null);
                }
            } else if (name.equals("isExibition")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$isExibition((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'isExibition' to null.");
                }
            } else if (name.equals("isFashion")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$isFashion((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'isFashion' to null.");
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return realm.copyToRealm(obj);
    }

    public static twj.lastlocal.in.twj.models.ExibitionDateBean copyOrUpdate(Realm realm, twj.lastlocal.in.twj.models.ExibitionDateBean object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (twj.lastlocal.in.twj.models.ExibitionDateBean) cachedRealmObject;
        }

        return copy(realm, object, update, cache);
    }

    public static twj.lastlocal.in.twj.models.ExibitionDateBean copy(Realm realm, twj.lastlocal.in.twj.models.ExibitionDateBean newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (twj.lastlocal.in.twj.models.ExibitionDateBean) cachedRealmObject;
        }

        // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
        twj.lastlocal.in.twj.models.ExibitionDateBean realmObject = realm.createObjectInternal(twj.lastlocal.in.twj.models.ExibitionDateBean.class, false, Collections.<String>emptyList());
        cache.put(newObject, (RealmObjectProxy) realmObject);

        twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface realmObjectSource = (twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) newObject;
        twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface realmObjectCopy = (twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) realmObject;

        realmObjectCopy.realmSet$date(realmObjectSource.realmGet$date());
        realmObjectCopy.realmSet$time(realmObjectSource.realmGet$time());
        realmObjectCopy.realmSet$attend(realmObjectSource.realmGet$attend());
        realmObjectCopy.realmSet$venueId(realmObjectSource.realmGet$venueId());
        realmObjectCopy.realmSet$approvedBy(realmObjectSource.realmGet$approvedBy());
        realmObjectCopy.realmSet$isExibition(realmObjectSource.realmGet$isExibition());
        realmObjectCopy.realmSet$isFashion(realmObjectSource.realmGet$isFashion());
        return realmObject;
    }

    public static long insert(Realm realm, twj.lastlocal.in.twj.models.ExibitionDateBean object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(twj.lastlocal.in.twj.models.ExibitionDateBean.class);
        long tableNativePtr = table.getNativePtr();
        ExibitionDateBeanColumnInfo columnInfo = (ExibitionDateBeanColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.ExibitionDateBean.class);
        long rowIndex = OsObject.createRow(table);
        cache.put(object, rowIndex);
        String realmGet$date = ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$date();
        if (realmGet$date != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.dateIndex, rowIndex, realmGet$date, false);
        }
        String realmGet$time = ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$time();
        if (realmGet$time != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.timeIndex, rowIndex, realmGet$time, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.attendIndex, rowIndex, ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$attend(), false);
        String realmGet$venueId = ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$venueId();
        if (realmGet$venueId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.venueIdIndex, rowIndex, realmGet$venueId, false);
        }
        String realmGet$approvedBy = ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$approvedBy();
        if (realmGet$approvedBy != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.approvedByIndex, rowIndex, realmGet$approvedBy, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.isExibitionIndex, rowIndex, ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$isExibition(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.isFashionIndex, rowIndex, ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$isFashion(), false);
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(twj.lastlocal.in.twj.models.ExibitionDateBean.class);
        long tableNativePtr = table.getNativePtr();
        ExibitionDateBeanColumnInfo columnInfo = (ExibitionDateBeanColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.ExibitionDateBean.class);
        twj.lastlocal.in.twj.models.ExibitionDateBean object = null;
        while (objects.hasNext()) {
            object = (twj.lastlocal.in.twj.models.ExibitionDateBean) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = OsObject.createRow(table);
            cache.put(object, rowIndex);
            String realmGet$date = ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$date();
            if (realmGet$date != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.dateIndex, rowIndex, realmGet$date, false);
            }
            String realmGet$time = ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$time();
            if (realmGet$time != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.timeIndex, rowIndex, realmGet$time, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.attendIndex, rowIndex, ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$attend(), false);
            String realmGet$venueId = ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$venueId();
            if (realmGet$venueId != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.venueIdIndex, rowIndex, realmGet$venueId, false);
            }
            String realmGet$approvedBy = ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$approvedBy();
            if (realmGet$approvedBy != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.approvedByIndex, rowIndex, realmGet$approvedBy, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.isExibitionIndex, rowIndex, ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$isExibition(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.isFashionIndex, rowIndex, ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$isFashion(), false);
        }
    }

    public static long insertOrUpdate(Realm realm, twj.lastlocal.in.twj.models.ExibitionDateBean object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(twj.lastlocal.in.twj.models.ExibitionDateBean.class);
        long tableNativePtr = table.getNativePtr();
        ExibitionDateBeanColumnInfo columnInfo = (ExibitionDateBeanColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.ExibitionDateBean.class);
        long rowIndex = OsObject.createRow(table);
        cache.put(object, rowIndex);
        String realmGet$date = ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$date();
        if (realmGet$date != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.dateIndex, rowIndex, realmGet$date, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.dateIndex, rowIndex, false);
        }
        String realmGet$time = ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$time();
        if (realmGet$time != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.timeIndex, rowIndex, realmGet$time, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.timeIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.attendIndex, rowIndex, ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$attend(), false);
        String realmGet$venueId = ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$venueId();
        if (realmGet$venueId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.venueIdIndex, rowIndex, realmGet$venueId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.venueIdIndex, rowIndex, false);
        }
        String realmGet$approvedBy = ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$approvedBy();
        if (realmGet$approvedBy != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.approvedByIndex, rowIndex, realmGet$approvedBy, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.approvedByIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.isExibitionIndex, rowIndex, ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$isExibition(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.isFashionIndex, rowIndex, ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$isFashion(), false);
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(twj.lastlocal.in.twj.models.ExibitionDateBean.class);
        long tableNativePtr = table.getNativePtr();
        ExibitionDateBeanColumnInfo columnInfo = (ExibitionDateBeanColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.ExibitionDateBean.class);
        twj.lastlocal.in.twj.models.ExibitionDateBean object = null;
        while (objects.hasNext()) {
            object = (twj.lastlocal.in.twj.models.ExibitionDateBean) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = OsObject.createRow(table);
            cache.put(object, rowIndex);
            String realmGet$date = ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$date();
            if (realmGet$date != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.dateIndex, rowIndex, realmGet$date, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.dateIndex, rowIndex, false);
            }
            String realmGet$time = ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$time();
            if (realmGet$time != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.timeIndex, rowIndex, realmGet$time, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.timeIndex, rowIndex, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.attendIndex, rowIndex, ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$attend(), false);
            String realmGet$venueId = ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$venueId();
            if (realmGet$venueId != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.venueIdIndex, rowIndex, realmGet$venueId, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.venueIdIndex, rowIndex, false);
            }
            String realmGet$approvedBy = ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$approvedBy();
            if (realmGet$approvedBy != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.approvedByIndex, rowIndex, realmGet$approvedBy, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.approvedByIndex, rowIndex, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.isExibitionIndex, rowIndex, ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$isExibition(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.isFashionIndex, rowIndex, ((twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) object).realmGet$isFashion(), false);
        }
    }

    public static twj.lastlocal.in.twj.models.ExibitionDateBean createDetachedCopy(twj.lastlocal.in.twj.models.ExibitionDateBean realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        twj.lastlocal.in.twj.models.ExibitionDateBean unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new twj.lastlocal.in.twj.models.ExibitionDateBean();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (twj.lastlocal.in.twj.models.ExibitionDateBean) cachedObject.object;
            }
            unmanagedObject = (twj.lastlocal.in.twj.models.ExibitionDateBean) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface unmanagedCopy = (twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) unmanagedObject;
        twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface realmSource = (twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$date(realmSource.realmGet$date());
        unmanagedCopy.realmSet$time(realmSource.realmGet$time());
        unmanagedCopy.realmSet$attend(realmSource.realmGet$attend());
        unmanagedCopy.realmSet$venueId(realmSource.realmGet$venueId());
        unmanagedCopy.realmSet$approvedBy(realmSource.realmGet$approvedBy());
        unmanagedCopy.realmSet$isExibition(realmSource.realmGet$isExibition());
        unmanagedCopy.realmSet$isFashion(realmSource.realmGet$isFashion());

        return unmanagedObject;
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxy aExibitionDateBean = (twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aExibitionDateBean.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aExibitionDateBean.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aExibitionDateBean.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }
}

package io.realm;


import android.util.JsonReader;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmProxyMediator;
import io.realm.internal.Row;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

@io.realm.annotations.RealmModule
class DefaultRealmModuleMediator extends RealmProxyMediator {

    private static final Set<Class<? extends RealmModel>> MODEL_CLASSES;
    static {
        Set<Class<? extends RealmModel>> modelClasses = new HashSet<Class<? extends RealmModel>>(6);
        modelClasses.add(twj.lastlocal.in.twj.models.CounterUser.class);
        modelClasses.add(twj.lastlocal.in.twj.models.DataBean.class);
        modelClasses.add(twj.lastlocal.in.twj.models.ExibitionDateBean.class);
        modelClasses.add(twj.lastlocal.in.twj.models.PullData.class);
        modelClasses.add(twj.lastlocal.in.twj.models.Venue.class);
        modelClasses.add(twj.lastlocal.in.twj.models.Visitor.class);
        MODEL_CLASSES = Collections.unmodifiableSet(modelClasses);
    }

    @Override
    public Map<Class<? extends RealmModel>, OsObjectSchemaInfo> getExpectedObjectSchemaInfoMap() {
        Map<Class<? extends RealmModel>, OsObjectSchemaInfo> infoMap = new HashMap<Class<? extends RealmModel>, OsObjectSchemaInfo>(6);
        infoMap.put(twj.lastlocal.in.twj.models.CounterUser.class, io.realm.twj_lastlocal_in_twj_models_CounterUserRealmProxy.getExpectedObjectSchemaInfo());
        infoMap.put(twj.lastlocal.in.twj.models.DataBean.class, io.realm.twj_lastlocal_in_twj_models_DataBeanRealmProxy.getExpectedObjectSchemaInfo());
        infoMap.put(twj.lastlocal.in.twj.models.ExibitionDateBean.class, io.realm.twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxy.getExpectedObjectSchemaInfo());
        infoMap.put(twj.lastlocal.in.twj.models.PullData.class, io.realm.twj_lastlocal_in_twj_models_PullDataRealmProxy.getExpectedObjectSchemaInfo());
        infoMap.put(twj.lastlocal.in.twj.models.Venue.class, io.realm.twj_lastlocal_in_twj_models_VenueRealmProxy.getExpectedObjectSchemaInfo());
        infoMap.put(twj.lastlocal.in.twj.models.Visitor.class, io.realm.twj_lastlocal_in_twj_models_VisitorRealmProxy.getExpectedObjectSchemaInfo());
        return infoMap;
    }

    @Override
    public ColumnInfo createColumnInfo(Class<? extends RealmModel> clazz, OsSchemaInfo schemaInfo) {
        checkClass(clazz);

        if (clazz.equals(twj.lastlocal.in.twj.models.CounterUser.class)) {
            return io.realm.twj_lastlocal_in_twj_models_CounterUserRealmProxy.createColumnInfo(schemaInfo);
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.DataBean.class)) {
            return io.realm.twj_lastlocal_in_twj_models_DataBeanRealmProxy.createColumnInfo(schemaInfo);
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.ExibitionDateBean.class)) {
            return io.realm.twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxy.createColumnInfo(schemaInfo);
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.PullData.class)) {
            return io.realm.twj_lastlocal_in_twj_models_PullDataRealmProxy.createColumnInfo(schemaInfo);
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.Venue.class)) {
            return io.realm.twj_lastlocal_in_twj_models_VenueRealmProxy.createColumnInfo(schemaInfo);
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.Visitor.class)) {
            return io.realm.twj_lastlocal_in_twj_models_VisitorRealmProxy.createColumnInfo(schemaInfo);
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public String getSimpleClassNameImpl(Class<? extends RealmModel> clazz) {
        checkClass(clazz);

        if (clazz.equals(twj.lastlocal.in.twj.models.CounterUser.class)) {
            return "CounterUser";
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.DataBean.class)) {
            return "DataBean";
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.ExibitionDateBean.class)) {
            return "ExibitionDateBean";
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.PullData.class)) {
            return "PullData";
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.Venue.class)) {
            return "Venue";
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.Visitor.class)) {
            return "Visitor";
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public <E extends RealmModel> E newInstance(Class<E> clazz, Object baseRealm, Row row, ColumnInfo columnInfo, boolean acceptDefaultValue, List<String> excludeFields) {
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        try {
            objectContext.set((BaseRealm) baseRealm, row, columnInfo, acceptDefaultValue, excludeFields);
            checkClass(clazz);

            if (clazz.equals(twj.lastlocal.in.twj.models.CounterUser.class)) {
                return clazz.cast(new io.realm.twj_lastlocal_in_twj_models_CounterUserRealmProxy());
            }
            if (clazz.equals(twj.lastlocal.in.twj.models.DataBean.class)) {
                return clazz.cast(new io.realm.twj_lastlocal_in_twj_models_DataBeanRealmProxy());
            }
            if (clazz.equals(twj.lastlocal.in.twj.models.ExibitionDateBean.class)) {
                return clazz.cast(new io.realm.twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxy());
            }
            if (clazz.equals(twj.lastlocal.in.twj.models.PullData.class)) {
                return clazz.cast(new io.realm.twj_lastlocal_in_twj_models_PullDataRealmProxy());
            }
            if (clazz.equals(twj.lastlocal.in.twj.models.Venue.class)) {
                return clazz.cast(new io.realm.twj_lastlocal_in_twj_models_VenueRealmProxy());
            }
            if (clazz.equals(twj.lastlocal.in.twj.models.Visitor.class)) {
                return clazz.cast(new io.realm.twj_lastlocal_in_twj_models_VisitorRealmProxy());
            }
            throw getMissingProxyClassException(clazz);
        } finally {
            objectContext.clear();
        }
    }

    @Override
    public Set<Class<? extends RealmModel>> getModelClasses() {
        return MODEL_CLASSES;
    }

    @Override
    public <E extends RealmModel> E copyOrUpdate(Realm realm, E obj, boolean update, Map<RealmModel, RealmObjectProxy> cache) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<E> clazz = (Class<E>) ((obj instanceof RealmObjectProxy) ? obj.getClass().getSuperclass() : obj.getClass());

        if (clazz.equals(twj.lastlocal.in.twj.models.CounterUser.class)) {
            return clazz.cast(io.realm.twj_lastlocal_in_twj_models_CounterUserRealmProxy.copyOrUpdate(realm, (twj.lastlocal.in.twj.models.CounterUser) obj, update, cache));
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.DataBean.class)) {
            return clazz.cast(io.realm.twj_lastlocal_in_twj_models_DataBeanRealmProxy.copyOrUpdate(realm, (twj.lastlocal.in.twj.models.DataBean) obj, update, cache));
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.ExibitionDateBean.class)) {
            return clazz.cast(io.realm.twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxy.copyOrUpdate(realm, (twj.lastlocal.in.twj.models.ExibitionDateBean) obj, update, cache));
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.PullData.class)) {
            return clazz.cast(io.realm.twj_lastlocal_in_twj_models_PullDataRealmProxy.copyOrUpdate(realm, (twj.lastlocal.in.twj.models.PullData) obj, update, cache));
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.Venue.class)) {
            return clazz.cast(io.realm.twj_lastlocal_in_twj_models_VenueRealmProxy.copyOrUpdate(realm, (twj.lastlocal.in.twj.models.Venue) obj, update, cache));
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.Visitor.class)) {
            return clazz.cast(io.realm.twj_lastlocal_in_twj_models_VisitorRealmProxy.copyOrUpdate(realm, (twj.lastlocal.in.twj.models.Visitor) obj, update, cache));
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public void insert(Realm realm, RealmModel object, Map<RealmModel, Long> cache) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((object instanceof RealmObjectProxy) ? object.getClass().getSuperclass() : object.getClass());

        if (clazz.equals(twj.lastlocal.in.twj.models.CounterUser.class)) {
            io.realm.twj_lastlocal_in_twj_models_CounterUserRealmProxy.insert(realm, (twj.lastlocal.in.twj.models.CounterUser) object, cache);
        } else if (clazz.equals(twj.lastlocal.in.twj.models.DataBean.class)) {
            io.realm.twj_lastlocal_in_twj_models_DataBeanRealmProxy.insert(realm, (twj.lastlocal.in.twj.models.DataBean) object, cache);
        } else if (clazz.equals(twj.lastlocal.in.twj.models.ExibitionDateBean.class)) {
            io.realm.twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxy.insert(realm, (twj.lastlocal.in.twj.models.ExibitionDateBean) object, cache);
        } else if (clazz.equals(twj.lastlocal.in.twj.models.PullData.class)) {
            io.realm.twj_lastlocal_in_twj_models_PullDataRealmProxy.insert(realm, (twj.lastlocal.in.twj.models.PullData) object, cache);
        } else if (clazz.equals(twj.lastlocal.in.twj.models.Venue.class)) {
            io.realm.twj_lastlocal_in_twj_models_VenueRealmProxy.insert(realm, (twj.lastlocal.in.twj.models.Venue) object, cache);
        } else if (clazz.equals(twj.lastlocal.in.twj.models.Visitor.class)) {
            io.realm.twj_lastlocal_in_twj_models_VisitorRealmProxy.insert(realm, (twj.lastlocal.in.twj.models.Visitor) object, cache);
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public void insert(Realm realm, Collection<? extends RealmModel> objects) {
        Iterator<? extends RealmModel> iterator = objects.iterator();
        RealmModel object = null;
        Map<RealmModel, Long> cache = new HashMap<RealmModel, Long>(objects.size());
        if (iterator.hasNext()) {
            //  access the first element to figure out the clazz for the routing below
            object = iterator.next();
            // This cast is correct because obj is either
            // generated by RealmProxy or the original type extending directly from RealmObject
            @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((object instanceof RealmObjectProxy) ? object.getClass().getSuperclass() : object.getClass());

            if (clazz.equals(twj.lastlocal.in.twj.models.CounterUser.class)) {
                io.realm.twj_lastlocal_in_twj_models_CounterUserRealmProxy.insert(realm, (twj.lastlocal.in.twj.models.CounterUser) object, cache);
            } else if (clazz.equals(twj.lastlocal.in.twj.models.DataBean.class)) {
                io.realm.twj_lastlocal_in_twj_models_DataBeanRealmProxy.insert(realm, (twj.lastlocal.in.twj.models.DataBean) object, cache);
            } else if (clazz.equals(twj.lastlocal.in.twj.models.ExibitionDateBean.class)) {
                io.realm.twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxy.insert(realm, (twj.lastlocal.in.twj.models.ExibitionDateBean) object, cache);
            } else if (clazz.equals(twj.lastlocal.in.twj.models.PullData.class)) {
                io.realm.twj_lastlocal_in_twj_models_PullDataRealmProxy.insert(realm, (twj.lastlocal.in.twj.models.PullData) object, cache);
            } else if (clazz.equals(twj.lastlocal.in.twj.models.Venue.class)) {
                io.realm.twj_lastlocal_in_twj_models_VenueRealmProxy.insert(realm, (twj.lastlocal.in.twj.models.Venue) object, cache);
            } else if (clazz.equals(twj.lastlocal.in.twj.models.Visitor.class)) {
                io.realm.twj_lastlocal_in_twj_models_VisitorRealmProxy.insert(realm, (twj.lastlocal.in.twj.models.Visitor) object, cache);
            } else {
                throw getMissingProxyClassException(clazz);
            }
            if (iterator.hasNext()) {
                if (clazz.equals(twj.lastlocal.in.twj.models.CounterUser.class)) {
                    io.realm.twj_lastlocal_in_twj_models_CounterUserRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(twj.lastlocal.in.twj.models.DataBean.class)) {
                    io.realm.twj_lastlocal_in_twj_models_DataBeanRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(twj.lastlocal.in.twj.models.ExibitionDateBean.class)) {
                    io.realm.twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(twj.lastlocal.in.twj.models.PullData.class)) {
                    io.realm.twj_lastlocal_in_twj_models_PullDataRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(twj.lastlocal.in.twj.models.Venue.class)) {
                    io.realm.twj_lastlocal_in_twj_models_VenueRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(twj.lastlocal.in.twj.models.Visitor.class)) {
                    io.realm.twj_lastlocal_in_twj_models_VisitorRealmProxy.insert(realm, iterator, cache);
                } else {
                    throw getMissingProxyClassException(clazz);
                }
            }
        }
    }

    @Override
    public void insertOrUpdate(Realm realm, RealmModel obj, Map<RealmModel, Long> cache) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((obj instanceof RealmObjectProxy) ? obj.getClass().getSuperclass() : obj.getClass());

        if (clazz.equals(twj.lastlocal.in.twj.models.CounterUser.class)) {
            io.realm.twj_lastlocal_in_twj_models_CounterUserRealmProxy.insertOrUpdate(realm, (twj.lastlocal.in.twj.models.CounterUser) obj, cache);
        } else if (clazz.equals(twj.lastlocal.in.twj.models.DataBean.class)) {
            io.realm.twj_lastlocal_in_twj_models_DataBeanRealmProxy.insertOrUpdate(realm, (twj.lastlocal.in.twj.models.DataBean) obj, cache);
        } else if (clazz.equals(twj.lastlocal.in.twj.models.ExibitionDateBean.class)) {
            io.realm.twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxy.insertOrUpdate(realm, (twj.lastlocal.in.twj.models.ExibitionDateBean) obj, cache);
        } else if (clazz.equals(twj.lastlocal.in.twj.models.PullData.class)) {
            io.realm.twj_lastlocal_in_twj_models_PullDataRealmProxy.insertOrUpdate(realm, (twj.lastlocal.in.twj.models.PullData) obj, cache);
        } else if (clazz.equals(twj.lastlocal.in.twj.models.Venue.class)) {
            io.realm.twj_lastlocal_in_twj_models_VenueRealmProxy.insertOrUpdate(realm, (twj.lastlocal.in.twj.models.Venue) obj, cache);
        } else if (clazz.equals(twj.lastlocal.in.twj.models.Visitor.class)) {
            io.realm.twj_lastlocal_in_twj_models_VisitorRealmProxy.insertOrUpdate(realm, (twj.lastlocal.in.twj.models.Visitor) obj, cache);
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public void insertOrUpdate(Realm realm, Collection<? extends RealmModel> objects) {
        Iterator<? extends RealmModel> iterator = objects.iterator();
        RealmModel object = null;
        Map<RealmModel, Long> cache = new HashMap<RealmModel, Long>(objects.size());
        if (iterator.hasNext()) {
            //  access the first element to figure out the clazz for the routing below
            object = iterator.next();
            // This cast is correct because obj is either
            // generated by RealmProxy or the original type extending directly from RealmObject
            @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((object instanceof RealmObjectProxy) ? object.getClass().getSuperclass() : object.getClass());

            if (clazz.equals(twj.lastlocal.in.twj.models.CounterUser.class)) {
                io.realm.twj_lastlocal_in_twj_models_CounterUserRealmProxy.insertOrUpdate(realm, (twj.lastlocal.in.twj.models.CounterUser) object, cache);
            } else if (clazz.equals(twj.lastlocal.in.twj.models.DataBean.class)) {
                io.realm.twj_lastlocal_in_twj_models_DataBeanRealmProxy.insertOrUpdate(realm, (twj.lastlocal.in.twj.models.DataBean) object, cache);
            } else if (clazz.equals(twj.lastlocal.in.twj.models.ExibitionDateBean.class)) {
                io.realm.twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxy.insertOrUpdate(realm, (twj.lastlocal.in.twj.models.ExibitionDateBean) object, cache);
            } else if (clazz.equals(twj.lastlocal.in.twj.models.PullData.class)) {
                io.realm.twj_lastlocal_in_twj_models_PullDataRealmProxy.insertOrUpdate(realm, (twj.lastlocal.in.twj.models.PullData) object, cache);
            } else if (clazz.equals(twj.lastlocal.in.twj.models.Venue.class)) {
                io.realm.twj_lastlocal_in_twj_models_VenueRealmProxy.insertOrUpdate(realm, (twj.lastlocal.in.twj.models.Venue) object, cache);
            } else if (clazz.equals(twj.lastlocal.in.twj.models.Visitor.class)) {
                io.realm.twj_lastlocal_in_twj_models_VisitorRealmProxy.insertOrUpdate(realm, (twj.lastlocal.in.twj.models.Visitor) object, cache);
            } else {
                throw getMissingProxyClassException(clazz);
            }
            if (iterator.hasNext()) {
                if (clazz.equals(twj.lastlocal.in.twj.models.CounterUser.class)) {
                    io.realm.twj_lastlocal_in_twj_models_CounterUserRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(twj.lastlocal.in.twj.models.DataBean.class)) {
                    io.realm.twj_lastlocal_in_twj_models_DataBeanRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(twj.lastlocal.in.twj.models.ExibitionDateBean.class)) {
                    io.realm.twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(twj.lastlocal.in.twj.models.PullData.class)) {
                    io.realm.twj_lastlocal_in_twj_models_PullDataRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(twj.lastlocal.in.twj.models.Venue.class)) {
                    io.realm.twj_lastlocal_in_twj_models_VenueRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(twj.lastlocal.in.twj.models.Visitor.class)) {
                    io.realm.twj_lastlocal_in_twj_models_VisitorRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else {
                    throw getMissingProxyClassException(clazz);
                }
            }
        }
    }

    @Override
    public <E extends RealmModel> E createOrUpdateUsingJsonObject(Class<E> clazz, Realm realm, JSONObject json, boolean update)
        throws JSONException {
        checkClass(clazz);

        if (clazz.equals(twj.lastlocal.in.twj.models.CounterUser.class)) {
            return clazz.cast(io.realm.twj_lastlocal_in_twj_models_CounterUserRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.DataBean.class)) {
            return clazz.cast(io.realm.twj_lastlocal_in_twj_models_DataBeanRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.ExibitionDateBean.class)) {
            return clazz.cast(io.realm.twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.PullData.class)) {
            return clazz.cast(io.realm.twj_lastlocal_in_twj_models_PullDataRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.Venue.class)) {
            return clazz.cast(io.realm.twj_lastlocal_in_twj_models_VenueRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.Visitor.class)) {
            return clazz.cast(io.realm.twj_lastlocal_in_twj_models_VisitorRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public <E extends RealmModel> E createUsingJsonStream(Class<E> clazz, Realm realm, JsonReader reader)
        throws IOException {
        checkClass(clazz);

        if (clazz.equals(twj.lastlocal.in.twj.models.CounterUser.class)) {
            return clazz.cast(io.realm.twj_lastlocal_in_twj_models_CounterUserRealmProxy.createUsingJsonStream(realm, reader));
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.DataBean.class)) {
            return clazz.cast(io.realm.twj_lastlocal_in_twj_models_DataBeanRealmProxy.createUsingJsonStream(realm, reader));
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.ExibitionDateBean.class)) {
            return clazz.cast(io.realm.twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxy.createUsingJsonStream(realm, reader));
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.PullData.class)) {
            return clazz.cast(io.realm.twj_lastlocal_in_twj_models_PullDataRealmProxy.createUsingJsonStream(realm, reader));
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.Venue.class)) {
            return clazz.cast(io.realm.twj_lastlocal_in_twj_models_VenueRealmProxy.createUsingJsonStream(realm, reader));
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.Visitor.class)) {
            return clazz.cast(io.realm.twj_lastlocal_in_twj_models_VisitorRealmProxy.createUsingJsonStream(realm, reader));
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public <E extends RealmModel> E createDetachedCopy(E realmObject, int maxDepth, Map<RealmModel, RealmObjectProxy.CacheData<RealmModel>> cache) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<E> clazz = (Class<E>) realmObject.getClass().getSuperclass();

        if (clazz.equals(twj.lastlocal.in.twj.models.CounterUser.class)) {
            return clazz.cast(io.realm.twj_lastlocal_in_twj_models_CounterUserRealmProxy.createDetachedCopy((twj.lastlocal.in.twj.models.CounterUser) realmObject, 0, maxDepth, cache));
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.DataBean.class)) {
            return clazz.cast(io.realm.twj_lastlocal_in_twj_models_DataBeanRealmProxy.createDetachedCopy((twj.lastlocal.in.twj.models.DataBean) realmObject, 0, maxDepth, cache));
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.ExibitionDateBean.class)) {
            return clazz.cast(io.realm.twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxy.createDetachedCopy((twj.lastlocal.in.twj.models.ExibitionDateBean) realmObject, 0, maxDepth, cache));
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.PullData.class)) {
            return clazz.cast(io.realm.twj_lastlocal_in_twj_models_PullDataRealmProxy.createDetachedCopy((twj.lastlocal.in.twj.models.PullData) realmObject, 0, maxDepth, cache));
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.Venue.class)) {
            return clazz.cast(io.realm.twj_lastlocal_in_twj_models_VenueRealmProxy.createDetachedCopy((twj.lastlocal.in.twj.models.Venue) realmObject, 0, maxDepth, cache));
        }
        if (clazz.equals(twj.lastlocal.in.twj.models.Visitor.class)) {
            return clazz.cast(io.realm.twj_lastlocal_in_twj_models_VisitorRealmProxy.createDetachedCopy((twj.lastlocal.in.twj.models.Visitor) realmObject, 0, maxDepth, cache));
        }
        throw getMissingProxyClassException(clazz);
    }

}

package io.realm;


public interface twj_lastlocal_in_twj_models_VenueRealmProxyInterface {
    public long realmGet$_id();
    public void realmSet$_id(long value);
    public String realmGet$venueId();
    public void realmSet$venueId(String value);
    public String realmGet$location();
    public void realmSet$location(String value);
    public String realmGet$fromDate();
    public void realmSet$fromDate(String value);
    public String realmGet$toDate();
    public void realmSet$toDate(String value);
}

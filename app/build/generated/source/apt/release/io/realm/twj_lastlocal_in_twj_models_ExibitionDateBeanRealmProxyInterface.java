package io.realm;


public interface twj_lastlocal_in_twj_models_ExibitionDateBeanRealmProxyInterface {
    public String realmGet$date();
    public void realmSet$date(String value);
    public String realmGet$time();
    public void realmSet$time(String value);
    public int realmGet$attend();
    public void realmSet$attend(int value);
    public String realmGet$venueId();
    public void realmSet$venueId(String value);
    public String realmGet$approvedBy();
    public void realmSet$approvedBy(String value);
    public int realmGet$isExibition();
    public void realmSet$isExibition(int value);
    public int realmGet$isFashion();
    public void realmSet$isFashion(int value);
}

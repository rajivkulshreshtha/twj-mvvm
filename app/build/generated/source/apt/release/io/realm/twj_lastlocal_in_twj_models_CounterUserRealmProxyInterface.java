package io.realm;


public interface twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface {
    public long realmGet$_id();
    public void realmSet$_id(long value);
    public String realmGet$adminId();
    public void realmSet$adminId(String value);
    public String realmGet$emailId();
    public void realmSet$emailId(String value);
    public String realmGet$password();
    public void realmSet$password(String value);
    public String realmGet$fullname();
    public void realmSet$fullname(String value);
    public String realmGet$isEnable();
    public void realmSet$isEnable(String value);
    public String realmGet$apiKey();
    public void realmSet$apiKey(String value);
}

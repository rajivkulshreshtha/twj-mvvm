package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.ProxyUtils;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class twj_lastlocal_in_twj_models_VisitorRealmProxy extends twj.lastlocal.in.twj.models.Visitor
    implements RealmObjectProxy, twj_lastlocal_in_twj_models_VisitorRealmProxyInterface {

    static final class VisitorColumnInfo extends ColumnInfo {
        long _idIndex;
        long userRegistrationIdIndex;
        long fullNameIndex;
        long emailIdIndex;
        long phoneNumberIndex;
        long addressIndex;
        long registrationFromIndex;
        long isExistingUserIndex;
        long whereDidYouHereIndex;
        long registrationDateIndex;
        long attentedTodayIndex;
        long attendanceEditedIndex;
        long userDetailEditedIndex;
        long checkExibitionIndex;
        long checkFashionIndex;
        long exbitionIdIndex;
        long dateIndex;
        long attendIndex;
        long venueIdIndex;
        long approvedByIndex;
        long isExibitionIndex;
        long isFashionIndex;
        long exibitionTimeIndex;
        long fashionTimeIndex;
        long tempIsExibitionIndex;
        long tempIsFashionIndex;

        VisitorColumnInfo(OsSchemaInfo schemaInfo) {
            super(26);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("Visitor");
            this._idIndex = addColumnDetails("_id", "_id", objectSchemaInfo);
            this.userRegistrationIdIndex = addColumnDetails("userRegistrationId", "userRegistrationId", objectSchemaInfo);
            this.fullNameIndex = addColumnDetails("fullName", "fullName", objectSchemaInfo);
            this.emailIdIndex = addColumnDetails("emailId", "emailId", objectSchemaInfo);
            this.phoneNumberIndex = addColumnDetails("phoneNumber", "phoneNumber", objectSchemaInfo);
            this.addressIndex = addColumnDetails("address", "address", objectSchemaInfo);
            this.registrationFromIndex = addColumnDetails("registrationFrom", "registrationFrom", objectSchemaInfo);
            this.isExistingUserIndex = addColumnDetails("isExistingUser", "isExistingUser", objectSchemaInfo);
            this.whereDidYouHereIndex = addColumnDetails("whereDidYouHere", "whereDidYouHere", objectSchemaInfo);
            this.registrationDateIndex = addColumnDetails("registrationDate", "registrationDate", objectSchemaInfo);
            this.attentedTodayIndex = addColumnDetails("attentedToday", "attentedToday", objectSchemaInfo);
            this.attendanceEditedIndex = addColumnDetails("attendanceEdited", "attendanceEdited", objectSchemaInfo);
            this.userDetailEditedIndex = addColumnDetails("userDetailEdited", "userDetailEdited", objectSchemaInfo);
            this.checkExibitionIndex = addColumnDetails("checkExibition", "checkExibition", objectSchemaInfo);
            this.checkFashionIndex = addColumnDetails("checkFashion", "checkFashion", objectSchemaInfo);
            this.exbitionIdIndex = addColumnDetails("exbitionId", "exbitionId", objectSchemaInfo);
            this.dateIndex = addColumnDetails("date", "date", objectSchemaInfo);
            this.attendIndex = addColumnDetails("attend", "attend", objectSchemaInfo);
            this.venueIdIndex = addColumnDetails("venueId", "venueId", objectSchemaInfo);
            this.approvedByIndex = addColumnDetails("approvedBy", "approvedBy", objectSchemaInfo);
            this.isExibitionIndex = addColumnDetails("isExibition", "isExibition", objectSchemaInfo);
            this.isFashionIndex = addColumnDetails("isFashion", "isFashion", objectSchemaInfo);
            this.exibitionTimeIndex = addColumnDetails("exibitionTime", "exibitionTime", objectSchemaInfo);
            this.fashionTimeIndex = addColumnDetails("fashionTime", "fashionTime", objectSchemaInfo);
            this.tempIsExibitionIndex = addColumnDetails("tempIsExibition", "tempIsExibition", objectSchemaInfo);
            this.tempIsFashionIndex = addColumnDetails("tempIsFashion", "tempIsFashion", objectSchemaInfo);
        }

        VisitorColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new VisitorColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final VisitorColumnInfo src = (VisitorColumnInfo) rawSrc;
            final VisitorColumnInfo dst = (VisitorColumnInfo) rawDst;
            dst._idIndex = src._idIndex;
            dst.userRegistrationIdIndex = src.userRegistrationIdIndex;
            dst.fullNameIndex = src.fullNameIndex;
            dst.emailIdIndex = src.emailIdIndex;
            dst.phoneNumberIndex = src.phoneNumberIndex;
            dst.addressIndex = src.addressIndex;
            dst.registrationFromIndex = src.registrationFromIndex;
            dst.isExistingUserIndex = src.isExistingUserIndex;
            dst.whereDidYouHereIndex = src.whereDidYouHereIndex;
            dst.registrationDateIndex = src.registrationDateIndex;
            dst.attentedTodayIndex = src.attentedTodayIndex;
            dst.attendanceEditedIndex = src.attendanceEditedIndex;
            dst.userDetailEditedIndex = src.userDetailEditedIndex;
            dst.checkExibitionIndex = src.checkExibitionIndex;
            dst.checkFashionIndex = src.checkFashionIndex;
            dst.exbitionIdIndex = src.exbitionIdIndex;
            dst.dateIndex = src.dateIndex;
            dst.attendIndex = src.attendIndex;
            dst.venueIdIndex = src.venueIdIndex;
            dst.approvedByIndex = src.approvedByIndex;
            dst.isExibitionIndex = src.isExibitionIndex;
            dst.isFashionIndex = src.isFashionIndex;
            dst.exibitionTimeIndex = src.exibitionTimeIndex;
            dst.fashionTimeIndex = src.fashionTimeIndex;
            dst.tempIsExibitionIndex = src.tempIsExibitionIndex;
            dst.tempIsFashionIndex = src.tempIsFashionIndex;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();

    private VisitorColumnInfo columnInfo;
    private ProxyState<twj.lastlocal.in.twj.models.Visitor> proxyState;

    twj_lastlocal_in_twj_models_VisitorRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (VisitorColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<twj.lastlocal.in.twj.models.Visitor>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public long realmGet$_id() {
        proxyState.getRealm$realm().checkIfValid();
        return (long) proxyState.getRow$realm().getLong(columnInfo._idIndex);
    }

    @Override
    public void realmSet$_id(long value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field '_id' cannot be changed after object was created.");
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$userRegistrationId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.userRegistrationIdIndex);
    }

    @Override
    public void realmSet$userRegistrationId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.userRegistrationIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.userRegistrationIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.userRegistrationIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.userRegistrationIdIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$fullName() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.fullNameIndex);
    }

    @Override
    public void realmSet$fullName(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.fullNameIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.fullNameIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.fullNameIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.fullNameIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$emailId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.emailIdIndex);
    }

    @Override
    public void realmSet$emailId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.emailIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.emailIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.emailIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.emailIdIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$phoneNumber() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.phoneNumberIndex);
    }

    @Override
    public void realmSet$phoneNumber(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.phoneNumberIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.phoneNumberIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.phoneNumberIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.phoneNumberIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$address() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.addressIndex);
    }

    @Override
    public void realmSet$address(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.addressIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.addressIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.addressIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.addressIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$registrationFrom() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.registrationFromIndex);
    }

    @Override
    public void realmSet$registrationFrom(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.registrationFromIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.registrationFromIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$isExistingUser() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.isExistingUserIndex);
    }

    @Override
    public void realmSet$isExistingUser(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.isExistingUserIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.isExistingUserIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$whereDidYouHere() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.whereDidYouHereIndex);
    }

    @Override
    public void realmSet$whereDidYouHere(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.whereDidYouHereIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.whereDidYouHereIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.whereDidYouHereIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.whereDidYouHereIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$registrationDate() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.registrationDateIndex);
    }

    @Override
    public void realmSet$registrationDate(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.registrationDateIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.registrationDateIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.registrationDateIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.registrationDateIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$attentedToday() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.attentedTodayIndex);
    }

    @Override
    public void realmSet$attentedToday(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.attentedTodayIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.attentedTodayIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$attendanceEdited() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.attendanceEditedIndex);
    }

    @Override
    public void realmSet$attendanceEdited(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.attendanceEditedIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.attendanceEditedIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$userDetailEdited() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.userDetailEditedIndex);
    }

    @Override
    public void realmSet$userDetailEdited(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.userDetailEditedIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.userDetailEditedIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$checkExibition() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.checkExibitionIndex);
    }

    @Override
    public void realmSet$checkExibition(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.checkExibitionIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.checkExibitionIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$checkFashion() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.checkFashionIndex);
    }

    @Override
    public void realmSet$checkFashion(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.checkFashionIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.checkFashionIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$exbitionId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.exbitionIdIndex);
    }

    @Override
    public void realmSet$exbitionId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.exbitionIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.exbitionIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.exbitionIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.exbitionIdIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$date() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.dateIndex);
    }

    @Override
    public void realmSet$date(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.dateIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.dateIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.dateIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.dateIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$attend() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.attendIndex);
    }

    @Override
    public void realmSet$attend(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.attendIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.attendIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$venueId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.venueIdIndex);
    }

    @Override
    public void realmSet$venueId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.venueIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.venueIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.venueIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.venueIdIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$approvedBy() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.approvedByIndex);
    }

    @Override
    public void realmSet$approvedBy(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.approvedByIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.approvedByIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.approvedByIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.approvedByIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$isExibition() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.isExibitionIndex);
    }

    @Override
    public void realmSet$isExibition(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.isExibitionIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.isExibitionIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$isFashion() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.isFashionIndex);
    }

    @Override
    public void realmSet$isFashion(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.isFashionIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.isFashionIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$exibitionTime() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.exibitionTimeIndex);
    }

    @Override
    public void realmSet$exibitionTime(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.exibitionTimeIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.exibitionTimeIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.exibitionTimeIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.exibitionTimeIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$fashionTime() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.fashionTimeIndex);
    }

    @Override
    public void realmSet$fashionTime(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.fashionTimeIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.fashionTimeIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.fashionTimeIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.fashionTimeIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$tempIsExibition() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.tempIsExibitionIndex);
    }

    @Override
    public void realmSet$tempIsExibition(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.tempIsExibitionIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.tempIsExibitionIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$tempIsFashion() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.tempIsFashionIndex);
    }

    @Override
    public void realmSet$tempIsFashion(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.tempIsFashionIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.tempIsFashionIndex, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("Visitor", 26, 0);
        builder.addPersistedProperty("_id", RealmFieldType.INTEGER, Property.PRIMARY_KEY, Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("userRegistrationId", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("fullName", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("emailId", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("phoneNumber", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("address", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("registrationFrom", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("isExistingUser", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("whereDidYouHere", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("registrationDate", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("attentedToday", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("attendanceEdited", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("userDetailEdited", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("checkExibition", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("checkFashion", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("exbitionId", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("date", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("attend", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("venueId", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("approvedBy", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("isExibition", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("isFashion", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("exibitionTime", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("fashionTime", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("tempIsExibition", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("tempIsFashion", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static VisitorColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new VisitorColumnInfo(schemaInfo);
    }

    public static String getSimpleClassName() {
        return "Visitor";
    }

    public static final class ClassNameHelper {
        public static final String INTERNAL_CLASS_NAME = "Visitor";
    }

    @SuppressWarnings("cast")
    public static twj.lastlocal.in.twj.models.Visitor createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        twj.lastlocal.in.twj.models.Visitor obj = null;
        if (update) {
            Table table = realm.getTable(twj.lastlocal.in.twj.models.Visitor.class);
            VisitorColumnInfo columnInfo = (VisitorColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.Visitor.class);
            long pkColumnIndex = columnInfo._idIndex;
            long rowIndex = Table.NO_MATCH;
            if (!json.isNull("_id")) {
                rowIndex = table.findFirstLong(pkColumnIndex, json.getLong("_id"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.Visitor.class), false, Collections.<String> emptyList());
                    obj = new io.realm.twj_lastlocal_in_twj_models_VisitorRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("_id")) {
                if (json.isNull("_id")) {
                    obj = (io.realm.twj_lastlocal_in_twj_models_VisitorRealmProxy) realm.createObjectInternal(twj.lastlocal.in.twj.models.Visitor.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.twj_lastlocal_in_twj_models_VisitorRealmProxy) realm.createObjectInternal(twj.lastlocal.in.twj.models.Visitor.class, json.getLong("_id"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field '_id'.");
            }
        }

        final twj_lastlocal_in_twj_models_VisitorRealmProxyInterface objProxy = (twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) obj;
        if (json.has("userRegistrationId")) {
            if (json.isNull("userRegistrationId")) {
                objProxy.realmSet$userRegistrationId(null);
            } else {
                objProxy.realmSet$userRegistrationId((String) json.getString("userRegistrationId"));
            }
        }
        if (json.has("fullName")) {
            if (json.isNull("fullName")) {
                objProxy.realmSet$fullName(null);
            } else {
                objProxy.realmSet$fullName((String) json.getString("fullName"));
            }
        }
        if (json.has("emailId")) {
            if (json.isNull("emailId")) {
                objProxy.realmSet$emailId(null);
            } else {
                objProxy.realmSet$emailId((String) json.getString("emailId"));
            }
        }
        if (json.has("phoneNumber")) {
            if (json.isNull("phoneNumber")) {
                objProxy.realmSet$phoneNumber(null);
            } else {
                objProxy.realmSet$phoneNumber((String) json.getString("phoneNumber"));
            }
        }
        if (json.has("address")) {
            if (json.isNull("address")) {
                objProxy.realmSet$address(null);
            } else {
                objProxy.realmSet$address((String) json.getString("address"));
            }
        }
        if (json.has("registrationFrom")) {
            if (json.isNull("registrationFrom")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'registrationFrom' to null.");
            } else {
                objProxy.realmSet$registrationFrom((int) json.getInt("registrationFrom"));
            }
        }
        if (json.has("isExistingUser")) {
            if (json.isNull("isExistingUser")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'isExistingUser' to null.");
            } else {
                objProxy.realmSet$isExistingUser((int) json.getInt("isExistingUser"));
            }
        }
        if (json.has("whereDidYouHere")) {
            if (json.isNull("whereDidYouHere")) {
                objProxy.realmSet$whereDidYouHere(null);
            } else {
                objProxy.realmSet$whereDidYouHere((String) json.getString("whereDidYouHere"));
            }
        }
        if (json.has("registrationDate")) {
            if (json.isNull("registrationDate")) {
                objProxy.realmSet$registrationDate(null);
            } else {
                objProxy.realmSet$registrationDate((String) json.getString("registrationDate"));
            }
        }
        if (json.has("attentedToday")) {
            if (json.isNull("attentedToday")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'attentedToday' to null.");
            } else {
                objProxy.realmSet$attentedToday((int) json.getInt("attentedToday"));
            }
        }
        if (json.has("attendanceEdited")) {
            if (json.isNull("attendanceEdited")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'attendanceEdited' to null.");
            } else {
                objProxy.realmSet$attendanceEdited((int) json.getInt("attendanceEdited"));
            }
        }
        if (json.has("userDetailEdited")) {
            if (json.isNull("userDetailEdited")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'userDetailEdited' to null.");
            } else {
                objProxy.realmSet$userDetailEdited((int) json.getInt("userDetailEdited"));
            }
        }
        if (json.has("checkExibition")) {
            if (json.isNull("checkExibition")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'checkExibition' to null.");
            } else {
                objProxy.realmSet$checkExibition((int) json.getInt("checkExibition"));
            }
        }
        if (json.has("checkFashion")) {
            if (json.isNull("checkFashion")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'checkFashion' to null.");
            } else {
                objProxy.realmSet$checkFashion((int) json.getInt("checkFashion"));
            }
        }
        if (json.has("exbitionId")) {
            if (json.isNull("exbitionId")) {
                objProxy.realmSet$exbitionId(null);
            } else {
                objProxy.realmSet$exbitionId((String) json.getString("exbitionId"));
            }
        }
        if (json.has("date")) {
            if (json.isNull("date")) {
                objProxy.realmSet$date(null);
            } else {
                objProxy.realmSet$date((String) json.getString("date"));
            }
        }
        if (json.has("attend")) {
            if (json.isNull("attend")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'attend' to null.");
            } else {
                objProxy.realmSet$attend((int) json.getInt("attend"));
            }
        }
        if (json.has("venueId")) {
            if (json.isNull("venueId")) {
                objProxy.realmSet$venueId(null);
            } else {
                objProxy.realmSet$venueId((String) json.getString("venueId"));
            }
        }
        if (json.has("approvedBy")) {
            if (json.isNull("approvedBy")) {
                objProxy.realmSet$approvedBy(null);
            } else {
                objProxy.realmSet$approvedBy((String) json.getString("approvedBy"));
            }
        }
        if (json.has("isExibition")) {
            if (json.isNull("isExibition")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'isExibition' to null.");
            } else {
                objProxy.realmSet$isExibition((int) json.getInt("isExibition"));
            }
        }
        if (json.has("isFashion")) {
            if (json.isNull("isFashion")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'isFashion' to null.");
            } else {
                objProxy.realmSet$isFashion((int) json.getInt("isFashion"));
            }
        }
        if (json.has("exibitionTime")) {
            if (json.isNull("exibitionTime")) {
                objProxy.realmSet$exibitionTime(null);
            } else {
                objProxy.realmSet$exibitionTime((String) json.getString("exibitionTime"));
            }
        }
        if (json.has("fashionTime")) {
            if (json.isNull("fashionTime")) {
                objProxy.realmSet$fashionTime(null);
            } else {
                objProxy.realmSet$fashionTime((String) json.getString("fashionTime"));
            }
        }
        if (json.has("tempIsExibition")) {
            if (json.isNull("tempIsExibition")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'tempIsExibition' to null.");
            } else {
                objProxy.realmSet$tempIsExibition((int) json.getInt("tempIsExibition"));
            }
        }
        if (json.has("tempIsFashion")) {
            if (json.isNull("tempIsFashion")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'tempIsFashion' to null.");
            } else {
                objProxy.realmSet$tempIsFashion((int) json.getInt("tempIsFashion"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static twj.lastlocal.in.twj.models.Visitor createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        final twj.lastlocal.in.twj.models.Visitor obj = new twj.lastlocal.in.twj.models.Visitor();
        final twj_lastlocal_in_twj_models_VisitorRealmProxyInterface objProxy = (twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("_id")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$_id((long) reader.nextLong());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field '_id' to null.");
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("userRegistrationId")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$userRegistrationId((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$userRegistrationId(null);
                }
            } else if (name.equals("fullName")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$fullName((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$fullName(null);
                }
            } else if (name.equals("emailId")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$emailId((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$emailId(null);
                }
            } else if (name.equals("phoneNumber")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$phoneNumber((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$phoneNumber(null);
                }
            } else if (name.equals("address")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$address((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$address(null);
                }
            } else if (name.equals("registrationFrom")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$registrationFrom((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'registrationFrom' to null.");
                }
            } else if (name.equals("isExistingUser")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$isExistingUser((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'isExistingUser' to null.");
                }
            } else if (name.equals("whereDidYouHere")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$whereDidYouHere((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$whereDidYouHere(null);
                }
            } else if (name.equals("registrationDate")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$registrationDate((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$registrationDate(null);
                }
            } else if (name.equals("attentedToday")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$attentedToday((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'attentedToday' to null.");
                }
            } else if (name.equals("attendanceEdited")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$attendanceEdited((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'attendanceEdited' to null.");
                }
            } else if (name.equals("userDetailEdited")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$userDetailEdited((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'userDetailEdited' to null.");
                }
            } else if (name.equals("checkExibition")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$checkExibition((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'checkExibition' to null.");
                }
            } else if (name.equals("checkFashion")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$checkFashion((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'checkFashion' to null.");
                }
            } else if (name.equals("exbitionId")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$exbitionId((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$exbitionId(null);
                }
            } else if (name.equals("date")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$date((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$date(null);
                }
            } else if (name.equals("attend")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$attend((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'attend' to null.");
                }
            } else if (name.equals("venueId")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$venueId((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$venueId(null);
                }
            } else if (name.equals("approvedBy")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$approvedBy((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$approvedBy(null);
                }
            } else if (name.equals("isExibition")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$isExibition((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'isExibition' to null.");
                }
            } else if (name.equals("isFashion")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$isFashion((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'isFashion' to null.");
                }
            } else if (name.equals("exibitionTime")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$exibitionTime((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$exibitionTime(null);
                }
            } else if (name.equals("fashionTime")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$fashionTime((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$fashionTime(null);
                }
            } else if (name.equals("tempIsExibition")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$tempIsExibition((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'tempIsExibition' to null.");
                }
            } else if (name.equals("tempIsFashion")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$tempIsFashion((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'tempIsFashion' to null.");
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field '_id'.");
        }
        return realm.copyToRealm(obj);
    }

    public static twj.lastlocal.in.twj.models.Visitor copyOrUpdate(Realm realm, twj.lastlocal.in.twj.models.Visitor object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (twj.lastlocal.in.twj.models.Visitor) cachedRealmObject;
        }

        twj.lastlocal.in.twj.models.Visitor realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(twj.lastlocal.in.twj.models.Visitor.class);
            VisitorColumnInfo columnInfo = (VisitorColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.Visitor.class);
            long pkColumnIndex = columnInfo._idIndex;
            long rowIndex = table.findFirstLong(pkColumnIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$_id());
            if (rowIndex == Table.NO_MATCH) {
                canUpdate = false;
            } else {
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.Visitor.class), false, Collections.<String> emptyList());
                    realmObject = new io.realm.twj_lastlocal_in_twj_models_VisitorRealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            }
        }

        return (canUpdate) ? update(realm, realmObject, object, cache) : copy(realm, object, update, cache);
    }

    public static twj.lastlocal.in.twj.models.Visitor copy(Realm realm, twj.lastlocal.in.twj.models.Visitor newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (twj.lastlocal.in.twj.models.Visitor) cachedRealmObject;
        }

        // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
        twj.lastlocal.in.twj.models.Visitor realmObject = realm.createObjectInternal(twj.lastlocal.in.twj.models.Visitor.class, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) newObject).realmGet$_id(), false, Collections.<String>emptyList());
        cache.put(newObject, (RealmObjectProxy) realmObject);

        twj_lastlocal_in_twj_models_VisitorRealmProxyInterface realmObjectSource = (twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) newObject;
        twj_lastlocal_in_twj_models_VisitorRealmProxyInterface realmObjectCopy = (twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) realmObject;

        realmObjectCopy.realmSet$userRegistrationId(realmObjectSource.realmGet$userRegistrationId());
        realmObjectCopy.realmSet$fullName(realmObjectSource.realmGet$fullName());
        realmObjectCopy.realmSet$emailId(realmObjectSource.realmGet$emailId());
        realmObjectCopy.realmSet$phoneNumber(realmObjectSource.realmGet$phoneNumber());
        realmObjectCopy.realmSet$address(realmObjectSource.realmGet$address());
        realmObjectCopy.realmSet$registrationFrom(realmObjectSource.realmGet$registrationFrom());
        realmObjectCopy.realmSet$isExistingUser(realmObjectSource.realmGet$isExistingUser());
        realmObjectCopy.realmSet$whereDidYouHere(realmObjectSource.realmGet$whereDidYouHere());
        realmObjectCopy.realmSet$registrationDate(realmObjectSource.realmGet$registrationDate());
        realmObjectCopy.realmSet$attentedToday(realmObjectSource.realmGet$attentedToday());
        realmObjectCopy.realmSet$attendanceEdited(realmObjectSource.realmGet$attendanceEdited());
        realmObjectCopy.realmSet$userDetailEdited(realmObjectSource.realmGet$userDetailEdited());
        realmObjectCopy.realmSet$checkExibition(realmObjectSource.realmGet$checkExibition());
        realmObjectCopy.realmSet$checkFashion(realmObjectSource.realmGet$checkFashion());
        realmObjectCopy.realmSet$exbitionId(realmObjectSource.realmGet$exbitionId());
        realmObjectCopy.realmSet$date(realmObjectSource.realmGet$date());
        realmObjectCopy.realmSet$attend(realmObjectSource.realmGet$attend());
        realmObjectCopy.realmSet$venueId(realmObjectSource.realmGet$venueId());
        realmObjectCopy.realmSet$approvedBy(realmObjectSource.realmGet$approvedBy());
        realmObjectCopy.realmSet$isExibition(realmObjectSource.realmGet$isExibition());
        realmObjectCopy.realmSet$isFashion(realmObjectSource.realmGet$isFashion());
        realmObjectCopy.realmSet$exibitionTime(realmObjectSource.realmGet$exibitionTime());
        realmObjectCopy.realmSet$fashionTime(realmObjectSource.realmGet$fashionTime());
        realmObjectCopy.realmSet$tempIsExibition(realmObjectSource.realmGet$tempIsExibition());
        realmObjectCopy.realmSet$tempIsFashion(realmObjectSource.realmGet$tempIsFashion());
        return realmObject;
    }

    public static long insert(Realm realm, twj.lastlocal.in.twj.models.Visitor object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(twj.lastlocal.in.twj.models.Visitor.class);
        long tableNativePtr = table.getNativePtr();
        VisitorColumnInfo columnInfo = (VisitorColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.Visitor.class);
        long pkColumnIndex = columnInfo._idIndex;
        long rowIndex = Table.NO_MATCH;
        Object primaryKeyValue = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$_id();
        if (primaryKeyValue != null) {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$_id());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$_id());
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$userRegistrationId = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$userRegistrationId();
        if (realmGet$userRegistrationId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.userRegistrationIdIndex, rowIndex, realmGet$userRegistrationId, false);
        }
        String realmGet$fullName = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$fullName();
        if (realmGet$fullName != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.fullNameIndex, rowIndex, realmGet$fullName, false);
        }
        String realmGet$emailId = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$emailId();
        if (realmGet$emailId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.emailIdIndex, rowIndex, realmGet$emailId, false);
        }
        String realmGet$phoneNumber = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$phoneNumber();
        if (realmGet$phoneNumber != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.phoneNumberIndex, rowIndex, realmGet$phoneNumber, false);
        }
        String realmGet$address = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$address();
        if (realmGet$address != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.addressIndex, rowIndex, realmGet$address, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.registrationFromIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$registrationFrom(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.isExistingUserIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$isExistingUser(), false);
        String realmGet$whereDidYouHere = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$whereDidYouHere();
        if (realmGet$whereDidYouHere != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.whereDidYouHereIndex, rowIndex, realmGet$whereDidYouHere, false);
        }
        String realmGet$registrationDate = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$registrationDate();
        if (realmGet$registrationDate != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.registrationDateIndex, rowIndex, realmGet$registrationDate, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.attentedTodayIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$attentedToday(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.attendanceEditedIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$attendanceEdited(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.userDetailEditedIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$userDetailEdited(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.checkExibitionIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$checkExibition(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.checkFashionIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$checkFashion(), false);
        String realmGet$exbitionId = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$exbitionId();
        if (realmGet$exbitionId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.exbitionIdIndex, rowIndex, realmGet$exbitionId, false);
        }
        String realmGet$date = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$date();
        if (realmGet$date != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.dateIndex, rowIndex, realmGet$date, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.attendIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$attend(), false);
        String realmGet$venueId = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$venueId();
        if (realmGet$venueId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.venueIdIndex, rowIndex, realmGet$venueId, false);
        }
        String realmGet$approvedBy = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$approvedBy();
        if (realmGet$approvedBy != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.approvedByIndex, rowIndex, realmGet$approvedBy, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.isExibitionIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$isExibition(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.isFashionIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$isFashion(), false);
        String realmGet$exibitionTime = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$exibitionTime();
        if (realmGet$exibitionTime != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.exibitionTimeIndex, rowIndex, realmGet$exibitionTime, false);
        }
        String realmGet$fashionTime = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$fashionTime();
        if (realmGet$fashionTime != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.fashionTimeIndex, rowIndex, realmGet$fashionTime, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.tempIsExibitionIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$tempIsExibition(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.tempIsFashionIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$tempIsFashion(), false);
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(twj.lastlocal.in.twj.models.Visitor.class);
        long tableNativePtr = table.getNativePtr();
        VisitorColumnInfo columnInfo = (VisitorColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.Visitor.class);
        long pkColumnIndex = columnInfo._idIndex;
        twj.lastlocal.in.twj.models.Visitor object = null;
        while (objects.hasNext()) {
            object = (twj.lastlocal.in.twj.models.Visitor) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = Table.NO_MATCH;
            Object primaryKeyValue = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$_id();
            if (primaryKeyValue != null) {
                rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$_id());
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$_id());
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$userRegistrationId = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$userRegistrationId();
            if (realmGet$userRegistrationId != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.userRegistrationIdIndex, rowIndex, realmGet$userRegistrationId, false);
            }
            String realmGet$fullName = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$fullName();
            if (realmGet$fullName != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.fullNameIndex, rowIndex, realmGet$fullName, false);
            }
            String realmGet$emailId = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$emailId();
            if (realmGet$emailId != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.emailIdIndex, rowIndex, realmGet$emailId, false);
            }
            String realmGet$phoneNumber = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$phoneNumber();
            if (realmGet$phoneNumber != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.phoneNumberIndex, rowIndex, realmGet$phoneNumber, false);
            }
            String realmGet$address = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$address();
            if (realmGet$address != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.addressIndex, rowIndex, realmGet$address, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.registrationFromIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$registrationFrom(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.isExistingUserIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$isExistingUser(), false);
            String realmGet$whereDidYouHere = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$whereDidYouHere();
            if (realmGet$whereDidYouHere != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.whereDidYouHereIndex, rowIndex, realmGet$whereDidYouHere, false);
            }
            String realmGet$registrationDate = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$registrationDate();
            if (realmGet$registrationDate != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.registrationDateIndex, rowIndex, realmGet$registrationDate, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.attentedTodayIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$attentedToday(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.attendanceEditedIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$attendanceEdited(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.userDetailEditedIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$userDetailEdited(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.checkExibitionIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$checkExibition(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.checkFashionIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$checkFashion(), false);
            String realmGet$exbitionId = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$exbitionId();
            if (realmGet$exbitionId != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.exbitionIdIndex, rowIndex, realmGet$exbitionId, false);
            }
            String realmGet$date = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$date();
            if (realmGet$date != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.dateIndex, rowIndex, realmGet$date, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.attendIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$attend(), false);
            String realmGet$venueId = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$venueId();
            if (realmGet$venueId != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.venueIdIndex, rowIndex, realmGet$venueId, false);
            }
            String realmGet$approvedBy = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$approvedBy();
            if (realmGet$approvedBy != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.approvedByIndex, rowIndex, realmGet$approvedBy, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.isExibitionIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$isExibition(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.isFashionIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$isFashion(), false);
            String realmGet$exibitionTime = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$exibitionTime();
            if (realmGet$exibitionTime != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.exibitionTimeIndex, rowIndex, realmGet$exibitionTime, false);
            }
            String realmGet$fashionTime = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$fashionTime();
            if (realmGet$fashionTime != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.fashionTimeIndex, rowIndex, realmGet$fashionTime, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.tempIsExibitionIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$tempIsExibition(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.tempIsFashionIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$tempIsFashion(), false);
        }
    }

    public static long insertOrUpdate(Realm realm, twj.lastlocal.in.twj.models.Visitor object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(twj.lastlocal.in.twj.models.Visitor.class);
        long tableNativePtr = table.getNativePtr();
        VisitorColumnInfo columnInfo = (VisitorColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.Visitor.class);
        long pkColumnIndex = columnInfo._idIndex;
        long rowIndex = Table.NO_MATCH;
        Object primaryKeyValue = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$_id();
        if (primaryKeyValue != null) {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$_id());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$_id());
        }
        cache.put(object, rowIndex);
        String realmGet$userRegistrationId = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$userRegistrationId();
        if (realmGet$userRegistrationId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.userRegistrationIdIndex, rowIndex, realmGet$userRegistrationId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.userRegistrationIdIndex, rowIndex, false);
        }
        String realmGet$fullName = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$fullName();
        if (realmGet$fullName != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.fullNameIndex, rowIndex, realmGet$fullName, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.fullNameIndex, rowIndex, false);
        }
        String realmGet$emailId = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$emailId();
        if (realmGet$emailId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.emailIdIndex, rowIndex, realmGet$emailId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.emailIdIndex, rowIndex, false);
        }
        String realmGet$phoneNumber = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$phoneNumber();
        if (realmGet$phoneNumber != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.phoneNumberIndex, rowIndex, realmGet$phoneNumber, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.phoneNumberIndex, rowIndex, false);
        }
        String realmGet$address = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$address();
        if (realmGet$address != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.addressIndex, rowIndex, realmGet$address, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.addressIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.registrationFromIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$registrationFrom(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.isExistingUserIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$isExistingUser(), false);
        String realmGet$whereDidYouHere = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$whereDidYouHere();
        if (realmGet$whereDidYouHere != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.whereDidYouHereIndex, rowIndex, realmGet$whereDidYouHere, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.whereDidYouHereIndex, rowIndex, false);
        }
        String realmGet$registrationDate = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$registrationDate();
        if (realmGet$registrationDate != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.registrationDateIndex, rowIndex, realmGet$registrationDate, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.registrationDateIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.attentedTodayIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$attentedToday(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.attendanceEditedIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$attendanceEdited(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.userDetailEditedIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$userDetailEdited(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.checkExibitionIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$checkExibition(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.checkFashionIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$checkFashion(), false);
        String realmGet$exbitionId = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$exbitionId();
        if (realmGet$exbitionId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.exbitionIdIndex, rowIndex, realmGet$exbitionId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.exbitionIdIndex, rowIndex, false);
        }
        String realmGet$date = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$date();
        if (realmGet$date != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.dateIndex, rowIndex, realmGet$date, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.dateIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.attendIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$attend(), false);
        String realmGet$venueId = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$venueId();
        if (realmGet$venueId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.venueIdIndex, rowIndex, realmGet$venueId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.venueIdIndex, rowIndex, false);
        }
        String realmGet$approvedBy = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$approvedBy();
        if (realmGet$approvedBy != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.approvedByIndex, rowIndex, realmGet$approvedBy, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.approvedByIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.isExibitionIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$isExibition(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.isFashionIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$isFashion(), false);
        String realmGet$exibitionTime = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$exibitionTime();
        if (realmGet$exibitionTime != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.exibitionTimeIndex, rowIndex, realmGet$exibitionTime, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.exibitionTimeIndex, rowIndex, false);
        }
        String realmGet$fashionTime = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$fashionTime();
        if (realmGet$fashionTime != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.fashionTimeIndex, rowIndex, realmGet$fashionTime, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.fashionTimeIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.tempIsExibitionIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$tempIsExibition(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.tempIsFashionIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$tempIsFashion(), false);
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(twj.lastlocal.in.twj.models.Visitor.class);
        long tableNativePtr = table.getNativePtr();
        VisitorColumnInfo columnInfo = (VisitorColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.Visitor.class);
        long pkColumnIndex = columnInfo._idIndex;
        twj.lastlocal.in.twj.models.Visitor object = null;
        while (objects.hasNext()) {
            object = (twj.lastlocal.in.twj.models.Visitor) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = Table.NO_MATCH;
            Object primaryKeyValue = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$_id();
            if (primaryKeyValue != null) {
                rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$_id());
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$_id());
            }
            cache.put(object, rowIndex);
            String realmGet$userRegistrationId = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$userRegistrationId();
            if (realmGet$userRegistrationId != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.userRegistrationIdIndex, rowIndex, realmGet$userRegistrationId, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.userRegistrationIdIndex, rowIndex, false);
            }
            String realmGet$fullName = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$fullName();
            if (realmGet$fullName != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.fullNameIndex, rowIndex, realmGet$fullName, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.fullNameIndex, rowIndex, false);
            }
            String realmGet$emailId = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$emailId();
            if (realmGet$emailId != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.emailIdIndex, rowIndex, realmGet$emailId, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.emailIdIndex, rowIndex, false);
            }
            String realmGet$phoneNumber = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$phoneNumber();
            if (realmGet$phoneNumber != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.phoneNumberIndex, rowIndex, realmGet$phoneNumber, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.phoneNumberIndex, rowIndex, false);
            }
            String realmGet$address = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$address();
            if (realmGet$address != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.addressIndex, rowIndex, realmGet$address, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.addressIndex, rowIndex, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.registrationFromIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$registrationFrom(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.isExistingUserIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$isExistingUser(), false);
            String realmGet$whereDidYouHere = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$whereDidYouHere();
            if (realmGet$whereDidYouHere != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.whereDidYouHereIndex, rowIndex, realmGet$whereDidYouHere, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.whereDidYouHereIndex, rowIndex, false);
            }
            String realmGet$registrationDate = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$registrationDate();
            if (realmGet$registrationDate != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.registrationDateIndex, rowIndex, realmGet$registrationDate, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.registrationDateIndex, rowIndex, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.attentedTodayIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$attentedToday(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.attendanceEditedIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$attendanceEdited(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.userDetailEditedIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$userDetailEdited(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.checkExibitionIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$checkExibition(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.checkFashionIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$checkFashion(), false);
            String realmGet$exbitionId = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$exbitionId();
            if (realmGet$exbitionId != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.exbitionIdIndex, rowIndex, realmGet$exbitionId, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.exbitionIdIndex, rowIndex, false);
            }
            String realmGet$date = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$date();
            if (realmGet$date != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.dateIndex, rowIndex, realmGet$date, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.dateIndex, rowIndex, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.attendIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$attend(), false);
            String realmGet$venueId = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$venueId();
            if (realmGet$venueId != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.venueIdIndex, rowIndex, realmGet$venueId, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.venueIdIndex, rowIndex, false);
            }
            String realmGet$approvedBy = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$approvedBy();
            if (realmGet$approvedBy != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.approvedByIndex, rowIndex, realmGet$approvedBy, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.approvedByIndex, rowIndex, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.isExibitionIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$isExibition(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.isFashionIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$isFashion(), false);
            String realmGet$exibitionTime = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$exibitionTime();
            if (realmGet$exibitionTime != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.exibitionTimeIndex, rowIndex, realmGet$exibitionTime, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.exibitionTimeIndex, rowIndex, false);
            }
            String realmGet$fashionTime = ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$fashionTime();
            if (realmGet$fashionTime != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.fashionTimeIndex, rowIndex, realmGet$fashionTime, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.fashionTimeIndex, rowIndex, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.tempIsExibitionIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$tempIsExibition(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.tempIsFashionIndex, rowIndex, ((twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) object).realmGet$tempIsFashion(), false);
        }
    }

    public static twj.lastlocal.in.twj.models.Visitor createDetachedCopy(twj.lastlocal.in.twj.models.Visitor realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        twj.lastlocal.in.twj.models.Visitor unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new twj.lastlocal.in.twj.models.Visitor();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (twj.lastlocal.in.twj.models.Visitor) cachedObject.object;
            }
            unmanagedObject = (twj.lastlocal.in.twj.models.Visitor) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        twj_lastlocal_in_twj_models_VisitorRealmProxyInterface unmanagedCopy = (twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) unmanagedObject;
        twj_lastlocal_in_twj_models_VisitorRealmProxyInterface realmSource = (twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$_id(realmSource.realmGet$_id());
        unmanagedCopy.realmSet$userRegistrationId(realmSource.realmGet$userRegistrationId());
        unmanagedCopy.realmSet$fullName(realmSource.realmGet$fullName());
        unmanagedCopy.realmSet$emailId(realmSource.realmGet$emailId());
        unmanagedCopy.realmSet$phoneNumber(realmSource.realmGet$phoneNumber());
        unmanagedCopy.realmSet$address(realmSource.realmGet$address());
        unmanagedCopy.realmSet$registrationFrom(realmSource.realmGet$registrationFrom());
        unmanagedCopy.realmSet$isExistingUser(realmSource.realmGet$isExistingUser());
        unmanagedCopy.realmSet$whereDidYouHere(realmSource.realmGet$whereDidYouHere());
        unmanagedCopy.realmSet$registrationDate(realmSource.realmGet$registrationDate());
        unmanagedCopy.realmSet$attentedToday(realmSource.realmGet$attentedToday());
        unmanagedCopy.realmSet$attendanceEdited(realmSource.realmGet$attendanceEdited());
        unmanagedCopy.realmSet$userDetailEdited(realmSource.realmGet$userDetailEdited());
        unmanagedCopy.realmSet$checkExibition(realmSource.realmGet$checkExibition());
        unmanagedCopy.realmSet$checkFashion(realmSource.realmGet$checkFashion());
        unmanagedCopy.realmSet$exbitionId(realmSource.realmGet$exbitionId());
        unmanagedCopy.realmSet$date(realmSource.realmGet$date());
        unmanagedCopy.realmSet$attend(realmSource.realmGet$attend());
        unmanagedCopy.realmSet$venueId(realmSource.realmGet$venueId());
        unmanagedCopy.realmSet$approvedBy(realmSource.realmGet$approvedBy());
        unmanagedCopy.realmSet$isExibition(realmSource.realmGet$isExibition());
        unmanagedCopy.realmSet$isFashion(realmSource.realmGet$isFashion());
        unmanagedCopy.realmSet$exibitionTime(realmSource.realmGet$exibitionTime());
        unmanagedCopy.realmSet$fashionTime(realmSource.realmGet$fashionTime());
        unmanagedCopy.realmSet$tempIsExibition(realmSource.realmGet$tempIsExibition());
        unmanagedCopy.realmSet$tempIsFashion(realmSource.realmGet$tempIsFashion());

        return unmanagedObject;
    }

    static twj.lastlocal.in.twj.models.Visitor update(Realm realm, twj.lastlocal.in.twj.models.Visitor realmObject, twj.lastlocal.in.twj.models.Visitor newObject, Map<RealmModel, RealmObjectProxy> cache) {
        twj_lastlocal_in_twj_models_VisitorRealmProxyInterface realmObjectTarget = (twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) realmObject;
        twj_lastlocal_in_twj_models_VisitorRealmProxyInterface realmObjectSource = (twj_lastlocal_in_twj_models_VisitorRealmProxyInterface) newObject;
        realmObjectTarget.realmSet$userRegistrationId(realmObjectSource.realmGet$userRegistrationId());
        realmObjectTarget.realmSet$fullName(realmObjectSource.realmGet$fullName());
        realmObjectTarget.realmSet$emailId(realmObjectSource.realmGet$emailId());
        realmObjectTarget.realmSet$phoneNumber(realmObjectSource.realmGet$phoneNumber());
        realmObjectTarget.realmSet$address(realmObjectSource.realmGet$address());
        realmObjectTarget.realmSet$registrationFrom(realmObjectSource.realmGet$registrationFrom());
        realmObjectTarget.realmSet$isExistingUser(realmObjectSource.realmGet$isExistingUser());
        realmObjectTarget.realmSet$whereDidYouHere(realmObjectSource.realmGet$whereDidYouHere());
        realmObjectTarget.realmSet$registrationDate(realmObjectSource.realmGet$registrationDate());
        realmObjectTarget.realmSet$attentedToday(realmObjectSource.realmGet$attentedToday());
        realmObjectTarget.realmSet$attendanceEdited(realmObjectSource.realmGet$attendanceEdited());
        realmObjectTarget.realmSet$userDetailEdited(realmObjectSource.realmGet$userDetailEdited());
        realmObjectTarget.realmSet$checkExibition(realmObjectSource.realmGet$checkExibition());
        realmObjectTarget.realmSet$checkFashion(realmObjectSource.realmGet$checkFashion());
        realmObjectTarget.realmSet$exbitionId(realmObjectSource.realmGet$exbitionId());
        realmObjectTarget.realmSet$date(realmObjectSource.realmGet$date());
        realmObjectTarget.realmSet$attend(realmObjectSource.realmGet$attend());
        realmObjectTarget.realmSet$venueId(realmObjectSource.realmGet$venueId());
        realmObjectTarget.realmSet$approvedBy(realmObjectSource.realmGet$approvedBy());
        realmObjectTarget.realmSet$isExibition(realmObjectSource.realmGet$isExibition());
        realmObjectTarget.realmSet$isFashion(realmObjectSource.realmGet$isFashion());
        realmObjectTarget.realmSet$exibitionTime(realmObjectSource.realmGet$exibitionTime());
        realmObjectTarget.realmSet$fashionTime(realmObjectSource.realmGet$fashionTime());
        realmObjectTarget.realmSet$tempIsExibition(realmObjectSource.realmGet$tempIsExibition());
        realmObjectTarget.realmSet$tempIsFashion(realmObjectSource.realmGet$tempIsFashion());
        return realmObject;
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        twj_lastlocal_in_twj_models_VisitorRealmProxy aVisitor = (twj_lastlocal_in_twj_models_VisitorRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aVisitor.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aVisitor.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aVisitor.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }
}

package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.ProxyUtils;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class twj_lastlocal_in_twj_models_PullDataRealmProxy extends twj.lastlocal.in.twj.models.PullData
    implements RealmObjectProxy, twj_lastlocal_in_twj_models_PullDataRealmProxyInterface {

    static final class PullDataColumnInfo extends ColumnInfo {
        long _idIndex;
        long statusCodeIndex;
        long messageIndex;
        long dataIndex;

        PullDataColumnInfo(OsSchemaInfo schemaInfo) {
            super(4);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("PullData");
            this._idIndex = addColumnDetails("_id", "_id", objectSchemaInfo);
            this.statusCodeIndex = addColumnDetails("statusCode", "statusCode", objectSchemaInfo);
            this.messageIndex = addColumnDetails("message", "message", objectSchemaInfo);
            this.dataIndex = addColumnDetails("data", "data", objectSchemaInfo);
        }

        PullDataColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new PullDataColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final PullDataColumnInfo src = (PullDataColumnInfo) rawSrc;
            final PullDataColumnInfo dst = (PullDataColumnInfo) rawDst;
            dst._idIndex = src._idIndex;
            dst.statusCodeIndex = src.statusCodeIndex;
            dst.messageIndex = src.messageIndex;
            dst.dataIndex = src.dataIndex;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();

    private PullDataColumnInfo columnInfo;
    private ProxyState<twj.lastlocal.in.twj.models.PullData> proxyState;

    twj_lastlocal_in_twj_models_PullDataRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (PullDataColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<twj.lastlocal.in.twj.models.PullData>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public long realmGet$_id() {
        proxyState.getRealm$realm().checkIfValid();
        return (long) proxyState.getRow$realm().getLong(columnInfo._idIndex);
    }

    @Override
    public void realmSet$_id(long value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field '_id' cannot be changed after object was created.");
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$statusCode() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.statusCodeIndex);
    }

    @Override
    public void realmSet$statusCode(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.statusCodeIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.statusCodeIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$message() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.messageIndex);
    }

    @Override
    public void realmSet$message(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.messageIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.messageIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.messageIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.messageIndex, value);
    }

    @Override
    public twj.lastlocal.in.twj.models.DataBean realmGet$data() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNullLink(columnInfo.dataIndex)) {
            return null;
        }
        return proxyState.getRealm$realm().get(twj.lastlocal.in.twj.models.DataBean.class, proxyState.getRow$realm().getLink(columnInfo.dataIndex), false, Collections.<String>emptyList());
    }

    @Override
    public void realmSet$data(twj.lastlocal.in.twj.models.DataBean value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("data")) {
                return;
            }
            if (value != null && !RealmObject.isManaged(value)) {
                value = ((Realm) proxyState.getRealm$realm()).copyToRealm(value);
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                // Table#nullifyLink() does not support default value. Just using Row.
                row.nullifyLink(columnInfo.dataIndex);
                return;
            }
            proxyState.checkValidObject(value);
            row.getTable().setLink(columnInfo.dataIndex, row.getIndex(), ((RealmObjectProxy) value).realmGet$proxyState().getRow$realm().getIndex(), true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().nullifyLink(columnInfo.dataIndex);
            return;
        }
        proxyState.checkValidObject(value);
        proxyState.getRow$realm().setLink(columnInfo.dataIndex, ((RealmObjectProxy) value).realmGet$proxyState().getRow$realm().getIndex());
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("PullData", 4, 0);
        builder.addPersistedProperty("_id", RealmFieldType.INTEGER, Property.PRIMARY_KEY, Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("statusCode", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("message", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedLinkProperty("data", RealmFieldType.OBJECT, "DataBean");
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static PullDataColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new PullDataColumnInfo(schemaInfo);
    }

    public static String getSimpleClassName() {
        return "PullData";
    }

    public static final class ClassNameHelper {
        public static final String INTERNAL_CLASS_NAME = "PullData";
    }

    @SuppressWarnings("cast")
    public static twj.lastlocal.in.twj.models.PullData createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = new ArrayList<String>(1);
        twj.lastlocal.in.twj.models.PullData obj = null;
        if (update) {
            Table table = realm.getTable(twj.lastlocal.in.twj.models.PullData.class);
            PullDataColumnInfo columnInfo = (PullDataColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.PullData.class);
            long pkColumnIndex = columnInfo._idIndex;
            long rowIndex = Table.NO_MATCH;
            if (!json.isNull("_id")) {
                rowIndex = table.findFirstLong(pkColumnIndex, json.getLong("_id"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.PullData.class), false, Collections.<String> emptyList());
                    obj = new io.realm.twj_lastlocal_in_twj_models_PullDataRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("data")) {
                excludeFields.add("data");
            }
            if (json.has("_id")) {
                if (json.isNull("_id")) {
                    obj = (io.realm.twj_lastlocal_in_twj_models_PullDataRealmProxy) realm.createObjectInternal(twj.lastlocal.in.twj.models.PullData.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.twj_lastlocal_in_twj_models_PullDataRealmProxy) realm.createObjectInternal(twj.lastlocal.in.twj.models.PullData.class, json.getLong("_id"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field '_id'.");
            }
        }

        final twj_lastlocal_in_twj_models_PullDataRealmProxyInterface objProxy = (twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) obj;
        if (json.has("statusCode")) {
            if (json.isNull("statusCode")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'statusCode' to null.");
            } else {
                objProxy.realmSet$statusCode((int) json.getInt("statusCode"));
            }
        }
        if (json.has("message")) {
            if (json.isNull("message")) {
                objProxy.realmSet$message(null);
            } else {
                objProxy.realmSet$message((String) json.getString("message"));
            }
        }
        if (json.has("data")) {
            if (json.isNull("data")) {
                objProxy.realmSet$data(null);
            } else {
                twj.lastlocal.in.twj.models.DataBean dataObj = twj_lastlocal_in_twj_models_DataBeanRealmProxy.createOrUpdateUsingJsonObject(realm, json.getJSONObject("data"), update);
                objProxy.realmSet$data(dataObj);
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static twj.lastlocal.in.twj.models.PullData createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        final twj.lastlocal.in.twj.models.PullData obj = new twj.lastlocal.in.twj.models.PullData();
        final twj_lastlocal_in_twj_models_PullDataRealmProxyInterface objProxy = (twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("_id")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$_id((long) reader.nextLong());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field '_id' to null.");
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("statusCode")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$statusCode((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'statusCode' to null.");
                }
            } else if (name.equals("message")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$message((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$message(null);
                }
            } else if (name.equals("data")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    objProxy.realmSet$data(null);
                } else {
                    twj.lastlocal.in.twj.models.DataBean dataObj = twj_lastlocal_in_twj_models_DataBeanRealmProxy.createUsingJsonStream(realm, reader);
                    objProxy.realmSet$data(dataObj);
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field '_id'.");
        }
        return realm.copyToRealm(obj);
    }

    public static twj.lastlocal.in.twj.models.PullData copyOrUpdate(Realm realm, twj.lastlocal.in.twj.models.PullData object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (twj.lastlocal.in.twj.models.PullData) cachedRealmObject;
        }

        twj.lastlocal.in.twj.models.PullData realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(twj.lastlocal.in.twj.models.PullData.class);
            PullDataColumnInfo columnInfo = (PullDataColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.PullData.class);
            long pkColumnIndex = columnInfo._idIndex;
            long rowIndex = table.findFirstLong(pkColumnIndex, ((twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) object).realmGet$_id());
            if (rowIndex == Table.NO_MATCH) {
                canUpdate = false;
            } else {
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.PullData.class), false, Collections.<String> emptyList());
                    realmObject = new io.realm.twj_lastlocal_in_twj_models_PullDataRealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            }
        }

        return (canUpdate) ? update(realm, realmObject, object, cache) : copy(realm, object, update, cache);
    }

    public static twj.lastlocal.in.twj.models.PullData copy(Realm realm, twj.lastlocal.in.twj.models.PullData newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (twj.lastlocal.in.twj.models.PullData) cachedRealmObject;
        }

        // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
        twj.lastlocal.in.twj.models.PullData realmObject = realm.createObjectInternal(twj.lastlocal.in.twj.models.PullData.class, ((twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) newObject).realmGet$_id(), false, Collections.<String>emptyList());
        cache.put(newObject, (RealmObjectProxy) realmObject);

        twj_lastlocal_in_twj_models_PullDataRealmProxyInterface realmObjectSource = (twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) newObject;
        twj_lastlocal_in_twj_models_PullDataRealmProxyInterface realmObjectCopy = (twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) realmObject;

        realmObjectCopy.realmSet$statusCode(realmObjectSource.realmGet$statusCode());
        realmObjectCopy.realmSet$message(realmObjectSource.realmGet$message());

        twj.lastlocal.in.twj.models.DataBean dataObj = realmObjectSource.realmGet$data();
        if (dataObj == null) {
            realmObjectCopy.realmSet$data(null);
        } else {
            twj.lastlocal.in.twj.models.DataBean cachedata = (twj.lastlocal.in.twj.models.DataBean) cache.get(dataObj);
            if (cachedata != null) {
                realmObjectCopy.realmSet$data(cachedata);
            } else {
                realmObjectCopy.realmSet$data(twj_lastlocal_in_twj_models_DataBeanRealmProxy.copyOrUpdate(realm, dataObj, update, cache));
            }
        }
        return realmObject;
    }

    public static long insert(Realm realm, twj.lastlocal.in.twj.models.PullData object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(twj.lastlocal.in.twj.models.PullData.class);
        long tableNativePtr = table.getNativePtr();
        PullDataColumnInfo columnInfo = (PullDataColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.PullData.class);
        long pkColumnIndex = columnInfo._idIndex;
        long rowIndex = Table.NO_MATCH;
        Object primaryKeyValue = ((twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) object).realmGet$_id();
        if (primaryKeyValue != null) {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) object).realmGet$_id());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) object).realmGet$_id());
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        Table.nativeSetLong(tableNativePtr, columnInfo.statusCodeIndex, rowIndex, ((twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) object).realmGet$statusCode(), false);
        String realmGet$message = ((twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) object).realmGet$message();
        if (realmGet$message != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.messageIndex, rowIndex, realmGet$message, false);
        }

        twj.lastlocal.in.twj.models.DataBean dataObj = ((twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) object).realmGet$data();
        if (dataObj != null) {
            Long cachedata = cache.get(dataObj);
            if (cachedata == null) {
                cachedata = twj_lastlocal_in_twj_models_DataBeanRealmProxy.insert(realm, dataObj, cache);
            }
            Table.nativeSetLink(tableNativePtr, columnInfo.dataIndex, rowIndex, cachedata, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(twj.lastlocal.in.twj.models.PullData.class);
        long tableNativePtr = table.getNativePtr();
        PullDataColumnInfo columnInfo = (PullDataColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.PullData.class);
        long pkColumnIndex = columnInfo._idIndex;
        twj.lastlocal.in.twj.models.PullData object = null;
        while (objects.hasNext()) {
            object = (twj.lastlocal.in.twj.models.PullData) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = Table.NO_MATCH;
            Object primaryKeyValue = ((twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) object).realmGet$_id();
            if (primaryKeyValue != null) {
                rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) object).realmGet$_id());
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) object).realmGet$_id());
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, rowIndex);
            Table.nativeSetLong(tableNativePtr, columnInfo.statusCodeIndex, rowIndex, ((twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) object).realmGet$statusCode(), false);
            String realmGet$message = ((twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) object).realmGet$message();
            if (realmGet$message != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.messageIndex, rowIndex, realmGet$message, false);
            }

            twj.lastlocal.in.twj.models.DataBean dataObj = ((twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) object).realmGet$data();
            if (dataObj != null) {
                Long cachedata = cache.get(dataObj);
                if (cachedata == null) {
                    cachedata = twj_lastlocal_in_twj_models_DataBeanRealmProxy.insert(realm, dataObj, cache);
                }
                table.setLink(columnInfo.dataIndex, rowIndex, cachedata, false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, twj.lastlocal.in.twj.models.PullData object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(twj.lastlocal.in.twj.models.PullData.class);
        long tableNativePtr = table.getNativePtr();
        PullDataColumnInfo columnInfo = (PullDataColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.PullData.class);
        long pkColumnIndex = columnInfo._idIndex;
        long rowIndex = Table.NO_MATCH;
        Object primaryKeyValue = ((twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) object).realmGet$_id();
        if (primaryKeyValue != null) {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) object).realmGet$_id());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) object).realmGet$_id());
        }
        cache.put(object, rowIndex);
        Table.nativeSetLong(tableNativePtr, columnInfo.statusCodeIndex, rowIndex, ((twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) object).realmGet$statusCode(), false);
        String realmGet$message = ((twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) object).realmGet$message();
        if (realmGet$message != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.messageIndex, rowIndex, realmGet$message, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.messageIndex, rowIndex, false);
        }

        twj.lastlocal.in.twj.models.DataBean dataObj = ((twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) object).realmGet$data();
        if (dataObj != null) {
            Long cachedata = cache.get(dataObj);
            if (cachedata == null) {
                cachedata = twj_lastlocal_in_twj_models_DataBeanRealmProxy.insertOrUpdate(realm, dataObj, cache);
            }
            Table.nativeSetLink(tableNativePtr, columnInfo.dataIndex, rowIndex, cachedata, false);
        } else {
            Table.nativeNullifyLink(tableNativePtr, columnInfo.dataIndex, rowIndex);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(twj.lastlocal.in.twj.models.PullData.class);
        long tableNativePtr = table.getNativePtr();
        PullDataColumnInfo columnInfo = (PullDataColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.PullData.class);
        long pkColumnIndex = columnInfo._idIndex;
        twj.lastlocal.in.twj.models.PullData object = null;
        while (objects.hasNext()) {
            object = (twj.lastlocal.in.twj.models.PullData) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = Table.NO_MATCH;
            Object primaryKeyValue = ((twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) object).realmGet$_id();
            if (primaryKeyValue != null) {
                rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) object).realmGet$_id());
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) object).realmGet$_id());
            }
            cache.put(object, rowIndex);
            Table.nativeSetLong(tableNativePtr, columnInfo.statusCodeIndex, rowIndex, ((twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) object).realmGet$statusCode(), false);
            String realmGet$message = ((twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) object).realmGet$message();
            if (realmGet$message != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.messageIndex, rowIndex, realmGet$message, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.messageIndex, rowIndex, false);
            }

            twj.lastlocal.in.twj.models.DataBean dataObj = ((twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) object).realmGet$data();
            if (dataObj != null) {
                Long cachedata = cache.get(dataObj);
                if (cachedata == null) {
                    cachedata = twj_lastlocal_in_twj_models_DataBeanRealmProxy.insertOrUpdate(realm, dataObj, cache);
                }
                Table.nativeSetLink(tableNativePtr, columnInfo.dataIndex, rowIndex, cachedata, false);
            } else {
                Table.nativeNullifyLink(tableNativePtr, columnInfo.dataIndex, rowIndex);
            }
        }
    }

    public static twj.lastlocal.in.twj.models.PullData createDetachedCopy(twj.lastlocal.in.twj.models.PullData realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        twj.lastlocal.in.twj.models.PullData unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new twj.lastlocal.in.twj.models.PullData();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (twj.lastlocal.in.twj.models.PullData) cachedObject.object;
            }
            unmanagedObject = (twj.lastlocal.in.twj.models.PullData) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        twj_lastlocal_in_twj_models_PullDataRealmProxyInterface unmanagedCopy = (twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) unmanagedObject;
        twj_lastlocal_in_twj_models_PullDataRealmProxyInterface realmSource = (twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$_id(realmSource.realmGet$_id());
        unmanagedCopy.realmSet$statusCode(realmSource.realmGet$statusCode());
        unmanagedCopy.realmSet$message(realmSource.realmGet$message());

        // Deep copy of data
        unmanagedCopy.realmSet$data(twj_lastlocal_in_twj_models_DataBeanRealmProxy.createDetachedCopy(realmSource.realmGet$data(), currentDepth + 1, maxDepth, cache));

        return unmanagedObject;
    }

    static twj.lastlocal.in.twj.models.PullData update(Realm realm, twj.lastlocal.in.twj.models.PullData realmObject, twj.lastlocal.in.twj.models.PullData newObject, Map<RealmModel, RealmObjectProxy> cache) {
        twj_lastlocal_in_twj_models_PullDataRealmProxyInterface realmObjectTarget = (twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) realmObject;
        twj_lastlocal_in_twj_models_PullDataRealmProxyInterface realmObjectSource = (twj_lastlocal_in_twj_models_PullDataRealmProxyInterface) newObject;
        realmObjectTarget.realmSet$statusCode(realmObjectSource.realmGet$statusCode());
        realmObjectTarget.realmSet$message(realmObjectSource.realmGet$message());
        twj.lastlocal.in.twj.models.DataBean dataObj = realmObjectSource.realmGet$data();
        if (dataObj == null) {
            realmObjectTarget.realmSet$data(null);
        } else {
            twj.lastlocal.in.twj.models.DataBean cachedata = (twj.lastlocal.in.twj.models.DataBean) cache.get(dataObj);
            if (cachedata != null) {
                realmObjectTarget.realmSet$data(cachedata);
            } else {
                realmObjectTarget.realmSet$data(twj_lastlocal_in_twj_models_DataBeanRealmProxy.copyOrUpdate(realm, dataObj, true, cache));
            }
        }
        return realmObject;
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        twj_lastlocal_in_twj_models_PullDataRealmProxy aPullData = (twj_lastlocal_in_twj_models_PullDataRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aPullData.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aPullData.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aPullData.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }
}

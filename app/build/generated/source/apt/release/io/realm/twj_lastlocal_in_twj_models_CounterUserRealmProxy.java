package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.ProxyUtils;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class twj_lastlocal_in_twj_models_CounterUserRealmProxy extends twj.lastlocal.in.twj.models.CounterUser
    implements RealmObjectProxy, twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface {

    static final class CounterUserColumnInfo extends ColumnInfo {
        long _idIndex;
        long adminIdIndex;
        long emailIdIndex;
        long passwordIndex;
        long fullnameIndex;
        long isEnableIndex;
        long apiKeyIndex;

        CounterUserColumnInfo(OsSchemaInfo schemaInfo) {
            super(7);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("CounterUser");
            this._idIndex = addColumnDetails("_id", "_id", objectSchemaInfo);
            this.adminIdIndex = addColumnDetails("adminId", "adminId", objectSchemaInfo);
            this.emailIdIndex = addColumnDetails("emailId", "emailId", objectSchemaInfo);
            this.passwordIndex = addColumnDetails("password", "password", objectSchemaInfo);
            this.fullnameIndex = addColumnDetails("fullname", "fullname", objectSchemaInfo);
            this.isEnableIndex = addColumnDetails("isEnable", "isEnable", objectSchemaInfo);
            this.apiKeyIndex = addColumnDetails("apiKey", "apiKey", objectSchemaInfo);
        }

        CounterUserColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new CounterUserColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final CounterUserColumnInfo src = (CounterUserColumnInfo) rawSrc;
            final CounterUserColumnInfo dst = (CounterUserColumnInfo) rawDst;
            dst._idIndex = src._idIndex;
            dst.adminIdIndex = src.adminIdIndex;
            dst.emailIdIndex = src.emailIdIndex;
            dst.passwordIndex = src.passwordIndex;
            dst.fullnameIndex = src.fullnameIndex;
            dst.isEnableIndex = src.isEnableIndex;
            dst.apiKeyIndex = src.apiKeyIndex;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();

    private CounterUserColumnInfo columnInfo;
    private ProxyState<twj.lastlocal.in.twj.models.CounterUser> proxyState;

    twj_lastlocal_in_twj_models_CounterUserRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (CounterUserColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<twj.lastlocal.in.twj.models.CounterUser>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public long realmGet$_id() {
        proxyState.getRealm$realm().checkIfValid();
        return (long) proxyState.getRow$realm().getLong(columnInfo._idIndex);
    }

    @Override
    public void realmSet$_id(long value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field '_id' cannot be changed after object was created.");
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$adminId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.adminIdIndex);
    }

    @Override
    public void realmSet$adminId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.adminIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.adminIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.adminIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.adminIdIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$emailId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.emailIdIndex);
    }

    @Override
    public void realmSet$emailId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.emailIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.emailIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.emailIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.emailIdIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$password() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.passwordIndex);
    }

    @Override
    public void realmSet$password(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.passwordIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.passwordIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.passwordIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.passwordIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$fullname() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.fullnameIndex);
    }

    @Override
    public void realmSet$fullname(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.fullnameIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.fullnameIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.fullnameIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.fullnameIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$isEnable() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.isEnableIndex);
    }

    @Override
    public void realmSet$isEnable(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.isEnableIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.isEnableIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.isEnableIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.isEnableIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$apiKey() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.apiKeyIndex);
    }

    @Override
    public void realmSet$apiKey(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.apiKeyIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.apiKeyIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.apiKeyIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.apiKeyIndex, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("CounterUser", 7, 0);
        builder.addPersistedProperty("_id", RealmFieldType.INTEGER, Property.PRIMARY_KEY, Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("adminId", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("emailId", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("password", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("fullname", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("isEnable", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("apiKey", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static CounterUserColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new CounterUserColumnInfo(schemaInfo);
    }

    public static String getSimpleClassName() {
        return "CounterUser";
    }

    public static final class ClassNameHelper {
        public static final String INTERNAL_CLASS_NAME = "CounterUser";
    }

    @SuppressWarnings("cast")
    public static twj.lastlocal.in.twj.models.CounterUser createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        twj.lastlocal.in.twj.models.CounterUser obj = null;
        if (update) {
            Table table = realm.getTable(twj.lastlocal.in.twj.models.CounterUser.class);
            CounterUserColumnInfo columnInfo = (CounterUserColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.CounterUser.class);
            long pkColumnIndex = columnInfo._idIndex;
            long rowIndex = Table.NO_MATCH;
            if (!json.isNull("_id")) {
                rowIndex = table.findFirstLong(pkColumnIndex, json.getLong("_id"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.CounterUser.class), false, Collections.<String> emptyList());
                    obj = new io.realm.twj_lastlocal_in_twj_models_CounterUserRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("_id")) {
                if (json.isNull("_id")) {
                    obj = (io.realm.twj_lastlocal_in_twj_models_CounterUserRealmProxy) realm.createObjectInternal(twj.lastlocal.in.twj.models.CounterUser.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.twj_lastlocal_in_twj_models_CounterUserRealmProxy) realm.createObjectInternal(twj.lastlocal.in.twj.models.CounterUser.class, json.getLong("_id"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field '_id'.");
            }
        }

        final twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface objProxy = (twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) obj;
        if (json.has("adminId")) {
            if (json.isNull("adminId")) {
                objProxy.realmSet$adminId(null);
            } else {
                objProxy.realmSet$adminId((String) json.getString("adminId"));
            }
        }
        if (json.has("emailId")) {
            if (json.isNull("emailId")) {
                objProxy.realmSet$emailId(null);
            } else {
                objProxy.realmSet$emailId((String) json.getString("emailId"));
            }
        }
        if (json.has("password")) {
            if (json.isNull("password")) {
                objProxy.realmSet$password(null);
            } else {
                objProxy.realmSet$password((String) json.getString("password"));
            }
        }
        if (json.has("fullname")) {
            if (json.isNull("fullname")) {
                objProxy.realmSet$fullname(null);
            } else {
                objProxy.realmSet$fullname((String) json.getString("fullname"));
            }
        }
        if (json.has("isEnable")) {
            if (json.isNull("isEnable")) {
                objProxy.realmSet$isEnable(null);
            } else {
                objProxy.realmSet$isEnable((String) json.getString("isEnable"));
            }
        }
        if (json.has("apiKey")) {
            if (json.isNull("apiKey")) {
                objProxy.realmSet$apiKey(null);
            } else {
                objProxy.realmSet$apiKey((String) json.getString("apiKey"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static twj.lastlocal.in.twj.models.CounterUser createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        final twj.lastlocal.in.twj.models.CounterUser obj = new twj.lastlocal.in.twj.models.CounterUser();
        final twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface objProxy = (twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("_id")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$_id((long) reader.nextLong());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field '_id' to null.");
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("adminId")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$adminId((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$adminId(null);
                }
            } else if (name.equals("emailId")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$emailId((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$emailId(null);
                }
            } else if (name.equals("password")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$password((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$password(null);
                }
            } else if (name.equals("fullname")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$fullname((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$fullname(null);
                }
            } else if (name.equals("isEnable")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$isEnable((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$isEnable(null);
                }
            } else if (name.equals("apiKey")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$apiKey((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$apiKey(null);
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field '_id'.");
        }
        return realm.copyToRealm(obj);
    }

    public static twj.lastlocal.in.twj.models.CounterUser copyOrUpdate(Realm realm, twj.lastlocal.in.twj.models.CounterUser object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (twj.lastlocal.in.twj.models.CounterUser) cachedRealmObject;
        }

        twj.lastlocal.in.twj.models.CounterUser realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(twj.lastlocal.in.twj.models.CounterUser.class);
            CounterUserColumnInfo columnInfo = (CounterUserColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.CounterUser.class);
            long pkColumnIndex = columnInfo._idIndex;
            long rowIndex = table.findFirstLong(pkColumnIndex, ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$_id());
            if (rowIndex == Table.NO_MATCH) {
                canUpdate = false;
            } else {
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.CounterUser.class), false, Collections.<String> emptyList());
                    realmObject = new io.realm.twj_lastlocal_in_twj_models_CounterUserRealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            }
        }

        return (canUpdate) ? update(realm, realmObject, object, cache) : copy(realm, object, update, cache);
    }

    public static twj.lastlocal.in.twj.models.CounterUser copy(Realm realm, twj.lastlocal.in.twj.models.CounterUser newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (twj.lastlocal.in.twj.models.CounterUser) cachedRealmObject;
        }

        // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
        twj.lastlocal.in.twj.models.CounterUser realmObject = realm.createObjectInternal(twj.lastlocal.in.twj.models.CounterUser.class, ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) newObject).realmGet$_id(), false, Collections.<String>emptyList());
        cache.put(newObject, (RealmObjectProxy) realmObject);

        twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface realmObjectSource = (twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) newObject;
        twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface realmObjectCopy = (twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) realmObject;

        realmObjectCopy.realmSet$adminId(realmObjectSource.realmGet$adminId());
        realmObjectCopy.realmSet$emailId(realmObjectSource.realmGet$emailId());
        realmObjectCopy.realmSet$password(realmObjectSource.realmGet$password());
        realmObjectCopy.realmSet$fullname(realmObjectSource.realmGet$fullname());
        realmObjectCopy.realmSet$isEnable(realmObjectSource.realmGet$isEnable());
        realmObjectCopy.realmSet$apiKey(realmObjectSource.realmGet$apiKey());
        return realmObject;
    }

    public static long insert(Realm realm, twj.lastlocal.in.twj.models.CounterUser object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(twj.lastlocal.in.twj.models.CounterUser.class);
        long tableNativePtr = table.getNativePtr();
        CounterUserColumnInfo columnInfo = (CounterUserColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.CounterUser.class);
        long pkColumnIndex = columnInfo._idIndex;
        long rowIndex = Table.NO_MATCH;
        Object primaryKeyValue = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$_id();
        if (primaryKeyValue != null) {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$_id());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$_id());
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$adminId = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$adminId();
        if (realmGet$adminId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.adminIdIndex, rowIndex, realmGet$adminId, false);
        }
        String realmGet$emailId = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$emailId();
        if (realmGet$emailId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.emailIdIndex, rowIndex, realmGet$emailId, false);
        }
        String realmGet$password = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$password();
        if (realmGet$password != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.passwordIndex, rowIndex, realmGet$password, false);
        }
        String realmGet$fullname = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$fullname();
        if (realmGet$fullname != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.fullnameIndex, rowIndex, realmGet$fullname, false);
        }
        String realmGet$isEnable = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$isEnable();
        if (realmGet$isEnable != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.isEnableIndex, rowIndex, realmGet$isEnable, false);
        }
        String realmGet$apiKey = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$apiKey();
        if (realmGet$apiKey != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.apiKeyIndex, rowIndex, realmGet$apiKey, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(twj.lastlocal.in.twj.models.CounterUser.class);
        long tableNativePtr = table.getNativePtr();
        CounterUserColumnInfo columnInfo = (CounterUserColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.CounterUser.class);
        long pkColumnIndex = columnInfo._idIndex;
        twj.lastlocal.in.twj.models.CounterUser object = null;
        while (objects.hasNext()) {
            object = (twj.lastlocal.in.twj.models.CounterUser) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = Table.NO_MATCH;
            Object primaryKeyValue = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$_id();
            if (primaryKeyValue != null) {
                rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$_id());
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$_id());
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$adminId = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$adminId();
            if (realmGet$adminId != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.adminIdIndex, rowIndex, realmGet$adminId, false);
            }
            String realmGet$emailId = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$emailId();
            if (realmGet$emailId != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.emailIdIndex, rowIndex, realmGet$emailId, false);
            }
            String realmGet$password = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$password();
            if (realmGet$password != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.passwordIndex, rowIndex, realmGet$password, false);
            }
            String realmGet$fullname = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$fullname();
            if (realmGet$fullname != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.fullnameIndex, rowIndex, realmGet$fullname, false);
            }
            String realmGet$isEnable = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$isEnable();
            if (realmGet$isEnable != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.isEnableIndex, rowIndex, realmGet$isEnable, false);
            }
            String realmGet$apiKey = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$apiKey();
            if (realmGet$apiKey != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.apiKeyIndex, rowIndex, realmGet$apiKey, false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, twj.lastlocal.in.twj.models.CounterUser object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(twj.lastlocal.in.twj.models.CounterUser.class);
        long tableNativePtr = table.getNativePtr();
        CounterUserColumnInfo columnInfo = (CounterUserColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.CounterUser.class);
        long pkColumnIndex = columnInfo._idIndex;
        long rowIndex = Table.NO_MATCH;
        Object primaryKeyValue = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$_id();
        if (primaryKeyValue != null) {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$_id());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$_id());
        }
        cache.put(object, rowIndex);
        String realmGet$adminId = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$adminId();
        if (realmGet$adminId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.adminIdIndex, rowIndex, realmGet$adminId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.adminIdIndex, rowIndex, false);
        }
        String realmGet$emailId = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$emailId();
        if (realmGet$emailId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.emailIdIndex, rowIndex, realmGet$emailId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.emailIdIndex, rowIndex, false);
        }
        String realmGet$password = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$password();
        if (realmGet$password != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.passwordIndex, rowIndex, realmGet$password, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.passwordIndex, rowIndex, false);
        }
        String realmGet$fullname = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$fullname();
        if (realmGet$fullname != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.fullnameIndex, rowIndex, realmGet$fullname, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.fullnameIndex, rowIndex, false);
        }
        String realmGet$isEnable = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$isEnable();
        if (realmGet$isEnable != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.isEnableIndex, rowIndex, realmGet$isEnable, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.isEnableIndex, rowIndex, false);
        }
        String realmGet$apiKey = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$apiKey();
        if (realmGet$apiKey != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.apiKeyIndex, rowIndex, realmGet$apiKey, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.apiKeyIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(twj.lastlocal.in.twj.models.CounterUser.class);
        long tableNativePtr = table.getNativePtr();
        CounterUserColumnInfo columnInfo = (CounterUserColumnInfo) realm.getSchema().getColumnInfo(twj.lastlocal.in.twj.models.CounterUser.class);
        long pkColumnIndex = columnInfo._idIndex;
        twj.lastlocal.in.twj.models.CounterUser object = null;
        while (objects.hasNext()) {
            object = (twj.lastlocal.in.twj.models.CounterUser) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = Table.NO_MATCH;
            Object primaryKeyValue = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$_id();
            if (primaryKeyValue != null) {
                rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$_id());
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$_id());
            }
            cache.put(object, rowIndex);
            String realmGet$adminId = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$adminId();
            if (realmGet$adminId != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.adminIdIndex, rowIndex, realmGet$adminId, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.adminIdIndex, rowIndex, false);
            }
            String realmGet$emailId = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$emailId();
            if (realmGet$emailId != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.emailIdIndex, rowIndex, realmGet$emailId, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.emailIdIndex, rowIndex, false);
            }
            String realmGet$password = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$password();
            if (realmGet$password != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.passwordIndex, rowIndex, realmGet$password, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.passwordIndex, rowIndex, false);
            }
            String realmGet$fullname = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$fullname();
            if (realmGet$fullname != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.fullnameIndex, rowIndex, realmGet$fullname, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.fullnameIndex, rowIndex, false);
            }
            String realmGet$isEnable = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$isEnable();
            if (realmGet$isEnable != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.isEnableIndex, rowIndex, realmGet$isEnable, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.isEnableIndex, rowIndex, false);
            }
            String realmGet$apiKey = ((twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) object).realmGet$apiKey();
            if (realmGet$apiKey != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.apiKeyIndex, rowIndex, realmGet$apiKey, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.apiKeyIndex, rowIndex, false);
            }
        }
    }

    public static twj.lastlocal.in.twj.models.CounterUser createDetachedCopy(twj.lastlocal.in.twj.models.CounterUser realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        twj.lastlocal.in.twj.models.CounterUser unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new twj.lastlocal.in.twj.models.CounterUser();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (twj.lastlocal.in.twj.models.CounterUser) cachedObject.object;
            }
            unmanagedObject = (twj.lastlocal.in.twj.models.CounterUser) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface unmanagedCopy = (twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) unmanagedObject;
        twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface realmSource = (twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$_id(realmSource.realmGet$_id());
        unmanagedCopy.realmSet$adminId(realmSource.realmGet$adminId());
        unmanagedCopy.realmSet$emailId(realmSource.realmGet$emailId());
        unmanagedCopy.realmSet$password(realmSource.realmGet$password());
        unmanagedCopy.realmSet$fullname(realmSource.realmGet$fullname());
        unmanagedCopy.realmSet$isEnable(realmSource.realmGet$isEnable());
        unmanagedCopy.realmSet$apiKey(realmSource.realmGet$apiKey());

        return unmanagedObject;
    }

    static twj.lastlocal.in.twj.models.CounterUser update(Realm realm, twj.lastlocal.in.twj.models.CounterUser realmObject, twj.lastlocal.in.twj.models.CounterUser newObject, Map<RealmModel, RealmObjectProxy> cache) {
        twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface realmObjectTarget = (twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) realmObject;
        twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface realmObjectSource = (twj_lastlocal_in_twj_models_CounterUserRealmProxyInterface) newObject;
        realmObjectTarget.realmSet$adminId(realmObjectSource.realmGet$adminId());
        realmObjectTarget.realmSet$emailId(realmObjectSource.realmGet$emailId());
        realmObjectTarget.realmSet$password(realmObjectSource.realmGet$password());
        realmObjectTarget.realmSet$fullname(realmObjectSource.realmGet$fullname());
        realmObjectTarget.realmSet$isEnable(realmObjectSource.realmGet$isEnable());
        realmObjectTarget.realmSet$apiKey(realmObjectSource.realmGet$apiKey());
        return realmObject;
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        twj_lastlocal_in_twj_models_CounterUserRealmProxy aCounterUser = (twj_lastlocal_in_twj_models_CounterUserRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aCounterUser.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aCounterUser.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aCounterUser.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }
}

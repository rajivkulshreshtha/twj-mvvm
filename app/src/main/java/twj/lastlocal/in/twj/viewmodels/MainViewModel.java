package twj.lastlocal.in.twj.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import java.util.Date;
import java.util.List;

import timber.log.Timber;
import twj.lastlocal.in.twj.models.Visitor;
import twj.lastlocal.in.twj.repo.AppRepository;
import twj.lastlocal.in.twj.utils.DateUtils;
import twj.lastlocal.in.twj.utils.GlobalGson;

public class MainViewModel extends AndroidViewModel {

    private AppRepository mRepository;
    public MutableLiveData<List<Visitor>> registeredVisitor = new MutableLiveData<>();
    public MutableLiveData<List<Visitor>> attendedTodayVisitor = new MutableLiveData<>();

    public MutableLiveData<List<Visitor>> newAttendingVisitors = new MutableLiveData<>();
    public MutableLiveData<List<Visitor>> existingAttendingVisitors = new MutableLiveData<>();

    public LiveData<Boolean> uploadLiveData;
    public LiveData<Boolean> downloadLiveData;


    public MutableLiveData<Boolean> haveUploadedLiveData = new MutableLiveData<>();


    public MainViewModel(@NonNull Application application) {
        super(application);
        this.mRepository = AppRepository.getInstance(application.getApplicationContext());
        this.haveUploadedLiveData = mRepository.haveUploadedLiveData;
    }


    public void queryRegisterVisitors(final String name, final String email, final String phone) {
        this.registeredVisitor.postValue(mRepository.getRegisteredVisitors(name, email, phone));
    }


    public void queryAttendedTodayVisitors(final String name, final String email, final String phone) {
        this.attendedTodayVisitor.postValue(mRepository.getAttendedTodayVisitors(name, email, phone));
    }


    public void update(List<Visitor> visitorList, String venueId, String userId) {

        for (Visitor visitor : visitorList) {
            boolean changed = false;

            if ((visitor.getIsExibition() != visitor.getTempIsExibition())) {
                changed = true;
                visitor.setIsExibition(visitor.getTempIsExibition());
                visitor.setExibitionTime(DateUtils.timeFormat.format(new Date(System.currentTimeMillis())));
            }

            if ((visitor.getIsFashion() != visitor.getTempIsFashion())) {
                changed = true;
                visitor.setIsFashion(visitor.getTempIsFashion());
                visitor.setFashionTime(DateUtils.timeFormat.format(new Date(System.currentTimeMillis())));
            }

            if (changed) {

                visitor.setAttend(1);
                visitor.setDate(DateUtils.dateFormat.format(new Date(System.currentTimeMillis())));
                visitor.setVenueId(venueId);
                visitor.setApprovedBy(userId);

                if ((visitor.getAttentedToday() == 1)) {
                    visitor.setAttendanceEdited(1);
                }
            }
        }

        mRepository.addOrUpdateVisitors(visitorList);

        /*mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insertOrUpdate(visitorList);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                L.log("update: onSuccess: " + visitorList.toString());
                finish();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                L.log("update: onError: " + error.getMessage());
            }
        });*/

    }


    public void uploadData(String counter_user_id, String api_key) {

        //Timber.d("OLD GSON: " + GlobalGson.get().toJson(mRepository.getExistingAttendingVisitors()));
        //Timber.d("NEW GSON: " + GlobalGson.get().toJson(mRepository.getNewAttendingVisitors()));
        //Timber.d("OLD: " + GlobalGson.get().toJson(mRepository.getNewAttendingVisitors()));
        if (mRepository == null) {
            Timber.d("mRepository null");
        }
        uploadLiveData = mRepository.uploadData(counter_user_id, api_key, GlobalGson.get().toJson(mRepository.getExistingAttendingVisitors()), GlobalGson.get().toJson(mRepository.getNewAttendingVisitors()));
    }

    public void downloadData() {
        downloadLiveData = mRepository.getData(true);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        mRepository.destroyInstance();
    }
}

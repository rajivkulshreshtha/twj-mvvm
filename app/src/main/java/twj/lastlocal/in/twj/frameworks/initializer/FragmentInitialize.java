package twj.lastlocal.in.twj.frameworks.initializer;

import android.view.View;

/**
 * Created by RajivLL on 23-Mar-18.
 */

public interface FragmentInitialize {

    void initialize(View view);

}

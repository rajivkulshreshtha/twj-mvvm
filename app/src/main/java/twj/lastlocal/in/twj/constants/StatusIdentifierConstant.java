package twj.lastlocal.in.twj.constants;

/**
 * Created by RajivLL on 09-Apr-18.
 */
public class StatusIdentifierConstant {

    public static final int UNSUCCESSFUL = 0;
    public static final int SUCCESSFUL = 1;

}

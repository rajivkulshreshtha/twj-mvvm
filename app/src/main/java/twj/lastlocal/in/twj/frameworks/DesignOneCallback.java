package twj.lastlocal.in.twj.frameworks;

/**
 * Created by RajivLL on 03-Aug-18.
 */

public abstract class DesignOneCallback {
    public abstract void callVoid();
    public abstract <T> void callGeneric(T data);
}

package twj.lastlocal.in.twj.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by RajivLL on 20-Sep-18.
 */

public class PullData extends RealmObject {

    public static final String STATUS_CODE = "statusCode";
    public static final String MESSAGE = "message";
    public static final String DATA = "data";

    /**
     * status_code : 1
     * message : success
     * data : {"counter_user":[{"admin_id":"2","email_id":"furqan@gmail.com","password":"furqan123","fullname":"shaikh furqan","is_enable":"1","api_key":"87242u34ghifdf786fs7"},{"admin_id":"3","email_id":"rajiv@gmail.com","password":"rajiv123","fullname":"rajiv kulshestra","is_enable":"1","api_key":"782634bghg3hlndvfj3j"}],"venue":[{"venue_id":"2","location":"Mumbai","from_date":"2018-09-20","to_date":"2018-09-21"}],"visitor":[{"user_registration_id":"1","first_name":"karan","last_name":"panchal","email_id":"karan@gmail.com","phone_number":"9898989898","address":"thane","registration_from":"1","is_existing_user":"1","registration_date":"2018-09-19","is_enable":"1"},{"user_registration_id":"2","first_name":"swati","last_name":"wadekar","email_id":"swati@gmail.com","phone_number":"9797979797","address":"charni road","registration_from":"1","is_existing_user":"1","registration_date":"2018-09-19","is_enable":"1"}]}
     */

    @PrimaryKey
    long _id = UUID.randomUUID().getMostSignificantBits();
    @SerializedName("status_code")
    private int statusCode;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataBean data;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public PullData() {
    }

    protected PullData(Parcel in) {
        this.statusCode = in.readInt();
        this.message = in.readString();
        this.data = in.readParcelable(DataBean.class.getClassLoader());
    }

    public static final Parcelable.Creator<PullData> CREATOR = new Parcelable.Creator<PullData>() {
        @Override
        public PullData createFromParcel(Parcel source) {
            return new PullData(source);
        }

        @Override
        public PullData[] newArray(int size) {
            return new PullData[size];
        }
    };

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("PullData{");
        sb.append("statusCode=").append(statusCode);
        sb.append(", message='").append(message).append('\'');
        sb.append(", data=").append(data);
        sb.append('}');
        return sb.toString();
    }

}

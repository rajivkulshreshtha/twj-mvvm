package twj.lastlocal.in.twj.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by RajivLL on 18-Jul-18.
 */

public class BasicResponse implements Parcelable {

    /**
     * statusCode : 0
     * message : Some input is missing
     */

    @SerializedName("status_code")
    private int statusCode;
    @SerializedName("message")
    private String message;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("BasicResponse{");
        sb.append("statusCode='").append(statusCode).append('\'');
        sb.append(", message='").append(message).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.statusCode);
        dest.writeString(this.message);
    }

    public BasicResponse() {
    }

    protected BasicResponse(Parcel in) {
        this.statusCode = in.readInt();
        this.message = in.readString();
    }

    public static final Parcelable.Creator<BasicResponse> CREATOR = new Parcelable.Creator<BasicResponse>() {
        @Override
        public BasicResponse createFromParcel(Parcel source) {
            return new BasicResponse(source);
        }

        @Override
        public BasicResponse[] newArray(int size) {
            return new BasicResponse[size];
        }
    };
}

package twj.lastlocal.in.twj.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.Date;
import java.util.List;

import twj.lastlocal.in.twj.models.Visitor;
import twj.lastlocal.in.twj.repo.AppRepository;
import twj.lastlocal.in.twj.utils.DateUtils;


public class RegisterViewModel extends AndroidViewModel {

    private AppRepository mRepository;


    public RegisterViewModel(@NonNull Application application) {
        super(application);
        this.mRepository = AppRepository.getInstance(application.getApplicationContext());

    }


    public void addNewVisitor(String fullName, String address, String email, String phone, String hearAboutUs, String venueId, String userId, boolean isExhibition, boolean isFashion) {

        final Visitor visitor = new Visitor();
        visitor.setFullName(fullName);
        visitor.setAddress(address);
        visitor.setEmailId(email);
        visitor.setPhoneNumber(phone);
        visitor.setWhereDidYouHere(hearAboutUs);
        visitor.setRegistrationFrom(3);
        visitor.setRegistrationDate(DateUtils.dateTimeFormat.format(new Date(System.currentTimeMillis())));

        //
        visitor.setAttend(1);
        visitor.setDate(DateUtils.dateFormat.format(new Date(System.currentTimeMillis())));
        //visitor.setTime(DateUtils.timeFormat.format(new Date(System.currentTimeMillis())));
        visitor.setVenueId(venueId);
        visitor.setApprovedBy(userId);

        //

        //final RealmList<ExibitionDateBean> exhibitionsList = new RealmList<>();
        // ExibitionDateBean exibitionDateBean = new ExibitionDateBean();
        //exibitionDateBean.setAttend(1);
        //exibitionDateBean.setDate(DateUtils.dateFormat.format(new Date(System.currentTimeMillis())));
        //exibitionDateBean.setTime(DateUtils.timeFormat.format(new Date(System.currentTimeMillis())));
        //exibitionDateBean.setVenueId(venueId);
        // exibitionDateBean.setApprovedBy(userId);

        //exibitionDateBean.setVenueId(Utils.getVenueId(EditActivity.this));
        //exibitionDateBean.setApprovedBy(Utils.getUserId(EditActivity.this));


        if (isExhibition) {
            //visitor.setCheckExibition(1);
            visitor.setIsExibition(1);
            visitor.setExibitionTime(DateUtils.timeFormat.format(new Date(System.currentTimeMillis())));

        }

        if (isFashion) {
            //visitor.setCheckFashion(1);
            visitor.setIsFashion(1);
            visitor.setFashionTime(DateUtils.timeFormat.format(new Date(System.currentTimeMillis())));
        }

        //exhibitionsList.add(exibitionDateBean);
        //visitor.setExibitionDate(exhibitionsList);

        mRepository.addOrUpdateVisitor(visitor);

    }


    //public void editVisitor(String fullName, String address, String email, String phone, String hearAboutUs, String venueId, String userId, boolean isExhibition, boolean isFashion) {
    public void editVisitor(Visitor visitor, String venueId, String userId) {

        /*final Visitor visitor = new Visitor();
        visitor.setFullName(fullName);
        visitor.setAddress(address);
        visitor.setEmailId(email);
        visitor.setPhoneNumber(phone);
        visitor.setWhereDidYouHere(hearAboutUs);
        visitor.setRegistrationFrom(3);
        visitor.setRegistrationDate(DateUtils.dateTimeFormat.format(new Date(System.currentTimeMillis())));*/

        //
        //visitor.setAttend(1);//doubt
        //visitor.setDate(DateUtils.dateFormat.format(new Date(System.currentTimeMillis())));
        //visitor.setTime(DateUtils.timeFormat.format(new Date(System.currentTimeMillis())));
        //visitor.setVenueId(venueId);
        //visitor.setApprovedBy(userId);

        //

        //final RealmList<ExibitionDateBean> exhibitionsList = new RealmList<>();
        // ExibitionDateBean exibitionDateBean = new ExibitionDateBean();
        //exibitionDateBean.setAttend(1);
        //exibitionDateBean.setDate(DateUtils.dateFormat.format(new Date(System.currentTimeMillis())));
        //exibitionDateBean.setTime(DateUtils.timeFormat.format(new Date(System.currentTimeMillis())));
        //exibitionDateBean.setVenueId(venueId);
        // exibitionDateBean.setApprovedBy(userId);

        //exibitionDateBean.setVenueId(Utils.getVenueId(EditActivity.this));
        //exibitionDateBean.setApprovedBy(Utils.getUserId(EditActivity.this));


        /*if (isExhibition) {
            //visitor.setCheckExibition(1);
            visitor.setIsExibition(1);
            visitor.setExibitionTime(DateUtils.timeFormat.format(new Date(System.currentTimeMillis())));

        }

        if (isFashion) {
            //visitor.setCheckFashion(1);
            visitor.setIsFashion(1);
            visitor.setFashionTime(DateUtils.timeFormat.format(new Date(System.currentTimeMillis())));
        }*/

        //exhibitionsList.add(exibitionDateBean);
        //visitor.setExibitionDate(exhibitionsList);

        boolean changed = false;

        if ((visitor.getIsExibition() != visitor.getTempIsExibition())) {
            changed = true;
            visitor.setIsExibition(visitor.getTempIsExibition());
            visitor.setExibitionTime(DateUtils.timeFormat.format(new Date(System.currentTimeMillis())));
        }

        if ((visitor.getIsFashion() != visitor.getTempIsFashion())) {
            changed = true;
            visitor.setIsFashion(visitor.getTempIsFashion());
            visitor.setFashionTime(DateUtils.timeFormat.format(new Date(System.currentTimeMillis())));
        }

        if (changed) {

            visitor.setAttend(1);
            visitor.setDate(DateUtils.dateFormat.format(new Date(System.currentTimeMillis())));
            visitor.setVenueId(venueId);
            visitor.setApprovedBy(userId);

            if ((visitor.getAttentedToday() == 1)) {
                visitor.setAttendanceEdited(1);
            }
        }

        if ((visitor.getAttentedToday() == 1)) {
            visitor.setUserDetailEdited(1);
        }

        mRepository.addOrUpdateVisitor(visitor);

    }


    @Override
    protected void onCleared() {
        super.onCleared();
        mRepository.destroyInstance();
    }

    //TEST


    public void update(List<Visitor> visitorList, String venueId, String userId) {


        for (Visitor visitor : visitorList) {
            boolean changed = false;

            if ((visitor.getIsExibition() != visitor.getTempIsExibition())) {
                changed = true;
                visitor.setIsExibition(visitor.getTempIsExibition());
                visitor.setExibitionTime(DateUtils.timeFormat.format(new Date(System.currentTimeMillis())));
            }

            if ((visitor.getIsFashion() != visitor.getTempIsFashion())) {
                changed = true;
                visitor.setIsFashion(visitor.getTempIsFashion());
                visitor.setFashionTime(DateUtils.timeFormat.format(new Date(System.currentTimeMillis())));
            }

            if (changed) {

                visitor.setAttend(1);
                visitor.setDate(DateUtils.dateFormat.format(new Date(System.currentTimeMillis())));
                visitor.setVenueId(venueId);
                visitor.setApprovedBy(userId);

                if ((visitor.getAttentedToday() == 1)) {
                    visitor.setAttendanceEdited(1);
                }
            }
        }

        mRepository.addOrUpdateVisitors(visitorList);

        /*mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insertOrUpdate(visitorList);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                L.log("update: onSuccess: " + visitorList.toString());
                finish();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                L.log("update: onError: " + error.getMessage());
            }
        });*/

    }

}

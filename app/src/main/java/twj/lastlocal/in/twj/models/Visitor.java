package twj.lastlocal.in.twj.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by RajivLL on 20-Sep-18.
 */

public class Visitor extends RealmObject implements Parcelable {


    /**
     * user_registration_id : 1
     * full_name : karan chutiya
     * email_id : karan@female.com
     * phone_number : 9876543210
     * address : miththa nagar,chutiya mohalla,room no 69
     * registration_from : 3
     * is_existing_user : 1
     * where_did_you_here : online
     * registration_date : 2018-10-30 17:20:00
     * attented_today : 0
     * attendance_edited : 0
     * user_detail_edited : 0
     * check_exibition : 0
     * check_fashion : 0
     * exbition_id :
     * date : 2018-11-06
     * attend : 1
     * venue_id : 1
     * approved_by : 32
     * is_exibition : 1
     * is_fashion : 0
     * exibition_time : 18:00:00
     * fashion_time : 20:00:00
     */


    public static final String _ID = "_id";
    public static final String USER_REGISTRATION_ID = "userRegistrationId";
    public static final String FULL_NAME = "fullName";
    public static final String EMAIL_ID = "emailId";
    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String ADDRESS = "address";
    public static final String REGISTRATION_FROM = "registrationFrom";
    public static final String IS_EXISTING_USER = "isExistingUser";
    public static final String WHERE_DID_YOU_HERE = "whereDidYouHere";
    public static final String REGISTRATION_DATE = "registrationDate";
    public static final String ATTENTED_TODAY = "attentedToday";
    public static final String ATTENDANCE_EDITED = "attendanceEdited";
    public static final String USER_DETAIL_EDITED = "userDetailEdited";
    public static final String CHECK_EXIBITION = "checkExibition";
    public static final String CHECK_FASHION = "checkFashion";
    public static final String EXBITION_ID = "exbitionId";
    public static final String DATE = "date";
    public static final String ATTEND = "attend";
    public static final String VENUE_ID = "venueId";
    public static final String APPROVED_BY = "approvedBy";
    public static final String IS_EXIBITION = "isExibition";
    public static final String IS_FASHION = "isFashion";
    public static final String EXIBITION_TIME = "exibitionTime";
    public static final String FASHION_TIME = "fashionTime";
    public static final String TEMP_IS_EXIBITION = "tempIsExibition";
    public static final String TEMP_IS_FASHION = "tempIsFashion";


    @PrimaryKey
    long _id = UUID.randomUUID().getMostSignificantBits();
    @SerializedName("user_registration_id")
    private String userRegistrationId;
    @SerializedName("full_name")
    private String fullName;
    @SerializedName("email_id")
    private String emailId;
    @SerializedName("phone_number")
    private String phoneNumber;
    @SerializedName("address")
    private String address;
    @SerializedName("registration_from")
    private int registrationFrom;
    @SerializedName("is_existing_user")
    private int isExistingUser;
    @SerializedName("where_did_you_here")
    private String whereDidYouHere;
    @SerializedName("registration_date")
    private String registrationDate;
    @SerializedName("attented_today")
    private int attentedToday;
    @SerializedName("attendance_edited")
    private int attendanceEdited;
    @SerializedName("user_detail_edited")
    private int userDetailEdited;
    @SerializedName("check_exibition")
    private int checkExibition;
    @SerializedName("check_fashion")
    private int checkFashion;
    @SerializedName("exbition_id")
    private String exbitionId;
    @SerializedName("date")
    private String date;
    @SerializedName("attend")
    private int attend;
    @SerializedName("venue_id")
    private String venueId;
    @SerializedName("approved_by")
    private String approvedBy;
    @SerializedName("is_exibition")
    private int isExibition;
    @SerializedName("is_fashion")
    private int isFashion;
    @SerializedName("exibition_time")
    private String exibitionTime = "";
    @SerializedName("fashion_time")
    private String fashionTime = "";

    private int tempIsExibition;
    private int tempIsFashion;

    public int getTempIsExibition() {
        return tempIsExibition;
    }

    public void setTempIsExibition(int tempIsExibition) {
        this.tempIsExibition = tempIsExibition;
    }

    public int getTempIsFashion() {
        return tempIsFashion;
    }

    public void setTempIsFashion(int tempIsFashion) {
        this.tempIsFashion = tempIsFashion;
    }

    public String getUserRegistrationId() {
        return userRegistrationId;
    }

    public void setUserRegistrationId(String userRegistrationId) {
        this.userRegistrationId = userRegistrationId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getRegistrationFrom() {
        return registrationFrom;
    }

    public void setRegistrationFrom(int registrationFrom) {
        this.registrationFrom = registrationFrom;
    }

    public int getIsExistingUser() {
        return isExistingUser;
    }

    public void setIsExistingUser(int isExistingUser) {
        this.isExistingUser = isExistingUser;
    }

    public String getWhereDidYouHere() {
        return whereDidYouHere;
    }

    public void setWhereDidYouHere(String whereDidYouHere) {
        this.whereDidYouHere = whereDidYouHere;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public int getAttentedToday() {
        return attentedToday;
    }

    public void setAttentedToday(int attentedToday) {
        this.attentedToday = attentedToday;
    }

    public int getAttendanceEdited() {
        return attendanceEdited;
    }

    public void setAttendanceEdited(int attendanceEdited) {
        this.attendanceEdited = attendanceEdited;
    }

    public int getUserDetailEdited() {
        return userDetailEdited;
    }

    public void setUserDetailEdited(int userDetailEdited) {
        this.userDetailEdited = userDetailEdited;
    }

    public int getCheckExibition() {
        return checkExibition;
    }

    public void setCheckExibition(int checkExibition) {
        this.checkExibition = checkExibition;
    }

    public int getCheckFashion() {
        return checkFashion;
    }

    public void setCheckFashion(int checkFashion) {
        this.checkFashion = checkFashion;
    }

    public String getExbitionId() {
        return exbitionId;
    }

    public void setExbitionId(String exbitionId) {
        this.exbitionId = exbitionId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getAttend() {
        return attend;
    }

    public void setAttend(int attend) {
        this.attend = attend;
    }

    public String getVenueId() {
        return venueId;
    }

    public void setVenueId(String venueId) {
        this.venueId = venueId;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public int getIsExibition() {
        return isExibition;
    }

    public void setIsExibition(int isExibition) {
        this.isExibition = isExibition;
    }

    public int getIsFashion() {
        return isFashion;
    }

    public void setIsFashion(int isFashion) {
        this.isFashion = isFashion;
    }

    public String getExibitionTime() {
        return exibitionTime;
    }

    public void setExibitionTime(String exibitionTime) {
        this.exibitionTime = exibitionTime;
    }

    public String getFashionTime() {
        return fashionTime;
    }

    public void setFashionTime(String fashionTime) {
        this.fashionTime = fashionTime;
    }

    public Visitor() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this._id);
        dest.writeString(this.userRegistrationId);
        dest.writeString(this.fullName);
        dest.writeString(this.emailId);
        dest.writeString(this.phoneNumber);
        dest.writeString(this.address);
        dest.writeInt(this.registrationFrom);
        dest.writeInt(this.isExistingUser);
        dest.writeString(this.whereDidYouHere);
        dest.writeString(this.registrationDate);
        dest.writeInt(this.attentedToday);
        dest.writeInt(this.attendanceEdited);
        dest.writeInt(this.userDetailEdited);
        dest.writeInt(this.checkExibition);
        dest.writeInt(this.checkFashion);
        dest.writeString(this.exbitionId);
        dest.writeString(this.date);
        dest.writeInt(this.attend);
        dest.writeString(this.venueId);
        dest.writeString(this.approvedBy);
        dest.writeInt(this.isExibition);
        dest.writeInt(this.isFashion);
        dest.writeString(this.exibitionTime);
        dest.writeString(this.fashionTime);
        dest.writeInt(this.tempIsExibition);
        dest.writeInt(this.tempIsFashion);
    }

    protected Visitor(Parcel in) {
        this._id = in.readLong();
        this.userRegistrationId = in.readString();
        this.fullName = in.readString();
        this.emailId = in.readString();
        this.phoneNumber = in.readString();
        this.address = in.readString();
        this.registrationFrom = in.readInt();
        this.isExistingUser = in.readInt();
        this.whereDidYouHere = in.readString();
        this.registrationDate = in.readString();
        this.attentedToday = in.readInt();
        this.attendanceEdited = in.readInt();
        this.userDetailEdited = in.readInt();
        this.checkExibition = in.readInt();
        this.checkFashion = in.readInt();
        this.exbitionId = in.readString();
        this.date = in.readString();
        this.attend = in.readInt();
        this.venueId = in.readString();
        this.approvedBy = in.readString();
        this.isExibition = in.readInt();
        this.isFashion = in.readInt();
        this.exibitionTime = in.readString();
        this.fashionTime = in.readString();
        this.tempIsExibition = in.readInt();
        this.tempIsFashion = in.readInt();
    }

    public static final Creator<Visitor> CREATOR = new Creator<Visitor>() {
        @Override
        public Visitor createFromParcel(Parcel source) {
            return new Visitor(source);
        }

        @Override
        public Visitor[] newArray(int size) {
            return new Visitor[size];
        }
    };

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Visitor{");
        sb.append("_id=").append(_id);
        sb.append(", userRegistrationId='").append(userRegistrationId).append('\'');
        sb.append(", fullName='").append(fullName).append('\'');
        sb.append(", emailId='").append(emailId).append('\'');
        sb.append(", phoneNumber='").append(phoneNumber).append('\'');
        sb.append(", address='").append(address).append('\'');
        sb.append(", registrationFrom=").append(registrationFrom);
        sb.append(", isExistingUser=").append(isExistingUser);
        sb.append(", whereDidYouHere='").append(whereDidYouHere).append('\'');
        sb.append(", registrationDate='").append(registrationDate).append('\'');
        sb.append(", attentedToday=").append(attentedToday);
        sb.append(", attendanceEdited=").append(attendanceEdited);
        sb.append(", userDetailEdited=").append(userDetailEdited);
        sb.append(", checkExibition=").append(checkExibition);
        sb.append(", checkFashion=").append(checkFashion);
        sb.append(", exbitionId='").append(exbitionId).append('\'');
        sb.append(", date='").append(date).append('\'');
        sb.append(", attend=").append(attend);
        sb.append(", venueId='").append(venueId).append('\'');
        sb.append(", approvedBy='").append(approvedBy).append('\'');
        sb.append(", isExibition=").append(isExibition);
        sb.append(", isFashion=").append(isFashion);
        sb.append(", exibitionTime='").append(exibitionTime).append('\'');
        sb.append(", fashionTime='").append(fashionTime).append('\'');
        sb.append(", tempIsExibition=").append(tempIsExibition);
        sb.append(", tempIsFashion=").append(tempIsFashion);
        sb.append('}');
        return sb.toString();
    }
}
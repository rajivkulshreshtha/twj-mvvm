package twj.lastlocal.in.twj.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import twj.lastlocal.in.twj.R;
import twj.lastlocal.in.twj.frameworks.initializer.ActivityInitialize;
import twj.lastlocal.in.twj.models.Visitor;
import twj.lastlocal.in.twj.utils.Utils;
import twj.lastlocal.in.twj.viewmodels.RegisterViewModel;

public class EditActivity extends AppCompatActivity implements ActivityInitialize {

    //region REQUEST CODE for onActivityForResult
    public static final int REQUEST_CODE = 607;
    //endregion

    private RegisterViewModel registerViewModel;
    private EditText etFullName, etAddress, etPhoneNumber, etEmailId, etHearAboutUs;
    private CheckBox cbExhibition, cbFashion;
    private Button btSave;
    private Toolbar mToolbar;
    private Visitor editVisitor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);


        registerViewModel = ViewModelProviders.of(this).get(RegisterViewModel.class);

        initBundle();

        initialize();

        editHandler();


    }

    //region Init Bundle
    private void initBundle() {

        if (getIntent().hasExtra("EDIT_VISITOR")) {
            editVisitor = getIntent().getParcelableExtra("EDIT_VISITOR");
        }
    }
    //endregion

    //region Init Views
    @Override
    public void initialize() {

        mToolbar = findViewById(R.id.include);
        setSupportActionBar(mToolbar);

        etFullName = findViewById(R.id.full_name_edittext);
        etAddress = findViewById(R.id.address_edittext);
        etPhoneNumber = findViewById(R.id.phone_no_edittext);
        etEmailId = findViewById(R.id.email_id_edittext);
        etHearAboutUs = findViewById(R.id.hear_about_us_edittext);
        cbExhibition = findViewById(R.id.exhibition_checkbox);
        cbFashion = findViewById(R.id.fashion_checkbox);
        btSave = findViewById(R.id.save_button);

        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_back));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        clickHandler();
    }

    private void clickHandler() {
        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbExhibition.isChecked() || cbFashion.isChecked()) {
                    saveEditedVisitor();
                } else {
                    Toast.makeText(EditActivity.this, "Please check any checkbox", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    //endregion

    //region Init Edit Handler
    private void editHandler() {


        ((TextView) mToolbar.findViewById(R.id.toolbar_title)).setText("Edit Visitor");

        etFullName.setText(editVisitor.getFullName());
        etAddress.setText(editVisitor.getAddress());
        etPhoneNumber.setText(editVisitor.getPhoneNumber());
        etEmailId.setText(editVisitor.getEmailId());
        etHearAboutUs.setText(editVisitor.getWhereDidYouHere());


        if ((editVisitor.getCheckExibition() == 1) || (editVisitor.getIsExibition() == 1)) {
            cbExhibition.setChecked(true);
            cbExhibition.setEnabled(false);
            editVisitor.setTempIsExibition(1);
            editVisitor.setIsExibition(1);
        }

        if ((editVisitor.getCheckFashion() == 1) || (editVisitor.getIsFashion() == 1)) {
            cbFashion.setChecked(true);
            cbFashion.setEnabled(false);
            editVisitor.setTempIsFashion(1);
            editVisitor.setIsFashion(1);
        }
    }
    //endregion

    //region Save Edit Function
    private void saveEditedVisitor() {

        editVisitor.setFullName(etFullName.getText().toString());
        editVisitor.setAddress(etAddress.getText().toString());
        editVisitor.setEmailId(etEmailId.getText().toString());
        editVisitor.setPhoneNumber(etPhoneNumber.getText().toString());
        editVisitor.setWhereDidYouHere(etHearAboutUs.getText().toString());


        if (cbExhibition.isChecked()) {
            editVisitor.setTempIsExibition(1);
        }

        if (cbFashion.isChecked()) {
            editVisitor.setTempIsFashion(1);
        }

        registerViewModel.editVisitor(editVisitor, Utils.getVenueId(this), Utils.getUserId(this));

        saveAndGoBack();

    }

    //region Save And Go-Back Function
    private void saveAndGoBack() {
        //Pass RESULT to onActivityForResult
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
    //endregion

    //endregion

    //region Don't save Go-Back Function
    private void goBack() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Warning!");
        builder.setMessage("All your changes will be lost.");
        builder.setCancelable(false);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();

    }

    @Override
    public void onBackPressed() {
        goBack();
    }
    //endregion

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

}


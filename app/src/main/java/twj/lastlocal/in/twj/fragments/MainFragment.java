package twj.lastlocal.in.twj.fragments;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;
import twj.lastlocal.in.twj.R;
import twj.lastlocal.in.twj.activities.EditActivity;
import twj.lastlocal.in.twj.activities.MainActivity;
import twj.lastlocal.in.twj.adapters.VisitorAdapter;
import twj.lastlocal.in.twj.models.Visitor;
import twj.lastlocal.in.twj.utils.Utils;
import twj.lastlocal.in.twj.viewmodels.MainViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MainFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MainFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainFragment extends Fragment implements VisitorAdapter.VisitorAdapterCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private MainViewModel mainViewModel;
    private Context mContext;
    private EditText etFullName, etPhoneNumber, etEmailId;
    private Button btSearch, btSave;
    private RecyclerView recyclerView;
    private VisitorAdapter adapter;
    private boolean isAttendanceTodayMode;
    private List<Visitor> visitorList = new ArrayList<>();
    public static final String ATTENDANCE_TODAY_MODE = "attendance.mode";

    //region Make Observer
    private Observer<List<Visitor>> registeredVisitorObserver = new Observer<List<Visitor>>() {
        @Override
        public void onChanged(@Nullable List<Visitor> visitors) {
            if ((visitors != null ? visitors.size() : 0) > 0) {
                Timber.d(visitors.toString());
                handleAdapter(visitors);
            }
        }
    };


    private Observer<List<Visitor>> attendedTodayVisitorObserver = new Observer<List<Visitor>>() {
        @Override
        public void onChanged(@Nullable List<Visitor> visitors) {
            if ((visitors != null ? visitors.size() : 0) > 0) {
                Timber.d(visitors.toString());
                handleAdapter(visitors);
            }
        }
    };
    //endregion

    private OnFragmentInteractionListener mListener;

    public MainFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MainFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MainFragment newInstance(String param1, String param2) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mContext = context;
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            //Set Tab Mode
            initMode();
        }
    }

    //region Init TAB mode
    private void initMode() {
        isAttendanceTodayMode = getArguments().getBoolean(ATTENDANCE_TODAY_MODE, false);
    }
    //endregion

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        initViewModel();

        init(view);

        return view;
    }

    //region Init View Model
    private void initViewModel() {
        mainViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);

        if (isAttendanceTodayMode) {
            mainViewModel.attendedTodayVisitor.observe(getActivity(), attendedTodayVisitorObserver);
        } else {
            mainViewModel.registeredVisitor.observe(getActivity(), registeredVisitorObserver);
        }
    }
    //endregion

    //region Init Views
    private void init(View view) {

        recyclerView = view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        DividerItemDecoration decoration = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL);
        decoration.setDrawable(this.getResources().getDrawable(R.drawable.item_decoration));
        recyclerView.addItemDecoration(decoration);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        etFullName = view.findViewById(R.id.full_name_edittext);
        etPhoneNumber = view.findViewById(R.id.phone_no_edittext);
        etEmailId = view.findViewById(R.id.email_id_edittext);

        btSearch = view.findViewById(R.id.search);
        btSave = view.findViewById(R.id.save_button);

        if (isAttendanceTodayMode) {
            btSave.setVisibility(View.GONE);
        } else {
            btSave.setVisibility(View.VISIBLE);
        }

        clickHandler();

    }

    private void clickHandler() {

        if (!isAttendanceTodayMode) {

            btSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mainViewModel.update(visitorList, Utils.getVenueId(mContext), Utils.getUserId(mContext));
                    ((MainActivity) getActivity()).gotoAttendedToday();
                }
            });
        }

        btSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Utils.hideSoftKeyboard((InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE), view);

                if (etFullName.getText().toString().isEmpty() &&
                        etEmailId.getText().toString().isEmpty() &&
                        etPhoneNumber.getText().toString().isEmpty()) {

                    Toast.makeText(mContext, "Please fill any of field.", Toast.LENGTH_SHORT).show();

                } else {

                    if (isAttendanceTodayMode) {
                        mainViewModel.queryAttendedTodayVisitors(etFullName.getText().toString(),
                                etEmailId.getText().toString(),
                                etPhoneNumber.getText().toString());
                    } else {

                        mainViewModel.queryRegisterVisitors(etFullName.getText().toString(),
                                etEmailId.getText().toString(),
                                etPhoneNumber.getText().toString());
                    }
                }
            }
        });
    }
    //endregion

    //region Handle RecyclerView Search Adapter
    private void handleAdapter(List<Visitor> visitors) {

        visitorList.clear();
        visitorList.addAll(visitors);

        if (adapter == null) {
            adapter = new VisitorAdapter(visitorList, mContext);
            adapter.setVisitorAdapterCallback(this);
            recyclerView.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }
    //endregion

    //region On Edit Selected
    @Override
    public void editClicked(Visitor visitor) {
        Intent intent = new Intent(mContext, EditActivity.class);
        intent.putExtra("EDIT_VISITOR", visitor);
        getActivity().startActivityForResult(intent, EditActivity.REQUEST_CODE);
    }
    //endregion

    //region Result Handler
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((!isAttendanceTodayMode) && (requestCode == EditActivity.REQUEST_CODE) && (resultCode == Activity.RESULT_OK)) {
            visitorList.clear();
            adapter.notifyDataSetChanged();
        }
    }
    //endregion

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


}

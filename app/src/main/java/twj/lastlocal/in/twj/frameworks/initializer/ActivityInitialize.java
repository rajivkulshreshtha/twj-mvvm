package twj.lastlocal.in.twj.frameworks.initializer;

public interface ActivityInitialize {

    void initialize();
}

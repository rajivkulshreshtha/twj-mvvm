package twj.lastlocal.in.twj.models;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {

    private CounterUser counterUser;
    private Venue venue;
    private boolean haveUploaded;

    public void append(User user) {

        if (user.getCounterUser() != null) {
            this.setCounterUser(user.getCounterUser());
        }

        if (user.getVenue() != null) {
            this.setVenue(user.getVenue());
        }

        this.setHaveUploaded(user.isHaveUploaded());

    }

    public User() {
    }

    public CounterUser getCounterUser() {
        return counterUser;
    }

    public void setCounterUser(CounterUser counterUser) {
        this.counterUser = counterUser;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    public boolean isHaveUploaded() {
        return haveUploaded;
    }

    public void setHaveUploaded(boolean haveUploaded) {
        this.haveUploaded = haveUploaded;
    }

    public User(CounterUser counterUser, Venue venue) {

        if (counterUser != null && venue != null) {
            this.counterUser = counterUser;
            this.venue = venue;
        }
    }

    private User(Builder builder) {
        setCounterUser(builder.counterUser);
        setVenue(builder.venue);
        setHaveUploaded(builder.haveUploaded);
    }


    /**
     * {@code User} builder static inner class.
     */
    public static final class Builder {
        private CounterUser counterUser;
        private Venue venue;
        private boolean haveUploaded;

        public Builder() {
        }

        /**
         * Sets the {@code counterUser} and returns a reference to this Builder so that the methods can be chained together.
         *
         * @param counterUser the {@code counterUser} to set
         * @return a reference to this Builder
         */
        public Builder setCounterUser(CounterUser counterUser) {
            this.counterUser = counterUser;
            return this;
        }

        /**
         * Sets the {@code venue} and returns a reference to this Builder so that the methods can be chained together.
         *
         * @param venue the {@code venue} to set
         * @return a reference to this Builder
         */
        public Builder setVenue(Venue venue) {
            this.venue = venue;
            return this;
        }

        /**
         * Sets the {@code haveUploaded} and returns a reference to this Builder so that the methods can be chained together.
         *
         * @param haveUploaded the {@code haveUploaded} to set
         * @return a reference to this Builder
         */
        public Builder setHaveUploaded(boolean haveUploaded) {
            this.haveUploaded = haveUploaded;
            return this;
        }

        /**
         * Returns a {@code User} built from the parameters previously set.
         *
         * @return a {@code User} built with parameters of this {@code User.Builder}
         */
        public User build() {
            return new User(this);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.counterUser, flags);
        dest.writeParcelable(this.venue, flags);
        dest.writeByte(this.haveUploaded ? (byte) 1 : (byte) 0);
    }

    protected User(Parcel in) {
        this.counterUser = in.readParcelable(CounterUser.class.getClassLoader());
        this.venue = in.readParcelable(Venue.class.getClassLoader());
        this.haveUploaded = in.readByte() != 0;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("User{");
        sb.append("counterUser=").append(counterUser);
        sb.append(", venue=").append(venue);
        sb.append(", haveUploaded=").append(haveUploaded);
        sb.append('}');
        return sb.toString();
    }
}

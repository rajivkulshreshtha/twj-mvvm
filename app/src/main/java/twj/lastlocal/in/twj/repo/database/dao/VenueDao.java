package twj.lastlocal.in.twj.repo.database.dao;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import twj.lastlocal.in.twj.models.CounterUser;
import twj.lastlocal.in.twj.models.Venue;
import twj.lastlocal.in.twj.utils.LiveRealmData;

public class VenueDao {


    private Realm mRealm;

    public VenueDao(Realm mRealm) {
        this.mRealm = mRealm;
    }

    public LiveRealmData<Venue> getAllVenues() {

        /*return new LiveRealmData<Venue>() {
            @Override
            public RealmResults<Venue> runQuery(Realm realm) {
                return mRealm.where(Venue.class).findAll();
            }
        };*/
        return null;
    }

    public void addAllVenue(final List<Venue> venues) {
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insertOrUpdate(venues);
            }
        });
    }

    public void deleteAllVenues() {
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Venue> result = realm.where(Venue.class).findAllAsync();
                result.deleteAllFromRealm();
            }
        });
    }

    //void deleteVenue();

    public int getCount() {
        return (int) mRealm.where(CounterUser.class).count();
    }

    //start - Custom

    public void addOrUpdateVenue(final Venue venue) {
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insertOrUpdate(venue);
            }
        });
    }

    public void addOrUpdateVenues(final List<Venue> venues) {
        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insertOrUpdate(venues);
            }
        });
    }

    //end
}

package twj.lastlocal.in.twj.utils;

public class SingleEvent<T> {

    T content;
    boolean hasBeenHandled;

    public void setHasBeenHandled(boolean hasBeenHandled) {
        this.hasBeenHandled = hasBeenHandled;
    }


    public SingleEvent(T content) {
        this.content = content;
    }


    public T getContentIfNotHandled() {
        if (hasBeenHandled) {
            return null;
        } else {
            hasBeenHandled = true;
            return content;
        }
    }

    public T peekContent() {
        return content;
    }

}

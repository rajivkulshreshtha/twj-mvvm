package twj.lastlocal.in.twj.adapters;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import twj.lastlocal.in.twj.fragments.MainFragment;
import twj.lastlocal.in.twj.fragments.RegisterFragment;

import static twj.lastlocal.in.twj.fragments.MainFragment.ATTENDANCE_TODAY_MODE;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    //region Setting Up Tab
    @Override
    public Fragment getItem(int position) {
        Bundle bundle;
        Fragment fragment = null;
        if (position == 0) {
            fragment = new RegisterFragment();
        } else if (position == 1) {
            bundle = new Bundle();
            bundle.putBoolean(ATTENDANCE_TODAY_MODE, false);
            fragment = new MainFragment();
            fragment.setArguments(bundle);
        } else if (position == 2) {
            bundle = new Bundle();
            bundle.putBoolean(ATTENDANCE_TODAY_MODE, true);
            fragment = new MainFragment();
            fragment.setArguments(bundle);
        }
        return fragment;
    }
    //endregion

    //region Adding Tab count
    @Override
    public int getCount() {
        return 3;
    }
    //endregion

    //region Setting Tab Title
    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0) {
            title = "Register Visitor";
        } else if (position == 1) {
            title = "Already Registered";
        } else if (position == 2) {
            title = "Attended Today";
        }
        return title;
    }
    //endregion.
}

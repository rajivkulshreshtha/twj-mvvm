package twj.lastlocal.in.twj.utils;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class GenericAdapter extends TypeAdapter<Object> {
    @Override
    public void write(JsonWriter jsonWriter, Object o) throws IOException {
        jsonWriter.beginObject();

        List<Field> privateFields = new ArrayList<>();

        Field[] allFields = o.getClass().getDeclaredFields();

        for (Field field : allFields) {
            if (Modifier.isPrivate(field.getModifiers())) {
                privateFields.add(field);
            }
        }

        for (Field field : privateFields) {
            Object fieldValue = runGetter(field, o);
            jsonWriter.name(field.getName());
            if (fieldValue == null) {
                jsonWriter.value("");
            } else {
                jsonWriter.value(fieldValue.toString());
                //jsonWriter.value("");
            }
        }
        jsonWriter.endObject();
    }

    @Override
    public Object read(JsonReader jsonReader) throws IOException {
        /* Don't forget to add implementation here to have your Object back alive :) */
        return null;
    }

    /**
     * A generic field accessor runner.
     * Run the right getter on the field to get its value.
     *
     * @param field
     * @param o     {@code Object}
     * @return
     */
    public static Object runGetter(Field field, Object o) {
        // MZ: Find the correct method
        for (Method method : o.getClass().getMethods()) {
            if ((method.getName().startsWith("get")) && (method.getName().length() == (field.getName().length() + 3))) {
                if (method.getName().toLowerCase().endsWith(field.getName().toLowerCase())) {
                    try {
                        return method.invoke(o);
                    } catch (IllegalAccessException e) {
                    } catch (InvocationTargetException e) {
                    }
                }
            }
        }
        return null;
    }
}
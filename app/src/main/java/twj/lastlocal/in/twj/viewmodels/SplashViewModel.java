package twj.lastlocal.in.twj.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import twj.lastlocal.in.twj.repo.AppRepository;

public class SplashViewModel extends AndroidViewModel {


    private AppRepository mRepository;
    private LiveData<Boolean> splashLiveData;

    public SplashViewModel(@NonNull Application application) {
        super(application);
        this.mRepository = AppRepository.getInstance(application.getApplicationContext());
        this.splashLiveData = mRepository.getData(false);
    }

    public LiveData<Boolean> getSplashLiveData() {
        return splashLiveData;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        mRepository.destroyInstance();
    }

}

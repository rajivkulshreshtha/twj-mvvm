package twj.lastlocal.in.twj.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

    private String startDateString;
    private String endDateString;

    public final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public final static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public final static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

    public DateUtils(String startDateString, String endDateString) {
        this.startDateString = startDateString;
        this.endDateString = endDateString;
    }

    public long getDiffSeconds() {
        try {
            Date startDate = dateFormat.parse(startDateString);
            Date endDate = dateFormat.parse(endDateString);

            long diff = endDate.getTime() - startDate.getTime();
            return diff / 1000 % 60;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public long getDiffMinutes() {
        try {
            Date startDate = dateFormat.parse(startDateString);
            Date endDate = dateFormat.parse(endDateString);

            long diff = endDate.getTime() - startDate.getTime();
            return diff / (60 * 1000) % 60;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public long getDiffHours() {
        try {
            Date startDate = dateFormat.parse(startDateString);
            Date endDate = dateFormat.parse(endDateString);

            long diff = endDate.getTime() - startDate.getTime();
            return diff / (60 * 60 * 1000);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public long numbersOfDaysInEvents() {
        return getDiffDays() + 1;
    }

    private long getDiffDays() {
        try {
            Date startDate = dateFormat.parse(startDateString);
            Date endDate = dateFormat.parse(endDateString);

            return (int) ((endDate.getTime() - startDate.getTime()) / (1000 * 60 * 60 * 24));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

}

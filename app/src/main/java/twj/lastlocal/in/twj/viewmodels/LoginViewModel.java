package twj.lastlocal.in.twj.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import twj.lastlocal.in.twj.models.User;
import twj.lastlocal.in.twj.repo.AppRepository;

public class LoginViewModel extends AndroidViewModel {

    private AppRepository mRepository;
    private MutableLiveData<User> loginLiveData = new MutableLiveData<>();


    public LoginViewModel(@NonNull Application application) {
        super(application);
        this.mRepository = AppRepository.getInstance(application.getApplicationContext());
    }


    public MutableLiveData<User> getLoginLiveData() {
        return loginLiveData;
    }

    public void performLoginCheck(String email, String password) {
        loginLiveData.postValue(mRepository.performLoginCheck(email, password));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        mRepository.destroyInstance();
    }
}

package twj.lastlocal.in.twj.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.TypedValue;
import android.view.inputmethod.InputMethodManager;

import twj.lastlocal.in.twj.constants.ApplicationConstant;
import twj.lastlocal.in.twj.models.User;
import twj.lastlocal.in.twj.models.Venue;

public class NewUtils {


    public static int convertDpToPixels(float dp, Context context) {
        Resources resources = context.getResources();
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                resources.getDisplayMetrics()
        );

    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static void changeDrawableColor(Context context, Drawable drawable, int color) {


        if (drawable != null) {

            drawable.setColorFilter(new
                    PorterDuffColorFilter(ContextCompat.getColor(context, color), PorterDuff.Mode.SRC_IN));

        }
    }


    //start = SP
    public static void saveUserPreference(Context context, String userJson) {

        if (ApplicationConstant.DEBUG) {
            L.log(userJson);
        }
        SharedPreferences.Editor editor = context.getSharedPreferences("PREF_USER", Context.MODE_PRIVATE).edit();
        editor.putString("TAG_USER", userJson);
        editor.apply();

    }

    public static User getUserData(Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("PREF_USER", Context.MODE_PRIVATE);
        String userJson = sharedPreferences.getString("TAG_USER", "");

        if (ApplicationConstant.DEBUG) {
            L.log(userJson);
        }
        if (!userJson.isEmpty()) {
            User user = GlobalGson.get().fromJson(userJson, User.class);
            return user;
        } else {
            return null;
        }
    }


    public static String getUserApiKey(Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("PREF_USER", Context.MODE_PRIVATE);
        String userJson = sharedPreferences.getString("TAG_USER", "");

        if (ApplicationConstant.DEBUG) {
            L.log(userJson);
        }

        if (!userJson.isEmpty()) {
            User user = GlobalGson.get().fromJson(userJson, User.class);
            return user.getCounterUser().getApiKey();
        } else {
            return "0";
        }
    }

    public static String getUserId(Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("PREF_USER", Context.MODE_PRIVATE);
        String userJson = sharedPreferences.getString("TAG_USER", "");

        if (ApplicationConstant.DEBUG) {
            L.log(userJson);
        }

        if (!userJson.isEmpty()) {
            User user = GlobalGson.get().fromJson(userJson, User.class);
            return user.getCounterUser().getAdminId();
        } else {
            return "0";
        }
    }

    public static String getVenueId(Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("PREF_USER", Context.MODE_PRIVATE);
        String userJson = sharedPreferences.getString("TAG_USER", "");

        if (ApplicationConstant.DEBUG) {
            L.log(userJson);
        }

        if (!userJson.isEmpty()) {
            User user = GlobalGson.get().fromJson(userJson, User.class);
            return user.getVenue().getVenueId();
        } else {
            return "0";
        }
    }


    public static Venue getVenue(Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("PREF_USER", Context.MODE_PRIVATE);
        String userJson = sharedPreferences.getString("TAG_USER", "");

        if (ApplicationConstant.DEBUG) {
            L.log(userJson);
        }

        if (!userJson.isEmpty()) {
            User user = GlobalGson.get().fromJson(userJson, User.class);
            return user.getVenue();
        } else {
            return null;
        }
    }

    public static boolean hasUploaded(Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("PREF_USER", Context.MODE_PRIVATE);
        String userJson = sharedPreferences.getString("TAG_USER", "");

        if (ApplicationConstant.DEBUG) {
            L.log(userJson);
        }

        if (!userJson.isEmpty()) {
            User user = GlobalGson.get().fromJson(userJson, User.class);
            return user.isHaveUploaded();
        } else {
            return false;
        }
    }
    //end

    //start = TextFormatter (Spannable)
    public static Spannable makeBold(String str, int startIndex, int endIndex) {

        SpannableStringBuilder spannable = new SpannableStringBuilder(str);
        spannable.setSpan(new StyleSpan(Typeface.BOLD), startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannable;

    }

    public static Spannable changeColorAndBold(String str, int boldStartIndex, int boldStopIndex, int colorStartIndex, int colorStopIndex, int color) {
        SpannableStringBuilder spannable = new SpannableStringBuilder(str);
        spannable.setSpan(new StyleSpan(Typeface.BOLD), boldStartIndex, boldStopIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(new ForegroundColorSpan(color), colorStartIndex, colorStopIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannable;
    }
    //end


    //start = validators
    public static boolean emailValidator(String email) {

       /* if (email != null) {

            boolean foundMatch = email.matches("(?i)(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");
            return foundMatch;

        }
        return false;*/

        /*Pattern pattern;
        Matcher matcher;
        //final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        final String EMAIL_PATTERN = "(?i)(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();*/

        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();

    }

    public static boolean isValidMobile(String phone) {

        /*   boolean check=false;
    if(!Pattern.matches("[a-zA-Z]+", phone)) {
        if(phone.length() < 6 || phone.length() > 13) {
        // if(phone.length() != 10) {
            check = false;
            txtPhone.setError("Not Valid Number");
        } else {
            check = true;
        }
    } else {
        check=false;
    }
    return check;*/

        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }
    //end
}

package twj.lastlocal.in.twj.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import twj.lastlocal.in.twj.constants.ApplicationConstant;

public class L {

    private static final String TAG = "Logger";

    public static void log(String msg) {
        if (ApplicationConstant.DEBUG) {
            Log.i(TAG, msg);
        }
    }

    public static void log(String tag, String msg) {
        if (ApplicationConstant.DEBUG) {
            Log.i(tag, msg);
        }
    }

    public static void lToast(Context context, String msg) {
        if (ApplicationConstant.DEBUG) {
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
        }
    }

    public static void lLogAndToast(Context context, String msg) {
        if (ApplicationConstant.DEBUG) {
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
            Log.i(TAG, msg);
        }
    }

    public static void lLogAndToast(Context context, String tag, String msg) {
        if (ApplicationConstant.DEBUG) {
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
            Log.i(tag, msg);
        }
    }
}

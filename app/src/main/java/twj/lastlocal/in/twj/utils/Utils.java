package twj.lastlocal.in.twj.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import twj.lastlocal.in.twj.R;
import twj.lastlocal.in.twj.constants.ApplicationConstant;
import twj.lastlocal.in.twj.frameworks.DesignOneCallback;
import twj.lastlocal.in.twj.models.CounterUser;
import twj.lastlocal.in.twj.models.DataBean;
import twj.lastlocal.in.twj.models.User;
import twj.lastlocal.in.twj.models.Venue;
import twj.lastlocal.in.twj.models.Visitor;
import io.realm.Realm;
import io.realm.RealmResults;


public class Utils {

    public static Toolbar mToolbar;
    public static TextView toolbarTitle;
    public static Map<String, String> credentialMap;
    public static final String MULTIPART_FORM_DATA = "multipart/form-data";
    public static List<Visitor> visitorData;
    public static List<Venue> venueData;
    public static List<CounterUser> counterData;
    public static String jsonData;
    public static DataBean dataBean;
    public static Calendar calendar;


    public static int getDisplayWidthInPixel(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        //int width = displayMetrics.widthPixels;
        return displayMetrics.widthPixels;
    }


    public static int getDisplayHeightInPixel(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        //int height = displayMetrics.heightPixels;
        return displayMetrics.heightPixels;
    }


    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

    public static File createDirOnSDCard(String dirName) {
        File folder = new File(Environment.getExternalStorageDirectory() + "/"
                + dirName);
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdir();
        }
        return folder;
    }

    public static float getScreenParamPixelsNoDensity(Activity context, String paramWidthOrHeight) {
        Display display = context.getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float density = context.getResources().getDisplayMetrics().density;
        if (paramWidthOrHeight.equalsIgnoreCase("height")) {
            float dpHeight = outMetrics.heightPixels;/// density;
            return dpHeight;
        } else {
            float dpWidth = outMetrics.widthPixels;/// density;
            return dpWidth;
        }
    }


    public static String getFileName(URL extUrl) {
        String filename = "";
        String path = extUrl.getPath();
        String[] pathContents = path.split("[\\\\/]");
        if (pathContents != null) {
            int pathContentsLength = pathContents.length;
            System.out.println("Path Contents Length: " + pathContentsLength);
            for (int i = 0; i < pathContents.length; i++) {
                System.out.println("Path " + i + ": " + pathContents[i]);
            }
            String lastPart = pathContents[pathContentsLength - 1];
            String[] lastPartContents = lastPart.split("\\.");
            if (lastPartContents != null && lastPartContents.length > 1) {
                int lastPartContentLength = lastPartContents.length;
                System.out
                        .println("Last Part Length: " + lastPartContentLength);
                String name = "";
                for (int i = 0; i < lastPartContentLength; i++) {
                    System.out.println("Last Part " + i + ": "
                            + lastPartContents[i]);
                    if (i < (lastPartContents.length - 1)) {
                        name += lastPartContents[i];
                        if (i < (lastPartContentLength - 2)) {
                            name += ".";
                        }
                    }
                }
                String extension = lastPartContents[lastPartContentLength - 1];
                filename = name + "." + extension;
                System.out.println("Name: " + name);
                System.out.println("Extension: " + extension);
                System.out.println("Filename: " + filename);
            }
        }
        return filename;
    }

    public static Bitmap getRoundedShape(Bitmap bitmap, int width, int height) {
        Bitmap circleBitmap = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);

        BitmapShader shader = new BitmapShader(bitmap, TileMode.CLAMP,
                TileMode.CLAMP);
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setStrokeWidth(1);
        paint.setDither(true);
        paint.setShader(shader);
        Canvas c = new Canvas(circleBitmap);
        c.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 2, paint);
        return circleBitmap;
    }

    public static int convertDpToPixels(float dp, Context context) {
        Resources resources = context.getResources();
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                resources.getDisplayMetrics()
        );

    }

    /**
     * Reduce Size of Image
     *
     * @param res       Context Resourse for eg getResource()
     * @param resId     Image Id
     * @param reqWidth  width
     * @param reqHeight Height
     */
    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static float getScreenParam(Activity context, String paramWidthOrHeight) {
        Display display = context.getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float density = context.getResources().getDisplayMetrics().density;
        if (paramWidthOrHeight.equalsIgnoreCase("height")) {
            float dpHeight = outMetrics.heightPixels / density;
            return dpHeight;
        } else {
            float dpWidth = outMetrics.widthPixels / density;
            return dpWidth;
        }
    }


    public static void showDialog(Context context, String message) {
        if (ApplicationConstant.DEBUG) {

            AlertDialog.Builder buidler = new AlertDialog.Builder(context);
            buidler.setMessage(message);
            buidler.setCancelable(false);
            buidler.setPositiveButton(context.getResources().getString(R.string.ok_label),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method
                        }
                    });
            AlertDialog dialog = buidler.create();
            dialog.show();
        }
    }

    public static void showDialog(Context context, String message, String positiveLabel, String negativeLabel, DialogInterface.OnClickListener onPositiveListener, DialogInterface.OnClickListener onNegativeListener) {
        AlertDialog.Builder buidler = new AlertDialog.Builder(context);
        buidler.setMessage(message);
        buidler.setCancelable(false);

        if (onPositiveListener != null) {
            buidler.setPositiveButton((positiveLabel != null ? positiveLabel : context.getResources().getString(R.string.ok_label)),
                    onPositiveListener);
        }
        if (onNegativeListener != null) {
            buidler.setNegativeButton((negativeLabel != null ? negativeLabel : context.getResources().getString(R.string.cancel_label)),
                    onNegativeListener);
        }

        AlertDialog dialog = buidler.create();
        dialog.show();
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static void hideSoftKeyboard(InputMethodManager inputMethodManager,View view) {
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static boolean emailValidator(String email) {

       /* if (email != null) {

            boolean foundMatch = email.matches("(?i)(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");
            return foundMatch;

        }
        return false;*/

        /*Pattern pattern;
        Matcher matcher;
        //final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        final String EMAIL_PATTERN = "(?i)(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();*/

        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();

    }

    public static boolean isValidMobile(String phone) {

        /*   boolean check=false;
    if(!Pattern.matches("[a-zA-Z]+", phone)) {
        if(phone.length() < 6 || phone.length() > 13) {
        // if(phone.length() != 10) {
            check = false;
            txtPhone.setError("Not Valid Number");
        } else {
            check = true;
        }
    } else {
        check=false;
    }
    return check;*/

        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    public static String CurrencyFormatter(Double number) {
        DecimalFormat myCurrencyFormatter = new DecimalFormat("##,##,##,###.###");
        return myCurrencyFormatter.format(number);
    }


    public static String[] clean(final String[] v) {
        if (v != null) {
            List<String> list = new ArrayList<String>(Arrays.asList(v));
            list.removeAll(Collections.singleton(null));
            return list.toArray(new String[list.size()]);
        } else {
            return new String[]{};
        }
    }


    public static @NonNull
    <T> T checkNotNull(final T reference) {
        if (reference == null) {
            throw new NullPointerException();
        }
        return reference;
    }

    public static <T> boolean isNull(final T reference) {
        return reference == null;
    }

    public static void replaceFragment(@NonNull Context context, @NonNull Fragment fragment,
                                       Bundle bundle, int frameId, boolean addToBackStack) {


        checkNotNull(fragment);

        FragmentActivity fragmentActivity = (FragmentActivity) context;
        FragmentManager fragmentManager = fragmentActivity.getSupportFragmentManager();

        if (bundle != null) {
            bundle.putAll(fragment.getArguments());
            fragment.setArguments(bundle);
        }

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(frameId, fragment, "tag" + fragment.getClass().getSimpleName());

        if (addToBackStack) {
            transaction.addToBackStack("stack" + fragment.getClass().getSimpleName()); // <-- This makes magic!
        } else {
            transaction.disallowAddToBackStack();
        }

        transaction.commit();

    }


    public static Spannable makeBold(String str, int startIndex, int endIndex) {

        SpannableStringBuilder spannable = new SpannableStringBuilder(str);
        spannable.setSpan(new StyleSpan(Typeface.BOLD), startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannable;

    }

    public static Spannable changeColorAndBold(String str, int boldStartIndex, int boldStopIndex, int colorStartIndex, int colorStopIndex, int color) {
        SpannableStringBuilder spannable = new SpannableStringBuilder(str);
        spannable.setSpan(new StyleSpan(Typeface.BOLD), boldStartIndex, boldStopIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(new ForegroundColorSpan(color), colorStartIndex, colorStopIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannable;
    }

    public static void saveUserPreference(Context context, String userJson) {

        if (ApplicationConstant.DEBUG) {
            L.log(userJson);
        }
        SharedPreferences.Editor editor = context.getSharedPreferences("PREF_USER", Context.MODE_PRIVATE).edit();
        editor.putString("TAG_USER", userJson);
        editor.apply();

    }

    public static User getUserData(Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("PREF_USER", Context.MODE_PRIVATE);
        String userJson = sharedPreferences.getString("TAG_USER", "");

        if (ApplicationConstant.DEBUG) {
            L.log(userJson);
        }
        if (!userJson.isEmpty()) {
            User user = GlobalGson.get().fromJson(userJson, User.class);
            return user;
        } else {
            return null;
        }
    }


    public static String getUserApiKey(Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("PREF_USER", Context.MODE_PRIVATE);
        String userJson = sharedPreferences.getString("TAG_USER", "");

        if (ApplicationConstant.DEBUG) {
            L.log(userJson);
        }

        if (!userJson.isEmpty()) {
            User user = GlobalGson.get().fromJson(userJson, User.class);
            return user.getCounterUser().getApiKey();
        } else {
            return "0";
        }
    }

    public static String getUserId(Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("PREF_USER", Context.MODE_PRIVATE);
        String userJson = sharedPreferences.getString("TAG_USER", "");

        if (ApplicationConstant.DEBUG) {
            L.log(userJson);
        }

        if (!userJson.isEmpty()) {
            User user = GlobalGson.get().fromJson(userJson, User.class);
            return user.getCounterUser().getAdminId();
        } else {
            return "0";
        }
    }

    public static String getVenueId(Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("PREF_USER", Context.MODE_PRIVATE);
        String userJson = sharedPreferences.getString("TAG_USER", "");

        if (ApplicationConstant.DEBUG) {
            L.log(userJson);
        }

        if (!userJson.isEmpty()) {
            User user = GlobalGson.get().fromJson(userJson, User.class);
            return user.getVenue().getVenueId();
        } else {
            return "0";
        }
    }


    public static Venue getVenue(Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("PREF_USER", Context.MODE_PRIVATE);
        String userJson = sharedPreferences.getString("TAG_USER", "");

        if (ApplicationConstant.DEBUG) {
            L.log(userJson);
        }

        if (!userJson.isEmpty()) {
            User user = GlobalGson.get().fromJson(userJson, User.class);
            return user.getVenue();
        } else {
            return null;
        }
    }


    public static boolean hasUploaded(Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("PREF_USER", Context.MODE_PRIVATE);
        String userJson = sharedPreferences.getString("TAG_USER", "");

        if (ApplicationConstant.DEBUG) {
            L.log(userJson);
        }

        if (!userJson.isEmpty()) {
            User user = GlobalGson.get().fromJson(userJson, User.class);
            return user.isHaveUploaded();
        } else {
            return false;
        }
    }


    public static Toolbar setupToolbar(final Activity activity, @Nullable String str) {
        mToolbar = (Toolbar) activity.findViewById(R.id.toolbar);
        AppCompatActivity appCompatActivity = (AppCompatActivity) activity;
        appCompatActivity.setSupportActionBar(mToolbar);
        // Hide the title
        if (TextUtils.isEmpty(str)) {
            appCompatActivity.getSupportActionBar().setTitle(str);
        }
        // Set onClickListener to customView
        toolbarTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        appCompatActivity.getSupportActionBar().setHomeAsUpIndicator(appCompatActivity.getResources().getDrawable(R.drawable.ic_back));
        appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbarTitle.setText(str);
        return mToolbar;
    }

    public static void setToolbarTitle(String toolbarTitle) {
        if (Utils.toolbarTitle != null) {
            Utils.toolbarTitle.setText(toolbarTitle);
        }
    }

    public static String emptyIfNull(@Nullable String str) {
        return str == null ? "" : str;
    }

    public static String emptyIfNull(@Nullable String str, @NonNull String defaultString) {
        return str == null ? defaultString : str;
    }

    public static void getBackgroundMessage(View view, String message) {

        ViewGroup viewGroup = (ViewGroup) view;

        if (viewGroup != null) {
            TextView textView = new TextView(view.getContext());
            textView.setText(message);
            textView.setTextSize(30f);
            textView.setGravity(Gravity.CENTER);
            viewGroup.addView(textView);
        }

    }


    public static String getSampleTime(long millisecond) {

        if (calendar == null) {
            calendar = Calendar.getInstance();
            calendar.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        }

        calendar.setTimeInMillis(millisecond);

        return String.format("%d:%d", calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND));
    }


    public static void changeDrawableColor(Context context, Drawable drawable, int color) {


        if (drawable != null) {

            drawable.setColorFilter(new
                    PorterDuffColorFilter(ContextCompat.getColor(context, color), PorterDuff.Mode.SRC_IN));

        }
    }


    //New
    public static void writeToFile(DataBean data, DesignOneCallback designOneCallback) {

        L.log("YES Log hai new ");
        if (data != null) {

            jsonData = GlobalGson.get().toJson((DataBean) data);

        } else if (getAllDbDataString() == null) {
            L.log(" null bhai bc");
            return;
        }

        L.log("FILE-KA: " + jsonData);

        FileOutputStream fos = null;

        try {
            final File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/testfolder/");

            if (!dir.exists()) {
                if (!dir.mkdirs()) {
                    Log.e("ALERT", "could not create the directories");
                }
            }

            final File myFile = new File(dir, "realmFile" + ".txt");

            if (!myFile.exists()) {
                myFile.createNewFile();
            }

            fos = new FileOutputStream(myFile);
            fos.write(jsonData.getBytes());
            fos.close();
            if (designOneCallback != null) {
                designOneCallback.callVoid();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static String getAllDbDataString() {

        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                RealmResults<Visitor> visitors = realm
                        .where(Visitor.class)
                        .findAll();

                RealmResults<CounterUser> counterUsers = realm
                        .where(CounterUser.class)
                        .findAll();

                RealmResults<Venue> venues = realm
                        .where(Venue.class)
                        .findAll();

                visitorData = realm.copyFromRealm(visitors);
                venueData = realm.copyFromRealm(venues);
                counterData = realm.copyFromRealm(counterUsers);


            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                L.log("getAllDbDataString: OnSuccess");
                L.log("getAllDbDataString: counterData: " + counterData);
                L.log("getAllDbDataString: venueData: " + venueData.toString());
                L.log("getAllDbDataString: visitorData: " + visitorData.toString());

                jsonData = GlobalGson.get().toJson(new DataBean(counterData, venueData, visitorData));
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                L.log("getAllDbDataString: onError");
            }
        });

        realm = null;
        return jsonData;

    }


    public static DataBean getAllDbData() {

        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {


                RealmResults<Visitor> visitors = realm
                        .where(Visitor.class)
                        .findAll();

                RealmResults<CounterUser> counterUsers = realm
                        .where(CounterUser.class)
                        .findAll();

                RealmResults<Venue> venues = realm
                        .where(Venue.class)
                        .findAll();


                visitorData = realm.copyFromRealm(visitors);
                venueData = realm.copyFromRealm(venues);
                counterData = realm.copyFromRealm(counterUsers);


            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                L.log("getAllDbDataString: OnSuccess");
                dataBean = new DataBean(counterData, venueData, visitorData);
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                L.log("getAllDbDataString: onError");
            }
        });

        realm = null;
        return null;

    }
}

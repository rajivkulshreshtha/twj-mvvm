package twj.lastlocal.in.twj.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import timber.log.Timber;
import twj.lastlocal.in.twj.R;
import twj.lastlocal.in.twj.frameworks.initializer.ActivityInitialize;
import twj.lastlocal.in.twj.models.User;
import twj.lastlocal.in.twj.viewmodels.LoginViewModel;

public class LoginActivity extends AppCompatActivity implements ActivityInitialize {


    private LoginViewModel loginViewModel;
    private EditText etUsername, etPassword;
    private Button btLogin;

    //region Make Observer
    private Observer<User> loginObserver = new Observer<User>() {
        @Override
        public void onChanged(@Nullable User user) {
            if (user != null) {
                Timber.d(user.toString());
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                intent.putExtra("USER", user);
                startActivity(intent);
            } else {
                Toast.makeText(LoginActivity.this, "Incorrect Username or Password", Toast.LENGTH_SHORT).show();
            }
        }
    };
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        intiViewHolder();

        initialize();

    }

    //region Init View Holder
    private void intiViewHolder() {
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
    }
    //endregion

    //region Init Views
    @Override
    public void initialize() {
        etUsername = findViewById(R.id.username_editText);
        etPassword = findViewById(R.id.password_editText);
        btLogin = findViewById(R.id.login_button);

        clickHandler();

    }

    private void clickHandler() {
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doLogin(loginViewModel);
            }
        });
    }
    //endregion

    //region Login Handler
    private void doLogin(final LoginViewModel loginViewModel) {

        if (TextUtils.isEmpty(etUsername.getText().toString())) {
            Toast.makeText(this, "Please fill username", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(etPassword.getText().toString())) {
            Toast.makeText(this, "Please fill password", Toast.LENGTH_SHORT).show();
            return;
        }

        loginViewModel.getLoginLiveData().observe(this, loginObserver);
        loginViewModel.performLoginCheck(etUsername.getText().toString(), etPassword.getText().toString());

    }
    //endregion

    @Override
    public void onBackPressed() {
        this.finishAffinity();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

}

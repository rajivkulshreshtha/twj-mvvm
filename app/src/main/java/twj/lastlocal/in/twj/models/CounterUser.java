package twj.lastlocal.in.twj.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by RajivLL on 20-Sep-18.
 */

public class CounterUser extends RealmObject implements Parcelable {


    /**
     * admin_id : 32
     * email_id : furqan@gmail
     * password : furqan123
     * fullname : Shaikh Furqan
     * is_enable : 1
     * api_key : 7fe2b652059e4a683893
     */

    public static final String _ID = "_id";
    public static final String ADMIN_ID = "adminId";
    public static final String EMAIL_ID = "emailId";
    public static final String PASSWORD = "password";
    public static final String FULLNAME = "fullname";
    public static final String IS_ENABLE = "isEnable";
    public static final String API_KEY = "apiKey";

    @PrimaryKey
    long _id = UUID.randomUUID().getMostSignificantBits();
    @SerializedName("admin_id")
    private String adminId;
    @SerializedName("email_id")
    private String emailId;
    @SerializedName("password")
    private String password;
    @SerializedName("fullname")
    private String fullname;
    @SerializedName("is_enable")
    private String isEnable;
    @SerializedName("api_key")
    private String apiKey;

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(String isEnable) {
        this.isEnable = isEnable;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.adminId);
        dest.writeString(this.emailId);
        dest.writeString(this.password);
        dest.writeString(this.fullname);
        dest.writeString(this.isEnable);
        dest.writeString(this.apiKey);
    }

    public CounterUser() {
    }

    protected CounterUser(Parcel in) {
        this.adminId = in.readString();
        this.emailId = in.readString();
        this.password = in.readString();
        this.fullname = in.readString();
        this.isEnable = in.readString();
        this.apiKey = in.readString();
    }

    public static final Parcelable.Creator<CounterUser> CREATOR = new Parcelable.Creator<CounterUser>() {
        @Override
        public CounterUser createFromParcel(Parcel source) {
            return new CounterUser(source);
        }

        @Override
        public CounterUser[] newArray(int size) {
            return new CounterUser[size];
        }
    };

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("CounterUser{");
        sb.append("adminId='").append(adminId).append('\'');
        sb.append(", emailId='").append(emailId).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", fullname='").append(fullname).append('\'');
        sb.append(", isEnable='").append(isEnable).append('\'');
        sb.append(", apiKey='").append(apiKey).append('\'');
        sb.append('}');
        return sb.toString();
    }


}
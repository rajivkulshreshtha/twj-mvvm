package twj.lastlocal.in.twj.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import twj.lastlocal.in.twj.R;
import twj.lastlocal.in.twj.activities.EditActivity;
import twj.lastlocal.in.twj.activities.MainActivity;
import twj.lastlocal.in.twj.fragments.MainFragment;
import twj.lastlocal.in.twj.models.Visitor;
import twj.lastlocal.in.twj.utils.L;

public class VisitorAdapter extends RecyclerView.Adapter<VisitorAdapter.ViewHolder> {


    private final Context mContext;
    private Drawable selectDrawable;
    private Drawable deselectDrawable;
    private Drawable unSelectDrawable;
    private final List<Visitor> visitorRealmResults;
    private VisitorAdapterCallback visitorAdapterCallback;

    //region Set Adapter Callback
    public void setVisitorAdapterCallback(VisitorAdapterCallback visitorAdapterCallback) {
        this.visitorAdapterCallback = visitorAdapterCallback;
    }
    //endregion


    public VisitorAdapter(List<Visitor> visitorRealmResults, Context mContext) {

        this.mContext = mContext;
        this.visitorRealmResults = visitorRealmResults;
        this.selectDrawable = ContextCompat.getDrawable(mContext, R.drawable.another_buttonshape);
        this.deselectDrawable = ContextCompat.getDrawable(mContext, R.drawable.another_buttonshape);
        this.unSelectDrawable = ContextCompat.getDrawable(mContext, R.drawable.stroke_button);
    }

    @Override
    public VisitorAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.adapter_visitor, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final VisitorAdapter.ViewHolder holder, int position) {

        L.log("OnClick: " + visitorRealmResults.toString());

        final Visitor visitor = visitorRealmResults.get(position);
        holder.tvName.setText(visitor.getFullName());
        holder.tvNumber.setText(visitor.getPhoneNumber());
        holder.tvEmail.setText(visitor.getEmailId());
        holder.tvAddress.setText(visitor.getAddress());


        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(mContext, EditActivity.class);
                intent.putExtra("EDIT_VISITOR", visitor);
                ((MainActivity) mContext).startActivityForResult(intent, EditActivity.REQUEST_CODE);*/
                visitorAdapterCallback.editClicked(visitor);
            }
        });


        if ((visitor.getCheckExibition() == 1) || (visitor.getIsExibition() == 1)) {
            disableTextView(holder.tvExhibition, mContext);
            visitor.setTempIsExibition(1);
            //temp
            visitor.setIsExibition(1);
        } else {
            if (visitor.getTempIsExibition() == 1) {
                selectTextView(holder.tvExhibition, mContext);
            } else {
                unSelectTextView(holder.tvExhibition, mContext);
            }

            holder.tvExhibition.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (visitor.getTempIsExibition() == 1) {
                        unSelectTextView(holder.tvExhibition, mContext);
                        visitor.setTempIsExibition(0);
                    } else {
                        visitor.setTempIsExibition(1);
                        selectTextView(holder.tvExhibition, mContext);
                    }
                    //setEdit(visitor);
                    L.log("OnCLICK: " + visitorRealmResults.toString());
                }
            });
        }


        if ((visitor.getCheckFashion() == 1) || (visitor.getIsFashion() == 1)) {
            disableTextView(holder.tvFashion, mContext);
            visitor.setTempIsFashion(1);
            //temp
            visitor.setIsFashion(1);
        } else {
            if (visitor.getTempIsFashion() == 1) {
                selectTextView(holder.tvFashion, mContext);
            } else {
                unSelectTextView(holder.tvFashion, mContext);
            }


            holder.tvFashion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (visitor.getTempIsFashion() == 1) {
                        unSelectTextView(holder.tvFashion, mContext);
                        visitor.setTempIsFashion(0);
                    } else {
                        visitor.setTempIsFashion(1);
                        selectTextView(holder.tvFashion, mContext);
                    }
                    //setEdit(visitor);
                    L.log("OnCLICK: " + visitorRealmResults.toString());

                }
            });

        }


        /*switch (visitor.getCheckFashion()) {
            case 1:
                //holder.ivEdit.setVisibility(View.INVISIBLE);
                disableTextView(holder.tvFashion, mContext);
                visitor.setTempCheckFashion(1);
                break;
            case 0:

                if (visitor.getTempCheckFashion() == 1) {
                    selectTextView(holder.tvFashion, mContext);
                } else {
                    unSelectTextView(holder.tvFashion, mContext);
                }

                holder.tvFashion.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (visitor.getTempCheckFashion() == 1) {
                            unSelectTextView(holder.tvFashion, mContext);
                            visitor.setTempCheckFashion(0);
                        } else {
                            visitor.setTempCheckFashion(1);
                            selectTextView(holder.tvFashion, mContext);
                        }
                        setEdit(visitor);
                        L.log("OnCLICK: " + visitorRealmResults.toString());

                    }
                });
                break;
        }


        switch (visitor.getCheckExibition()) {
            case 1:
                //holder.ivEdit.setVisibility(View.INVISIBLE);
                disableTextView(holder.tvExhibition, mContext);
                visitor.setTempCheckExibition(1);
                break;
            case 0:

                if (visitor.getTempCheckExibition() == 1) {
                    selectTextView(holder.tvExhibition, mContext);
                } else {
                    unSelectTextView(holder.tvExhibition, mContext);
                }

                holder.tvExhibition.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (visitor.getTempCheckExibition() == 1) {
                            unSelectTextView(holder.tvExhibition, mContext);
                            visitor.setTempCheckExibition(0);
                        } else {
                            visitor.setTempCheckExibition(1);
                            selectTextView(holder.tvExhibition, mContext);
                        }
                        setEdit(visitor);
                        L.log("OnCLICK: " + visitorRealmResults.toString());
                    }
                });
                break;
        }*/


    }


    @Override
    public int getItemCount() {
        return visitorRealmResults.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvName, tvNumber, tvEmail, tvAddress;
        TextView tvExhibition, tvFashion;
        ImageView ivEdit;

        ViewHolder(View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.name_textview);
            tvNumber = itemView.findViewById(R.id.number_textview);
            tvEmail = itemView.findViewById(R.id.email_textview);
            tvAddress = itemView.findViewById(R.id.address_textview);
            tvExhibition = itemView.findViewById(R.id.exhibition_textView);
            tvFashion = itemView.findViewById(R.id.fashion_textView);
            ivEdit = itemView.findViewById(R.id.edit_imageview);

        }
    }


    private void unSelectTextView(TextView textView, Context context) {
        unSelectDrawable.setColorFilter(new PorterDuffColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.SRC_IN));
        textView.setBackground(unSelectDrawable);
        textView.setTextColor(ContextCompat.getColor(context, R.color.black));
    }

    private void selectTextView(TextView textView, Context context) {
        selectDrawable.setColorFilter(new PorterDuffColorFilter(Color.parseColor("#6A383E"), PorterDuff.Mode.SRC_IN));
        textView.setBackground(selectDrawable);
        textView.setTextColor(ContextCompat.getColor(context, R.color.white));
    }

    private void disableTextView(TextView textView, Context context) {
        deselectDrawable.setColorFilter(new PorterDuffColorFilter(Color.parseColor("#66000000"), PorterDuff.Mode.SRC_IN));
        textView.setBackground(deselectDrawable);
        textView.setTextColor(ContextCompat.getColor(context, R.color.white));
    }


    //region Adapter Callback Interface
    public interface VisitorAdapterCallback {
        void editClicked(Visitor visitor);
    }
    //endregion

}

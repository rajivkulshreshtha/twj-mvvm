package twj.lastlocal.in.twj.repo;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;
import twj.lastlocal.in.twj.models.BasicResponse;
import twj.lastlocal.in.twj.models.CounterUser;
import twj.lastlocal.in.twj.models.PullData;
import twj.lastlocal.in.twj.models.User;
import twj.lastlocal.in.twj.models.Venue;
import twj.lastlocal.in.twj.models.Visitor;
import twj.lastlocal.in.twj.repo.api.ApiClient;
import twj.lastlocal.in.twj.repo.database.AppDatabase;
import twj.lastlocal.in.twj.utils.GlobalGson;
import twj.lastlocal.in.twj.utils.LiveRealmData;
import twj.lastlocal.in.twj.utils.NewUtils;
import twj.lastlocal.in.twj.utils.Utils;

public class AppRepository {

    private static AppRepository instance;
    private Context mContext;
    private AppDatabase appDatabase;
    private ApiClient apiClient;
    public MutableLiveData<Boolean> haveUploadedLiveData = new MutableLiveData<>();

    //private Executor executor = Executors.newSingleThreadExecutor();

    public static AppRepository getInstance(Context context) {

        if (instance == null) {
            instance = new AppRepository(context);
        }

        return instance;
    }

    private AppRepository(Context context) {
        mContext = context;
        appDatabase = AppDatabase.getInstance(context);
        //apiClient = new ApiClient();
        apiClient = ApiClient.getInstance();
    }


    public LiveData<Boolean> getData(boolean isDownload) {

        final MutableLiveData<Boolean> mutableLiveData = new MutableLiveData<>();

        if (appDatabase.database.isEmpty() || isDownload) {

            if (NewUtils.isConnectingToInternet(mContext)) {

                appDatabase.visitorDao.deleteAllVisitors();

                retrofit2.Callback<PullData> callback = new Callback<PullData>() {
                    @Override
                    public void onResponse(Call<PullData> call, Response<PullData> response) {

                        if (response.isSuccessful()) {

                            Timber.d("Api Called");
                            mutableLiveData.setValue(true);


                            addOrUpdateVisitors(response.body().getData().getVisitor());
                            addOrUpdateVenues(response.body().getData().getVenue());
                            addOrUpdateCounters(response.body().getData().getCounterUser());


                            /**/

                            haveUploadedLiveData.setValue(changeHaveUploaded(false));

                            /**/

                        }
                    }

                    @Override
                    public void onFailure(Call<PullData> call, Throwable t) {

                        Timber.e(t, "Problem calling Api");
                        mutableLiveData.setValue(false);

                    }
                };

                apiClient.downloadData(callback);

            } else {
                mutableLiveData.setValue(false);
            }

        } else {
            mutableLiveData.setValue(true);
            //mutableLiveData.setValue(!appDatabase.database.isEmpty());

        }

        return mutableLiveData;

    }

    public LiveData<Boolean> uploadData(String counter_user_id, String api_key, String old_visitor, String new_visitor) {

        final MutableLiveData<Boolean> mutableLiveData = new MutableLiveData<>();

        //if (appDatabase.database.isEmpty()) {

        if (NewUtils.isConnectingToInternet(mContext)) {

            retrofit2.Callback<BasicResponse> callback = new Callback<BasicResponse>() {
                @Override
                public void onResponse(Call<BasicResponse> call, Response<BasicResponse> response) {

                    if (response.isSuccessful()) {

                        Timber.d("Api Called: " + response.body().toString());
                        mutableLiveData.setValue(true);
                        /**/

                        haveUploadedLiveData.setValue(changeHaveUploaded(true));

                        /**/
                    }
                }

                @Override
                public void onFailure(Call<BasicResponse> call, Throwable t) {

                    Timber.e(t, "Problem calling Api");
                    mutableLiveData.setValue(false);

                }
            };


            if (apiClient == null) {
                Timber.d("apiClient null");
            }
            Timber.d("callback:" + callback + "\n  counter_user_id: " + counter_user_id + "\n  api_key: " + api_key + "\n  old_visitor: " + old_visitor + "\n  new_visitor: " + new_visitor);

            //ApiClient.getInstance().uploadData(callback, counter_user_id, api_key, old_visitor, new_visitor);
            apiClient.uploadData(callback, counter_user_id, api_key, old_visitor, new_visitor);

        } else {
            mutableLiveData.setValue(false);
        }

        /*} else {
            mutableLiveData.setValue(true);
            //mutableLiveData.setValue(!appDatabase.database.isEmpty());

        }*/

        return mutableLiveData;

    }

   /* public LiveRealmData<Visitor> getRegisteredVisitors(final String query) {
        return appDatabase.visitorDao.registeredVisitors(query);
    }*/

    /*protected LiveData<List<Visitor>> getRegisteredVisitors(final String query) {
        return Transformations.switchMap(appDatabase.visitorDao.registeredVisitors(query), new Function<List<Visitor>, LiveData<List<Visitor>>>() {
            @Override
            public LiveData<List<Visitor>> apply(List<Visitor> input) {
                if (input != null) {
                    MutableLiveData<List<Visitor>> mutableLiveData = new MutableLiveData<List<Visitor>>();
                    mutableLiveData.setValue(input);
                    return mutableLiveData;
                } else {
                    return null;
                }
            }
        });
    }*/

    public List<Visitor> getRegisteredVisitors(final String name, final String email, final String phone) {
        return appDatabase.database.copyFromRealm(appDatabase.visitorDao.registeredVisitors(name, email, phone));
    }

    public List<Visitor> getAttendedTodayVisitors(final String name, final String email, final String phone) {
        return appDatabase.database.copyFromRealm(appDatabase.visitorDao.attendedTodayVisitors(name, email, phone));
    }

    public List<Visitor> getNewAttendingVisitors() {
        return appDatabase.database.copyFromRealm(appDatabase.visitorDao.newAttendingVisitors());
    }


    public List<Visitor> getExistingAttendingVisitors() {
        return appDatabase.database.copyFromRealm(appDatabase.visitorDao.existingAttendingVisitors());
    }

    public LiveRealmData<Visitor> getAttendedVisitors(final String query) {
        return appDatabase.visitorDao.attendedVisitors(query);
    }

    public User performLoginCheck(final String email, final String password) {
        return appDatabase.counterDao.performLogin(email, password);
    }

    public void addOrUpdateVisitors(final List<Visitor> visitors) {
        appDatabase.visitorDao.addOrUpdateVisitors(visitors);
    }

    private void addOrUpdateVenues(final List<Venue> venues) {
        appDatabase.venueDao.addOrUpdateVenues(venues);
    }

    private void addOrUpdateCounters(final List<CounterUser> counterUsers) {

        appDatabase.counterDao.addOrUpdateCounters(counterUsers);

    }


    public void addOrUpdateVisitor(final Visitor visitors) {
        appDatabase.visitorDao.addOrUpdateVisitor(visitors);
    }

    public void destroyInstance() {
        appDatabase.destroyInstance();
        apiClient.destroyInstance();
        instance = null;
    }


    private boolean changeHaveUploaded(boolean haveUploaded) {

        User user = new User();
        user.setHaveUploaded(haveUploaded);

        User preference = (Utils.getUserData(mContext) == null) ? new User() : Utils.getUserData(mContext);

        if (preference != null) {
            preference.append(user);
            Utils.saveUserPreference(mContext, GlobalGson.get().toJson(preference));
        }

        return haveUploaded;
    }
}

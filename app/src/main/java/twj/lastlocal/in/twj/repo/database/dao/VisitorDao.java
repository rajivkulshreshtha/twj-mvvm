package twj.lastlocal.in.twj.repo.database.dao;

import java.util.List;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmResults;
import twj.lastlocal.in.twj.models.Visitor;
import twj.lastlocal.in.twj.utils.LiveRealmData;

public class VisitorDao {


    private Realm mRealm;

    private boolean result;

    public VisitorDao(Realm mRealm) {
        this.mRealm = mRealm;
    }

    public LiveRealmData<Visitor> getAllVisitors() {

        /*return new LiveRealmData<Visitor>() {
            @Override
            public RealmResults<Visitor> runQuery(Realm realm) {

                return mRealm.where(Visitor.class).findAll();

            }
        };*/
        return new LiveRealmData<>(mRealm.where(Visitor.class).findAll());

    }

    public void deleteAllVisitors() {
        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Visitor> result = realm.where(Visitor.class).findAll();
                result.deleteAllFromRealm();
            }
        });
    }

    //void deleteVisitor();

    public LiveRealmData<Visitor> getOldVisitors() {
        /*return new LiveRealmData<Visitor>() {
            @Override
            public RealmResults<Visitor> runQuery(Realm realm) {
                return mRealm.where(Visitor.class).equalTo(Visitor.IS_EXISTING_USER, "1").findAllAsync();
            }
        };*/

        return new LiveRealmData<>(mRealm.where(Visitor.class).equalTo(Visitor.IS_EXISTING_USER, "1").findAllAsync());
    }

    public LiveRealmData<Visitor> getNewVisitors() {
        /*return new LiveRealmData<Visitor>() {
            @Override
            public RealmResults<Visitor> runQuery(Realm realm) {
                return mRealm.where(Visitor.class).equalTo(Visitor.IS_EXISTING_USER, "0").findAllAsync();
            }
        };*/

        return new LiveRealmData<>(mRealm.where(Visitor.class).equalTo(Visitor.IS_EXISTING_USER, "0").findAllAsync());
    }

    public int getCount() {
        return (int) mRealm.where(Visitor.class).count();
    }


    //start - Custom

    public LiveRealmData<Visitor> searchVisitors(final String query) {

        /*return new LiveRealmData<Visitor>() {
            @Override
            public RealmResults<Visitor> runQuery(Realm realm) {
                return mRealm
                        .where(Visitor.class)
                        .beginGroup()
                        .contains(Visitor.FULL_NAME, query, Case.INSENSITIVE)
                        .or()
                        .contains(Visitor.EMAIL_ID, query, Case.INSENSITIVE)
                        .or()
                        .contains(Visitor.ADDRESS, query, Case.INSENSITIVE)
                        .or()
                        .contains(Visitor.PHONE_NUMBER, query, Case.INSENSITIVE)
                        .endGroup()
                        *//*.and()
                        .equalTo(Visitor.ATTENDED_TODAY, 0)*//*
                        .findAllAsync();
            }
        };*/

        return null;

    }

    public void addOrUpdateVisitor(final Visitor visitor) {

       /* mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insertOrUpdate(visitor);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                result = true;
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Timber.e(error, "addOrUpdateVisitor");
                result = false;
            }
        });*/


        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insertOrUpdate(visitor);
            }
        });

    }

    public void addOrUpdateVisitors(final List<Visitor> visitors) {

        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insertOrUpdate(visitors);
            }
        });

    }


    //end

    //start new

    public RealmResults<Visitor> registeredVisitors(final String name, final String email, final String phone) {


       /* return mRealm
                .where(Visitor.class)
                .beginGroup()
                .contains(Visitor.FULL_NAME, name, Case.INSENSITIVE)
                .or()
                .contains(Visitor.EMAIL_ID, email, Case.INSENSITIVE)
                .or()
                .contains(Visitor.PHONE_NUMBER, phone, Case.INSENSITIVE)
                .endGroup()
                .equalTo(Visitor.IS_EXISTING_USER, 1)
                .findAllAsync();*/



        return mRealm
                .where(Visitor.class)
                .contains(Visitor.FULL_NAME, name, Case.INSENSITIVE)
                .and()
                .contains(Visitor.EMAIL_ID, email, Case.INSENSITIVE)
                .and()
                .contains(Visitor.PHONE_NUMBER, phone, Case.INSENSITIVE)
                .findAllAsync()
                .where()
                .equalTo(Visitor.IS_EXISTING_USER, 1)
                .findAllAsync();

        /*return new LiveRealmData<Visitor>() {
            @Override
            public RealmResults<Visitor> runQuery(Realm realm) {
                return mRealm
                        .where(Visitor.class)
                        .beginGroup()
                        .contains(Visitor.FULL_NAME, query, Case.INSENSITIVE)
                        .or()
                        .contains(Visitor.EMAIL_ID, query, Case.INSENSITIVE)
                        .or()
                        .contains(Visitor.PHONE_NUMBER, query, Case.INSENSITIVE)
                        .endGroup()
                        .and()
                        .equalTo(Visitor.IS_EXISTING_USER, 1)
                        .findAllAsync();
            }
        };*/

       /* return new LiveRealmData<>(mRealm
                .where(Visitor.class)
                .beginGroup()
                .contains(Visitor.FULL_NAME, query, Case.INSENSITIVE)
                .or()
                .contains(Visitor.EMAIL_ID, query, Case.INSENSITIVE)
                .or()
                .contains(Visitor.PHONE_NUMBER, query, Case.INSENSITIVE)
                .endGroup()
                .and()
                .equalTo(Visitor.IS_EXISTING_USER, 1)
                .findAllAsync());*/

    }

    public RealmResults<Visitor> attendedTodayVisitors(final String name, final String email, final String phone) {


       /* return mRealm
                .where(Visitor.class)
                .beginGroup()
                .equalTo(Visitor.ATTENTED_TODAY, 1)
                .or()
                .equalTo(Visitor.ATTEND, 1)
                .and().beginGroup()
                .contains(Visitor.FULL_NAME, name, Case.INSENSITIVE)
                .or()
                .contains(Visitor.EMAIL_ID, email, Case.INSENSITIVE)
                .or()
                .contains(Visitor.PHONE_NUMBER, phone, Case.INSENSITIVE)
                .endGroup()
                .endGroup()
                .findAllAsync();*/


        return mRealm
                .where(Visitor.class)
                .contains(Visitor.FULL_NAME, name, Case.INSENSITIVE)
                .and()
                .contains(Visitor.EMAIL_ID, email, Case.INSENSITIVE)
                .and()
                .contains(Visitor.PHONE_NUMBER, phone, Case.INSENSITIVE)
                .findAllAsync()
                .where()
                .equalTo(Visitor.ATTENTED_TODAY, 1)
                .or()
                .equalTo(Visitor.ATTEND, 1)
                .findAllAsync();
    }


    public LiveRealmData<Visitor> attendedVisitors(final String query) {

        /*return new LiveRealmData<Visitor>() {
            @Override
            public RealmResults<Visitor> runQuery(Realm realm) {
                return mRealm
                        .where(Visitor.class)
                        .beginGroup()
                        .contains(Visitor.FULL_NAME, query, Case.INSENSITIVE)
                        .or()
                        .contains(Visitor.EMAIL_ID, query, Case.INSENSITIVE)
                        .or()
                        .contains(Visitor.PHONE_NUMBER, query, Case.INSENSITIVE)
                        .endGroup()
                        .and()
                        .equalTo(Visitor.ATTENTED_TODAY, 1)
                        .findAllAsync();
            }
        };*/

        return new LiveRealmData<>(mRealm
                .where(Visitor.class)
                .beginGroup()
                .contains(Visitor.FULL_NAME, query, Case.INSENSITIVE)
                .or()
                .contains(Visitor.EMAIL_ID, query, Case.INSENSITIVE)
                .or()
                .contains(Visitor.PHONE_NUMBER, query, Case.INSENSITIVE)
                .endGroup()
                .and()
                .equalTo(Visitor.ATTENTED_TODAY, 1)
                .findAllAsync());


    }

    //end new

    //start query new


    public RealmResults<Visitor> newAttendingVisitors() {


        return mRealm
                .where(Visitor.class)
                .equalTo(Visitor.IS_EXISTING_USER, 0)
                .equalTo(Visitor.ATTEND, 1)
                .findAllAsync();

    }


    public RealmResults<Visitor> existingAttendingVisitors() {


        return mRealm
                .where(Visitor.class)
                .equalTo(Visitor.IS_EXISTING_USER, 1)
                .beginGroup()
                .equalTo(Visitor.ATTEND, 1)
                .or()
                .equalTo(Visitor.ATTENTED_TODAY, 1)
                .or()
                .equalTo(Visitor.ATTENDANCE_EDITED, 1)
                .or()
                .equalTo(Visitor.USER_DETAIL_EDITED, 1)
                .endGroup()
                .findAllAsync();
    }

    //end query new

}

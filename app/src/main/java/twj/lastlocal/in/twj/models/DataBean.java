package twj.lastlocal.in.twj.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;

/**
 * Created by RajivLL on 20-Sep-18.
 */

public class DataBean extends RealmObject implements Parcelable {

    public static final String COUNTER_USER = "counterUserRealmList";
    public static final String VENUE = "venueRealmList";
    public static final String VISITOR = "visitorRealmList";

    @Ignore
    @SerializedName("counter_user")
    private List<CounterUser> counterUser;
    @Ignore
    @SerializedName("venue")
    private List<Venue> venue;
    @Ignore
    @SerializedName("visitor")
    private List<Visitor> visitor;

    public DataBean(List<CounterUser> counterUserRealmList, List<Venue> venueRealmList, List<Visitor> visitorRealmList) {
        this.setCounterUserRealmList((new RealmList<CounterUser>(counterUserRealmList.toArray(new CounterUser[counterUserRealmList.size()]))));
        this.setVenueRealmList((new RealmList<Venue>(venueRealmList.toArray(new Venue[venueRealmList.size()]))));
        this.setVisitorRealmList((new RealmList<Visitor>(visitorRealmList.toArray(new Visitor[visitorRealmList.size()]))));

        this.setCounterUser((new ArrayList<CounterUser>(Arrays.asList(counterUserRealmList.toArray(new CounterUser[counterUserRealmList.size()])))));
        this.setVenue((new ArrayList<Venue>(Arrays.asList(venueRealmList.toArray(new Venue[venueRealmList.size()])))));
        this.setVisitor((new ArrayList<Visitor>(Arrays.asList(visitorRealmList.toArray(new Visitor[visitorRealmList.size()])))));
    }

    @Expose
    private RealmList<CounterUser> counterUserRealmList;
    @Expose
    private RealmList<Venue> venueRealmList;
    @Expose
    private RealmList<Visitor> visitorRealmList;

    public List<CounterUser> getCounterUser() {
        return counterUser;
    }

    public void setCounterUser(List<CounterUser> counterUser) {
        this.counterUser = counterUser;
    }

    public List<Venue> getVenue() {
        return venue;
    }

    public void setVenue(List<Venue> venue) {
        this.venue = venue;
    }

    public List<Visitor> getVisitor() {
        return visitor;
    }

    public void setVisitor(List<Visitor> visitor) {
        this.visitor = visitor;
    }

    public RealmList<CounterUser> getCounterUserRealmList() {
        return counterUserRealmList;
    }

    public void setCounterUserRealmList(RealmList<CounterUser> counterUserRealmList) {
        this.counterUserRealmList = counterUserRealmList;
    }

    public RealmList<Venue> getVenueRealmList() {
        return venueRealmList;
    }

    public void setVenueRealmList(RealmList<Venue> venueRealmList) {
        this.venueRealmList = venueRealmList;
    }

    public RealmList<Visitor> getVisitorRealmList() {
        return visitorRealmList;
    }

    public void setVisitorRealmList(RealmList<Visitor> visitorRealmList) {
        this.visitorRealmList = visitorRealmList;
    }

    public DataBean() {
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.counterUser);
        dest.writeTypedList(this.venue);
        dest.writeTypedList(this.visitor);
    }

    protected DataBean(Parcel in) {
        this.counterUser.addAll(Arrays.asList(in.createTypedArray(CounterUser.CREATOR)));
        this.venue.addAll(Arrays.asList(in.createTypedArray(Venue.CREATOR)));
        this.visitor.addAll(Arrays.asList(in.createTypedArray(Visitor.CREATOR)));
    }

    public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>() {
        @Override
        public DataBean createFromParcel(Parcel source) {
            return new DataBean(source);
        }

        @Override
        public DataBean[] newArray(int size) {
            return new DataBean[size];
        }
    };


    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("DataBean{");
        sb.append("counterUser=").append(counterUser);
        sb.append(", venue=").append(venue);
        sb.append(", visitor=").append(visitor);
        sb.append(", counterUserRealmList=").append(counterUserRealmList);
        sb.append(", venueRealmList=").append(venueRealmList);
        sb.append(", visitorRealmList=").append(visitorRealmList);
        sb.append('}');
        return sb.toString();
    }
}
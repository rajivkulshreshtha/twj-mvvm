package twj.lastlocal.in.twj.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by RajivLL on 20-Sep-18.
 */

public class Venue extends RealmObject implements Parcelable {

    public static final String _ID = "_id";
    public static final String VENUE_ID = "venueId";
    public static final String LOCATION = "location";
    public static final String FROM_DATE = "fromDate";
    public static final String TO_DATE = "toDate";

    /**
     * venue_id : 1
     * location : Mumbai
     * from_date : 2018-10-25
     * to_date : 2018-10-25
     */
    @PrimaryKey
    long _id = UUID.randomUUID().getMostSignificantBits();
    @SerializedName("venue_id")
    private String venueId;
    @SerializedName("location")
    private String location;
    @SerializedName("from_date")
    private String fromDate;
    @SerializedName("to_date")
    private String toDate;

    public String getVenueId() {
        return venueId;
    }

    public void setVenueId(String venueId) {
        this.venueId = venueId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.venueId);
        dest.writeString(this.location);
        dest.writeString(this.fromDate);
        dest.writeString(this.toDate);
    }

    public Venue() {
    }

    protected Venue(Parcel in) {
        this.venueId = in.readString();
        this.location = in.readString();
        this.fromDate = in.readString();
        this.toDate = in.readString();
    }

    public static final Parcelable.Creator<Venue> CREATOR = new Parcelable.Creator<Venue>() {
        @Override
        public Venue createFromParcel(Parcel source) {
            return new Venue(source);
        }

        @Override
        public Venue[] newArray(int size) {
            return new Venue[size];
        }
    };

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Venue{");
        sb.append("venueId='").append(venueId).append('\'');
        sb.append(", location='").append(location).append('\'');
        sb.append(", fromDate='").append(fromDate).append('\'');
        sb.append(", toDate='").append(toDate).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
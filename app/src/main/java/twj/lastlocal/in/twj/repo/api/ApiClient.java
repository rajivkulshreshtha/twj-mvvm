package twj.lastlocal.in.twj.repo.api;

import android.support.annotation.NonNull;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import twj.lastlocal.in.twj.constants.ApplicationConstant;
import twj.lastlocal.in.twj.models.BasicResponse;
import twj.lastlocal.in.twj.models.PullData;


public class ApiClient {

    private @NonNull
    Retrofit retrofit;
    private @NonNull
    WebServices services;


    //region Base URL Handler
    private static final String IP = "prvy.in";
    private static final String BASE_URL = "http://" + IP + "/sme/exhibition_new/api/Dump/";
    //endregion

    //region Retrofit Instance

    //region Init Retrofit Instance
    public static volatile ApiClient instance;
    private static final Object LOCK = new Object();
    //endregion


    //region Handle Retrofit Instance
    private ApiClient() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpBuilder.build())
                .build();

        services = retrofit.create(WebServices.class);
    }

    public static ApiClient getInstance() {
        if (instance == null) {
            synchronized (LOCK) {
                if (instance == null) {
                    instance = new ApiClient();
                }
            }
        }
        return instance;
    }
    //endregion


    //endregion

    //region Logging Interceptor
    //region Init Logging Interceptor
    private static OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();
    private static HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
    //endregion

    //region Handle Logging Interceptor
    static {
        if (ApplicationConstant.DEBUG) {
            okHttpBuilder.addInterceptor(loggingInterceptor);
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        }
    }
    //endregion
    //endregion

    //region Api Callbacks
    public void downloadData(retrofit2.Callback<PullData> callback) {
        Call<PullData> call = services.pullDump();
        call.enqueue(callback);
    }

    public void uploadData(retrofit2.Callback<BasicResponse> callback, String counter_user_id, String api_key, String old_visitor, String new_visitor) {
        Call<BasicResponse> call = services.pushDump(counter_user_id, api_key, old_visitor, new_visitor);
        call.enqueue(callback);
    }
    //endregion

    //region Destroy Instance
    public void destroyInstance() {
        instance = null;
    }
    //endregion

}

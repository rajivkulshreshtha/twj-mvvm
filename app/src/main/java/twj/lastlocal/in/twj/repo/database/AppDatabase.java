package twj.lastlocal.in.twj.repo.database;

import android.content.Context;

import io.realm.Realm;
import twj.lastlocal.in.twj.repo.database.dao.CounterDao;
import twj.lastlocal.in.twj.repo.database.dao.VenueDao;
import twj.lastlocal.in.twj.repo.database.dao.VisitorDao;

public class AppDatabase {
    //public static final String DATABASE_NAME = "AppDatabase.db";
    public static volatile AppDatabase instance;
    public static final Object LOCK = new Object();

    public Realm database;
    public VisitorDao visitorDao;
    public VenueDao venueDao;
    public CounterDao counterDao;

    public static AppDatabase getInstance(Context context) {
        if (instance == null) {
            synchronized (LOCK) {
                if (instance == null) {
                    instance = new AppDatabase();
                }
            }
        }
        return instance;
    }

    public AppDatabase() {
        database = Realm.getDefaultInstance();
        visitorDao = new VisitorDao(database);
        venueDao = new VenueDao(database);
        counterDao = new CounterDao(database);
    }


    public void destroyInstance() {
        instance = null;
    }

}

package twj.lastlocal.in.twj.repo.api;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import twj.lastlocal.in.twj.models.BasicResponse;
import twj.lastlocal.in.twj.models.PullData;

public interface WebServices {

    //region Download Data
    @GET("pull_dump_data")
    Call<PullData> pullDump();
    //endregion

    //region Upload Data
    @FormUrlEncoded
    @POST("push_dump_data")
    Call<BasicResponse> pushDump(@Field("counter_user_id") String counter_user_id,
                                 @Field("api_key") String api_key,
                                 @Field("old_visitor") String old_visitor,
                                 @Field("new_visitor") String new_visitor);
    //endregion

}

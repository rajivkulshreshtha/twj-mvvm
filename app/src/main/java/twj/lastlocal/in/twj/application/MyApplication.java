package twj.lastlocal.in.twj.application;

import android.app.Application;


import timber.log.Timber;
import twj.lastlocal.in.twj.R;
import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;
import io.realm.Realm;
import io.realm.RealmConfiguration;


/**
 * Created by RajivLL on 19-Sep-18.
 */

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Timber.plant(new Timber.DebugTree() {

            @Override
            protected String createStackElementTag(StackTraceElement element) {
                return String.format("| CLASS=%s | METHOD=%s | LINE=%s |",
                        super.createStackElementTag(element),
                        element.getMethodName(),
                        element.getLineNumber());
            }

        });

        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/google_sans_regular.ttf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());

        Realm.init(this);
        RealmConfiguration realmConfig = new RealmConfiguration.Builder()
                .name("myDatabase.realm")
                .schemaVersion(0)
                .build();
        Realm.setDefaultConfiguration(realmConfig);

    }

}

package twj.lastlocal.in.twj.repo.database.dao;

import java.util.List;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmResults;
import twj.lastlocal.in.twj.models.CounterUser;
import twj.lastlocal.in.twj.models.User;
import twj.lastlocal.in.twj.models.Venue;
import twj.lastlocal.in.twj.utils.LiveRealmData;

public class CounterDao {

    private Realm mRealm;

    public CounterDao(Realm mRealm) {
        this.mRealm = mRealm;
    }

    public LiveRealmData<CounterUser> getAllCounterUsers() {
        /*return new LiveRealmData<CounterUser>() {
            @Override
            public RealmResults<CounterUser> runQuery(Realm realm) {
                return mRealm.where(CounterUser.class).findAll();
            }
        };*/
        return null;
    }

    public void deleteAllCounterUsers() {
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<CounterUser> result = realm.where(CounterUser.class).findAllAsync();
                result.deleteAllFromRealm();
            }
        });
    }

    //void deleteCounterUser(CounterUser counterUser);

    public int getCount() {
        return (int) mRealm.where(CounterUser.class).count();
    }


    //start - Custom

    public void addOrUpdateCounter(final CounterUser counterUser) {
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                /*RealmResults<CounterUser> result = realm.where(CounterUser.class).findAll();
                result.deleteAllFromRealm();*/
                realm.insertOrUpdate(counterUser);
            }
        });
    }

    public void addOrUpdateCounters(final List<CounterUser> counterUsers) {

        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insertOrUpdate(counterUsers);
            }
        });

    }


    public User performLogin(final String email, final String password) {

        CounterUser counterUser = mRealm
                .where(CounterUser.class)
                .equalTo(CounterUser.EMAIL_ID, email, Case.INSENSITIVE)
                .equalTo(CounterUser.PASSWORD, password, Case.INSENSITIVE)
                .findFirst();

        if (counterUser != null) {
            return new User.Builder().setVenue(mRealm
                    .where(Venue.class)
                    .findFirst()).setCounterUser(counterUser).build();
        } else {
            return null;
        }
    }

    //end
}

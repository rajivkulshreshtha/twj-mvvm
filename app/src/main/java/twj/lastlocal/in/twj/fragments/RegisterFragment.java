package twj.lastlocal.in.twj.fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import timber.log.Timber;
import twj.lastlocal.in.twj.R;
import twj.lastlocal.in.twj.activities.MainActivity;
import twj.lastlocal.in.twj.utils.Utils;
import twj.lastlocal.in.twj.viewmodels.RegisterViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RegisterFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RegisterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RegisterFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    private RegisterViewModel registerViewModel;
    private Context mContext;
    private EditText etFullName, etAddress, etPhoneNumber, etEmailId, etHearAboutUs;
    private CheckBox cbExhibition, cbFashion;
    private Button btRegister;

    private OnFragmentInteractionListener mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mContext = context;
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public RegisterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RegisterFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RegisterFragment newInstance(String param1, String param2) {
        RegisterFragment fragment = new RegisterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);

        initViewModel();

        init(view);

        return view;
    }

    private void initViewModel() {
        registerViewModel = ViewModelProviders.of(getActivity()).get(RegisterViewModel.class);
    }

    //region Init Views
    private void init(View view) {

        etFullName = view.findViewById(R.id.full_name_edittext);
        etAddress = view.findViewById(R.id.address_edittext);
        etPhoneNumber = view.findViewById(R.id.phone_no_edittext);
        etEmailId = view.findViewById(R.id.email_id_edittext);
        etHearAboutUs = view.findViewById(R.id.hear_about_us_edittext);
        btRegister = view.findViewById(R.id.save_button);
        cbExhibition = view.findViewById(R.id.exhibition_checkbox);
        cbFashion = view.findViewById(R.id.fashion_checkbox);

        clickHandler();

    }

    private void clickHandler() {
        btRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (cbExhibition.isChecked() || cbFashion.isChecked()) {

                    //Add New Visitor
                    registerViewModel.addNewVisitor(
                            etFullName.getText().toString(),
                            etAddress.getText().toString(),
                            etEmailId.getText().toString(),
                            etPhoneNumber.getText().toString(),
                            etHearAboutUs.getText().toString(),
                            Utils.getVenueId(mContext),
                            Utils.getUserId(mContext),
                            cbExhibition.isChecked(),
                            cbFashion.isChecked()
                    );

                    ((MainActivity) getActivity()).gotoAttendedToday();

                    clearAll();

                } else {
                    Toast.makeText(mContext, "Please check any checkbox", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //region Clear All Views
    private void clearAll(){
        etFullName.setText(null);
        etAddress.setText(null);
        etEmailId.setText(null);
        etPhoneNumber.setText(null);
        etHearAboutUs.setText(null);
        cbExhibition.setChecked(false);
        cbFashion.setChecked(false);
    }
    //endregion
    //endregion

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


}

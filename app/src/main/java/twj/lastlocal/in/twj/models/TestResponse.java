package twj.lastlocal.in.twj.models;

public class TestResponse {

    Success success;
    Error error;


    public static class Success<T> extends TestResponse {

        String value;
        T data;

        public T getData() {
            return data;
        }

        public String getValue() {
            return value;
        }

        public Success(String value) {
            this.value = value;
        }

        public Success(T data) {
            this.data = data;
        }

        public Success(String value, T data) {
            this.value = value;
            this.data = data;
        }


    }


    public class Error extends TestResponse {

        String value;

        public String getValue() {
            return value;
        }

        public Error(String value) {
            this.value = value;
        }
    }

}

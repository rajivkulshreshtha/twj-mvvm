package twj.lastlocal.in.twj.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.inputmethod.InputMethodManager;

import twj.lastlocal.in.twj.R;
import twj.lastlocal.in.twj.adapters.ViewPagerAdapter;
import twj.lastlocal.in.twj.fragments.MainFragment;
import twj.lastlocal.in.twj.fragments.RegisterFragment;
import twj.lastlocal.in.twj.frameworks.initializer.ActivityInitialize;
import twj.lastlocal.in.twj.models.User;
import twj.lastlocal.in.twj.utils.GlobalGson;
import twj.lastlocal.in.twj.utils.MyProgressDialog;
import twj.lastlocal.in.twj.utils.Utils;
import twj.lastlocal.in.twj.viewmodels.MainViewModel;

public class MainActivity extends AppCompatActivity implements ActivityInitialize, MainFragment.OnFragmentInteractionListener, RegisterFragment.OnFragmentInteractionListener {


    //region Constants for Option Items
    private static final int ACTION_UPLOAD = 86;
    private static final int ACTION_DOWNLOAD = 415;
    private static final int ACTION_LOGOUT = 866;
    //endregion

    private MainViewModel mainViewModel;
    private ViewPager viewPager;
    private SubMenu subMenu;

    //region Make Observer
    private Observer<Boolean> uploadLiveDataObserver = new Observer<Boolean>() {
        @Override
        public void onChanged(@Nullable Boolean aBoolean) {

            if (aBoolean != null) {
                if (aBoolean) {
                    MyProgressDialog.dismiss();
                }
            }
        }
    };


    private Observer<Boolean> haveUploadedObserver = new Observer<Boolean>() {
        @Override
        public void onChanged(@Nullable Boolean aBoolean) {

            if (aBoolean != null) {
                if (subMenu != null) {
                    setUp(aBoolean, subMenu);
                }
            }
        }
    };
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initBundle();

        intiViewHolder();

        initialize();

    }

    //region Bundle Handler
    private void initBundle() {
        if (getIntent().hasExtra("USER")) {

            User user = getIntent().getParcelableExtra("USER");

            User preference = (Utils.getUserData(MainActivity.this) == null) ? new User() : Utils.getUserData(MainActivity.this);

            if (preference != null) {
                preference.append(user);
                Utils.saveUserPreference(MainActivity.this, GlobalGson.get().toJson(preference));
            }
        }
    }
    //endregion

    //region Init ViewHolder
    private void intiViewHolder() {
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        mainViewModel.haveUploadedLiveData.observe(this, haveUploadedObserver);
    }
    //endregion

    //region Init Views
    @Override
    public void initialize() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        pageChangeHandler();

    }

    private void pageChangeHandler() {
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                Utils.hideSoftKeyboard((InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE), viewPager);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }
    //endregion

    //region Init Option Items
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_item, menu);

        MenuItem menuItem = menu.findItem(R.id.action_notification1);
        subMenu = menuItem.getSubMenu();
        setUp(Utils.hasUploaded(this), subMenu);

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case ACTION_UPLOAD:
                MyProgressDialog.show(this, R.string.progress_loading_label);
                mainViewModel.uploadData(Utils.getUserId(this), Utils.getUserApiKey(this));
                mainViewModel.uploadLiveData.observe(this, uploadLiveDataObserver);
                break;
            case ACTION_DOWNLOAD:
                MyProgressDialog.show(this, R.string.progress_loading_label);
                mainViewModel.downloadData();
                mainViewModel.downloadLiveData.observe(this, uploadLiveDataObserver);
                break;
            case ACTION_LOGOUT:
                performLogout();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUp(boolean hasUploaded, SubMenu subMenu) {
        subMenu.clear();
        if (hasUploaded) {
            subMenu.add(0, ACTION_DOWNLOAD, 1, "Download").setIcon(R.drawable.ic_download);
        } else {
            subMenu.add(0, ACTION_UPLOAD, 1, "Upload").setIcon(R.drawable.ic_upload);
        }
        subMenu.add(0, ACTION_LOGOUT, 2, "Logout").setIcon(R.drawable.ic_logout);
    }

    //endregion

    //region Logout
    private void performLogout() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Logout");
        builder.setMessage("Do you want to logout ?");
        builder.setCancelable(false);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                SharedPreferences.Editor editor = getSharedPreferences("PREF_USER", Context.MODE_PRIVATE).edit();
                editor.clear();
                editor.apply();

                Intent intent;
                intent = new Intent(MainActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();

            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
    //endregion

    //region Goto Attended Today Tab
    public void gotoAttendedToday() {
        viewPager.setCurrentItem(2);
    }
    //endregion


    //region To pass Activity for Results to Fragment
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
    //endregion

    @Override
    public void onFragmentInteraction(Uri uri) {
    }

}

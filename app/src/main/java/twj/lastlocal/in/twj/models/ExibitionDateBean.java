package twj.lastlocal.in.twj.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by RajivLL on 20-Sep-18.
 */
public class ExibitionDateBean extends RealmObject implements Parcelable {


    public static final String DATE = "date";
    public static final String TIME = "time";
    public static final String ATTEND = "attend";
    public static final String VENUE_ID = "venueId";
    public static final String APPROVED_BY = "approvedBy";
    public static final String IS_EXIBITION = "isExibition";
    public static final String IS_FASHION = "isFashion";

    /**
     * date : 2018-10-25
     * time : 15:00:00
     * attend : 1
     * venue_id : 1
     * approved_by : 32
     * is_exibition : 1
     * is_fashion : 1
     */

    @SerializedName("date")
    private String date;
    @SerializedName("time")
    private String time;
    @SerializedName("attend")
    private int attend;
    @SerializedName("venue_id")
    private String venueId;
    @SerializedName("approved_by")
    private String approvedBy;
    @SerializedName("is_exibition")
    private int isExibition;
    @SerializedName("is_fashion")
    private int isFashion;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getAttend() {
        return attend;
    }

    public void setAttend(int attend) {
        this.attend = attend;
    }

    public String getVenueId() {
        return venueId;
    }

    public void setVenueId(String venueId) {
        this.venueId = venueId;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public int getIsExibition() {
        return isExibition;
    }

    public void setIsExibition(int isExibition) {
        this.isExibition = isExibition;
    }

    public int getIsFashion() {
        return isFashion;
    }

    public void setIsFashion(int isFashion) {
        this.isFashion = isFashion;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.date);
        dest.writeString(this.time);
        dest.writeInt(this.attend);
        dest.writeString(this.venueId);
        dest.writeString(this.approvedBy);
        dest.writeInt(this.isExibition);
        dest.writeInt(this.isFashion);
    }

    public ExibitionDateBean() {
    }

    protected ExibitionDateBean(Parcel in) {
        this.date = in.readString();
        this.time = in.readString();
        this.attend = in.readInt();
        this.venueId = in.readString();
        this.approvedBy = in.readString();
        this.isExibition = in.readInt();
        this.isFashion = in.readInt();
    }

    public static final Parcelable.Creator<ExibitionDateBean> CREATOR = new Parcelable.Creator<ExibitionDateBean>() {
        @Override
        public ExibitionDateBean createFromParcel(Parcel source) {
            return new ExibitionDateBean(source);
        }

        @Override
        public ExibitionDateBean[] newArray(int size) {
            return new ExibitionDateBean[size];
        }
    };

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ExibitionDateBean{");
        sb.append("date='").append(date).append('\'');
        sb.append(", time='").append(time).append('\'');
        sb.append(", attend=").append(attend);
        sb.append(", venueId='").append(venueId).append('\'');
        sb.append(", approvedBy='").append(approvedBy).append('\'');
        sb.append(", isExibition=").append(isExibition);
        sb.append(", isFashion=").append(isFashion);
        sb.append('}');
        return sb.toString();
    }
}
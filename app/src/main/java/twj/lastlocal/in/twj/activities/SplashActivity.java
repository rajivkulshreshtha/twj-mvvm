package twj.lastlocal.in.twj.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import timber.log.Timber;
import twj.lastlocal.in.twj.R;
import twj.lastlocal.in.twj.models.User;
import twj.lastlocal.in.twj.utils.Utils;
import twj.lastlocal.in.twj.viewmodels.SplashViewModel;

public class SplashActivity extends AppCompatActivity {

    //region Make Observer
    private Observer<Boolean> splashObserver = new Observer<Boolean>() {
        @Override
        public void onChanged(@Nullable Boolean aBoolean) {

            if (aBoolean != null) {
                if (aBoolean) {

                    if (isLoggedIn()) {
                        startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    } else {
                        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    }

                }
            }
        }
    };
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        removeStatusBar();

        initViewHolder();

    }

    //region Init View Holder
    private void initViewHolder() {
        SplashViewModel splashViewModel = ViewModelProviders.of(this).get(SplashViewModel.class);
        splashViewModel.getSplashLiveData().observe(this, splashObserver);
    }
    //endregion

    //region Remove Status Bar
    private void removeStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
    }
    //endregion

    //region Handle is User Logged In OR Not
    private boolean isLoggedIn() {
        boolean isLoggedIn = false;
        try {
            User user = Utils.getUserData(this);
            isLoggedIn = user != null && user.getCounterUser() != null;
        } catch (NullPointerException e) {
            Timber.e(e);
        }
        return isLoggedIn;
    }
    //endregion

}
